/**
 * @file
 * Attaches the behaviors for the Layout Builder module.
 */

((Drupal, Sortable, once, $) => {
  const { ajax, behaviors, debounce, announce, formatPlural } = Drupal;

  /**
   * The homebox behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the toolbar rendering functionality to the toolbar element.
   */
  behaviors.homeboxBuilder = {
    attach(context) {
      // Toggle body class when the homebox add dialog opens or closes.
      // @improve replace jquery code with vanilla code
      // https://www.drupal.org/project/homebox/issues/3440251:
      // The correct replacement for the code below would be:
      // window.addEventListener("dialog:beforecreate", function () {
      //   // Add the 'homebox-sidebar-open' class before creating the dialog
      //   document.body.classList.add("homebox-sidebar-open");
      // });
      // window.addEventListener("dialog:beforeclose", function () {
      //   // Remove the 'homebox-sidebar-open' class before closing the dialog
      //   document.body.classList.remove("homebox-sidebar-open");
      // });
      // BUT this is not possible currently due to this problem:
      // https://stackoverflow.com/questions/25256173/can-i-use-jquery-trigger-with-event-listeners-added-with-addeventlistener
      // also see https://www.drupal.org/project/drupal/issues/3390549#comment-15288774
      // Once removed, we can also remove the jQuery dependency entirely!
      $(window).on('dialog:beforecreate', function (event) {
        $('body:first').addClass('homebox-sidebar-open');
      });
      $(window).on('dialog:beforeclose', function (event) {
        $('body:first').removeClass('homebox-sidebar-open');
      });
      $(window).on('dialog:aftercreate', function (event) {
        // QUICKFIX for Core UI Dialog CSS conflicts: Remove the hardcoded
        // .ui-dialog classes, so only our .homebox-sidebar classes affect
        // the styles of the sidebar.
        // See:
        // - https://www.drupal.org/project/homebox/issues/3398587
        // - https://www.drupal.org/project/homebox/issues/3398605
        $('.homebox-sidebar:first', context).removeClass(
          'ui-dialog ui-widget ui-widget-content ui-front',
        );
        $('.homebox-sidebar__titlebar:first', context).removeClass(
          'ui-dialog-titlebar ui-widget-header ui-helper-clearfix',
        );
        $('.homebox-sidebar__title:first', context).removeClass(
          'ui-dialog-title',
        );
        $('.homebox-sidebar__close:first', context).removeClass(
          'ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close',
        );
        $('.homebox-sidebar__content:first', context).removeClass(
          'ui-front ui-dialog-content ui-widget-content',
        );
      });
    },
  };

  /**
   * Implements autosubmit on value change for the portlet forms
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attach homebox drag drop behaviors.
   */
  behaviors.homeboxPortletAutoSubmit = {
    attach(context) {
      once(
        'porletautosubmit',
        'form.homebox-portlet-form input, form.homebox-portlet-form select,  form.homebox-portlet-form textarea',
        context,
      ).forEach((input) => {
        input.addEventListener(
          'change',
          Drupal.debounce(function () {
            const form = this.closest('form');
            const submitButton = form.querySelector(
              'input.form-submit[type="submit"]',
            );
            submitButton.click();
          }, 500),
        );
      });
    },
  };

  /**
   * Provides the ability to drag portlets to new positions in the homebox.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attach homebox drag drop behaviors.
   */
  behaviors.homeboxPortletDragDrop = {
    attach(context) {
      // Target region (put)
      once('sortable_region', '.homebox-builder-region', context).forEach(
        (region) => {
          Sortable.create(region, {
            draggable: '.homebox-portlet-form.draggable',
            handle: '.homebox-portlet__titlebar-drag-handle',
            ghostClass: 'ui-state-drop',
            group: {
              name: 'homebox-builder',
              put: true,
              pull: true,
            },
            // Fix form handling, see https://github.com/SortableJS/Sortable/issues/972:
            filter:
              'input,select,textarea,label,button,fieldset,legend,datalist,output,option,optgroup,.homebox-icon:not(.homebox-icon--drag)',
            preventOnFilter: false,
            // We currently cant highlight target regions permanently
            // till this is merged in https://github.com/SortableJS/Sortable/pull/2198
            // See also: https://github.com/SortableJS/Sortable/issues/460
            // So for now, layout__region--current-target will only flash up
            // once, then return to its regular style.
            onMove(event) {
              const targetRegionAnimationDuration = getComputedStyle(
                region,
              ).getPropertyValue('--target-region-animation-duration-ms');
              // event.dragged
              event.to.classList.add('layout__region--current-target');
              setTimeout(
                function () {
                  // Remove after animation ends, so the animation plays again
                  // if the Portlet is dragged multiple times.
                  event.to.classList.remove('layout__region--current-target');
                },
                targetRegionAnimationDuration.replace('ms', '') || 1000,
              );
            },
            onAdd(event) {
              // Remove region formatter classes after a Portlet is dropped
              event.target.classList.remove('layout__region--current-target');
            },
            // Fix form handling END.
            onEnd(event) {
              Drupal.homeboxPortletMove(
                event.item,
                event.from,
                event.to,
                event.newIndex,
              );
            },
          });
        },
      );

      // Source region (pull)
      once('sortable_region', '.homebox-add-group__content', context).forEach(
        (region) => {
          Sortable.create(region, {
            draggable: '.homebox-add-portlet',
            ghostClass: 'ui-state-drop',
            group: {
              name: 'homebox-builder',
              put: false,
              pull: 'clone',
              revertClone: true,
            },
            sort: false,
            onEnd: (event) => {
              Drupal.homeboxPortletAdd(
                event.item.dataset.portletType,
                event.to.dataset.layoutRegion,
                event.newIndex,
              );
            },
          });
        },
      );
    },
  };

  /**
   * Callback used in {@link Drupal.behaviors.homeboxPortletDragDrop}.
   *
   * @param {HTMLElement} item
   *   The HTML element representing the repositioned portlet.
   * @param {HTMLElement} from
   *   The HTML element representing the previous parent of item
   * @param {HTMLElement} to
   *   The HTML element representing the current parent of item
   * @param {int} newIndex
   *   The new index / weight in the target region.
   *
   * @internal This method is a callback for homeboxPortletDragDrop and is used
   *  in FunctionalJavascript tests.
   */
  Drupal.homeboxPortletMove = function (item, from, to, newIndex) {
    const toLayoutRegion = to.dataset.layoutRegion;
    // Update layout region:
    const itemLayoutRegionInput = item.querySelector(
      "input[name$='[layout_region]']",
    );
    itemLayoutRegionInput.value = toLayoutRegion;
    // // Update weight:
    const itemLayoutRegionWeightInput = item.querySelector(
      "input[name$='[layout_region_weight]']",
    );
    itemLayoutRegionWeightInput.value = newIndex;

    const itemFormSubmit = item.querySelector(
      "input.form-submit[type='submit'][name='op']",
    );
    itemFormSubmit.click();
  };

  Drupal.homeboxPortletAdd = function (pluginId, region, weight) {
    const portletAddFormPluginId = document.getElementById(
      'add-homebox-portlet-type-id',
    );
    const portletAddFormRegion = document.getElementById(
      'add-homebox-portlet-layout-region',
    );
    const portletAddFormWeight = document.getElementById(
      'add-homebox-portlet-layout-region-weight',
    );
    const portletAddFormSubmit = document.querySelector(
      '.add-homebox-portlet-submit',
    );
    portletAddFormPluginId.value = pluginId;
    portletAddFormRegion.value = region;
    portletAddFormWeight.value = weight;
    portletAddFormSubmit.click();
  };
})(Drupal, Sortable, once, jQuery);

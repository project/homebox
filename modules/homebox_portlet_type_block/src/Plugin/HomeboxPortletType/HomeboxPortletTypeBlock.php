<?php

namespace Drupal\homebox_portlet_type_block\Plugin\HomeboxPortletType;

use Drupal\Core\Block\BlockManager;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\Core\Plugin\Context\LazyContextRepository;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Session\AccountInterface;
use Drupal\homebox\Entity\Homebox;
use Drupal\homebox\Plugin\HomeboxPortletTypeBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a HomeboxPortletType to render blocks.
 *
 * @HomeboxPortletType(
 *   id = "block",
 *   group = @Translation("Drupal"),
 *   admin_label = @Translation("Block Portlet"),
 *   admin_description = @Translation("Renders the selected Drupal Block."),
 *   label = @Translation("Block"),
 *   description = @Translation("Shows the selected Drupal Block."),
 * )
 */
class HomeboxPortletTypeBlock extends HomeboxPortletTypeBase {

  /**
   * The block manager.
   *
   * @var \Drupal\Core\Block\BlockManager
   */
  protected $blockManager;

  /**
   * The context repository.
   *
   * @var \Drupal\Core\Plugin\Context\LazyContextRepository
   */
  protected $contextRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, FormBuilderInterface $formBuilder, Renderer $renderer, ModuleHandlerInterface $module_handler, LayoutPluginManagerInterface $layoutPluginManager, Homebox $homebox = NULL, int $delta = NULL, BlockManager $blockManager, LazyContextRepository $context_repository, EntityTypeManager $entity_type_manager, AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $config_factory, $formBuilder, $renderer, $module_handler, $layoutPluginManager, $homebox, $delta);
    $this->blockManager = $blockManager;
    $this->contextRepository = $context_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('form_builder'),
      $container->get('renderer'),
      $container->get('module_handler'),
      $container->get('plugin.manager.core.layout'),
      NULL,
      NULL,
      $container->get('plugin.manager.block'),
      $container->get('context.repository'),
      $container->get('entity_type.manager'),
      $container->get('current_user'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function buildDisplayContent(): array {
    $build = [];
    $blockId = $this->getThirdPartyConfigurationValue('block_id');
    if (!empty($blockId)) {
      $account = $this->currentUser;
      $blockPlugin = $this->getBlockPlugin($blockId, $account);
      if (!empty($blockPlugin)) {
        $build['block'] = $blockPlugin->build();
        $this->renderer->addCacheableDependency($build, $blockPlugin);
      }
      else {
        $build['error'] = [
          '#theme' => 'status_messages',
          '#message_list' => ['warning' => [$this->t('Block ID "@blockId" not found or you don\'t have access.', ['@blockId' => $blockId])]],
          '#status_headings' => [
            // 'status' => t('Status message'),
            // 'error' => t('Error message'),
            'warning' => $this->t('Warning message'),
          ],
        ];
      }
    }
    else {
      $build['error'] = [
        '#theme' => 'status_messages',
        '#message_list' => ['warning' => [$this->t('No block selected! Block can not be rendered.')]],
        '#status_headings' => [
          // 'status' => t('Status message'),
          // 'error' => t('Error message'),
          'warning' => $this->t('Warning message'),
        ],
      ];
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultThirdPartyConfiguration(): array {
    return parent::defaultThirdPartyConfiguration() + [
      'block_id' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildThirdPartyConfigurationForm(array &$subform, FormStateInterface $subform_state): array {

    $subform['block_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Block'),
      '#options' => $this->listCompatibleBlocksAsOptions(),
      '#default_value' => $this->getThirdPartyConfigurationValue('block_id') ?? NULL,
      '#required' => TRUE,
      '#description' => $this->t('Select the block you wish to display for this portlet.'),
    ];

    // @improve Render the settings from the block here, if one exists!
    // See https://www.drupal.org/project/block_field for the code!
    return $subform;
  }

  /**
   * {@inheritdoc}
   */
  public function validateThirdPartyConfigurationForm(array &$subform, FormStateInterface $subform_state): void {
    // No need for additional validations. Drupal auto-validates the select
    // values kindly.
  }

  /**
   * {@inheritdoc}
   */
  public function submitThirdPartyConfigurationForm(array &$subform, FormStateInterface $subform_state): void {
    $blockId = $subform_state->getValue('block_id');
    $this->setThirdPartyConfigurationValue('block_id', $blockId);
  }

  /**
   * Shows a list of blocks that are compatible to render inside a homebox.
   *
   * @return array
   *   The block definitions.
   */
  protected function listCompatibleBlocks() {
    // Only add blocks which work without any available context.
    $filteredDefinitions = $this->blockManager->getFilteredDefinitions('block_ui', $this->contextRepository->getAvailableContexts());
    // Order by category, and then by admin label.
    $sortedDefinitions = $this->blockManager->getSortedDefinitions($filteredDefinitions);
    // Filter out definitions that are not intended to be placed by the UI.
    $definitions = array_filter(
          $sortedDefinitions, function (array $definition) {
              return empty($definition['_block_ui_hidden']);
          }
    );
    return $definitions;
  }

  /**
   * Lists the compatible blocks as an options array.
   *
   * @return array
   *   An options array as expected by a select form.
   */
  protected function listCompatibleBlocksAsOptions() {
    $blockList = $this->listCompatibleBlocks();
    $blockOptions = [];
    foreach ($blockList as $blockId => $blockValues) {
      $blockOptions[$blockId] = $blockValues['admin_label'];
    }
    return $blockOptions;
  }

  /**
   * Returns the block plugin from the given $blockId.
   *
   * @param string $blockId
   *   The block ID.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account to check access for.
   *
   * @return \Drupal\Core\Block\BlockBase|null
   *   The block plugin or NULL, if no access for $account.
   */
  protected function getBlockPlugin(string $blockId, AccountInterface $account): ?BlockPluginInterface {
    // @improve Provide the block settings here:
    // See https://www.drupal.org/project/block_field for the code!
    $blockConfiguration = [];
    /**
     * @var \Drupal\Core\Block\BlockBase $blockPlugin
     */
    $blockPlugin = $this->blockManager->createInstance($blockId, $blockConfiguration);
    $access_result = $blockPlugin->access($account);
    if (is_object($access_result) && $access_result->isForbidden() || is_bool($access_result) && !$access_result) {
      return NULL;
    }
    else {
      return $blockPlugin;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getPortletAddIconUrl(): ?string {
    $modulePath = $this->moduleHandler->getModule('homebox')->getPath();
    return base_path() . $modulePath . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'portlet-type-icons' . DIRECTORY_SEPARATOR . 'block.svg';
  }

}

<?php

namespace Drupal\homebox_portlet_type_examples\Plugin\HomeboxPortletType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\homebox\Plugin\HomeboxPortletTypeBase;

/**
 * Provides a simple homebox portlet type example to render just a text.
 *
 * @see \Drupal\homebox\Annotation\HomeboxPortletType for details.
 *
 * @HomeboxPortletType(
 *   id = "homebox_portlet_type_examples_locked_settings",
 *   group = @Translation("Homebox Portlet Examples"),
 *   admin_label = @Translation("Locked settings example"),
 *   admin_description = @Translation("Shows an example portlet with locked settings by plugin definition."),
 *   label = @Translation("Locked settings example"),
 *   description = @Translation("Shows an example portlet with locked settings by plugin definition"),
 *   singleton = FALSE,
 *   hideable = TRUE,
 *   draggable = TRUE,
 *   removable = TRUE,
 *   weight = 0,
 * )
 */
class LockedSettings extends HomeboxPortletTypeBase {

  /**
   * {@inheritdoc}
   */
  protected function buildDisplayContent(): array {
    $build = [
      '#plain_text' => $this->t('I am the HomeboxPortletTypeExampleLockedSettings portlet with predefined locked settings!'),
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildThirdPartyConfigurationForm(array &$subform, FormStateInterface $subform_state): array {
    // This example has no own configuration form.
    return $subform;
  }

  /**
   * {@inheritdoc}
   */
  public function validateThirdPartyConfigurationForm(array &$subform, FormStateInterface $subform_state): void {
    // Noting to validate.
  }

  /**
   * {@inheritdoc}
   */
  public function submitThirdPartyConfigurationForm(array &$subform, FormStateInterface $subform_state): void {
    // Noting to submit.
  }

  /**
   * {@inheritDoc}
   */
  public function getPortletAddIconUrl(): ?string {
    $modulePath = $this->moduleHandler->getModule('homebox_portlet_type_examples')->getPath();
    return base_path() . $modulePath . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'portlet-type-icons' . DIRECTORY_SEPARATOR . 'example.svg';
  }

}

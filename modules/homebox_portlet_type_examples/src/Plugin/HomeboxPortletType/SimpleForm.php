<?php

namespace Drupal\homebox_portlet_type_examples\Plugin\HomeboxPortletType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\homebox\Plugin\HomeboxPortletTypeBase;

/**
 * Provides a simple homebox portlet type example to render just a text.
 *
 * @HomeboxPortletType(
 *   id = "homebox_portlet_type_examples_simple_form",
 *   group = @Translation("Homebox Portlet Examples"),
 *   admin_label = @Translation("Simple form example"),
 *   admin_description = @Translation("Shows the indivdual form value."),
 *   label = @Translation("Simple form example"),
 *   description = @Translation("Shows the indivdual form value."),
 * )
 */
class SimpleForm extends HomeboxPortletTypeBase {

  /**
   * {@inheritdoc}
   */
  protected function buildDisplayContent(): array {
    $build = [
      '#plain_text' => $this->t('I am the HomeboxPortletTypeExampleSimple portlet! This is my example form value: @example_form_value', [
        '@example_form_value' => $this->getThirdPartyConfigurationValue('example_form_value') ?? 'Not set',
      ]),
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultThirdPartyConfiguration(): array {
    return parent::defaultThirdPartyConfiguration() + [
      'example_form_value' => 'I am the default value',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildThirdPartyConfigurationForm(array &$subform, FormStateInterface $subform_state): array {

    $subform['example_form_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Example form value'),
      '#default_value' => $this->getThirdPartyConfigurationValue('example_form_value') ?? NULL,
      // Originally, this was set to TRUE, but until
      // https://www.drupal.org/project/homebox/issues/3437895 is not resolved
      // this is set to FALSE.
      '#required' => FALSE,
      '#description' => $this->t('Enter the custom value'),
    ];

    return $subform;
  }

  /**
   * {@inheritdoc}
   */
  public function validateThirdPartyConfigurationForm(array &$subform, FormStateInterface $subform_state): void {
    // Example validation:
    if (strtolower($subform_state->getValue('example_form_value')) == 'wrong') {
      $subform_state->setErrorByName('example_form_value', $this->t('The value may not be "wrong"!'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitThirdPartyConfigurationForm(array &$subform, FormStateInterface $subform_state): void {
    // Noting to submit.
    $this->setThirdPartyConfigurationValue('example_form_value', $subform_state->getValue('example_form_value'));
  }

  /**
   * {@inheritDoc}
   */
  public function getPortletAddIconUrl(): ?string {
    $modulePath = $this->moduleHandler->getModule('homebox_portlet_type_examples')->getPath();
    return base_path() . $modulePath . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'portlet-type-icons' . DIRECTORY_SEPARATOR . 'example.svg';
  }

}

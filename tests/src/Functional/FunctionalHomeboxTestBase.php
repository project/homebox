<?php

namespace Drupal\Tests\homebox\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * The functional homebox test base class.
 *
 * @group homebox
 */
abstract class FunctionalHomeboxTestBase extends BrowserTestBase {
  use HomeboxTestHelper;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'layout_discovery',
    'options',
    'path',
    'test_page_test',
    'homebox',
    'homebox_portlet_type_examples',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A test homebox type.
   *
   * @var \Drupal\homebox\Entity\HomeboxType
   */
  protected $testHomeboxType;

  /**
   * The authenticated user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $authenticatedUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Initialize the homebox test helper:
    $this->testHomeboxType = $this->createTestHomeboxType();
    $this->authenticatedUser = $this->drupalCreateUser([]);
    $this->config('system.site')->set('page.front', '/test-page')->save();
  }

}

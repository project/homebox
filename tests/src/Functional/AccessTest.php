<?php

namespace Drupal\Tests\homebox\Functional;

use Drupal\user\Entity\Role;

/**
 * Functional Homebox tests, to verify the modules functionality.
 *
 * @group homebox
 */
class AccessTest extends FunctionalHomeboxTestBase {

  /**
   * Tests homebox page routes as authenticated user without homebox instances.
   */
  public function testHomeboxPageRoutesAsAuthenticatedUserWithoutInstance() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $authenticatedRole = Role::load('authenticated');

    // Login as authenticated user:
    $this->drupalLogin($this->authenticatedUser);

    // Without any permissions, the authenticated user shouldn't be able to see
    // their homebox:
    $this->drupalGet('/test-homebox');
    $session->statusCodeEquals(403);
    // Give the authenticated role the permission to view their own homebox:
    $authenticatedRole->grantPermission('view own ' . $this->testHomeboxType->id() . ' homebox')->save();
    // Go to the homebox page and check if the page is accessible:
    $this->drupalGet('/test-homebox');
    $session->statusCodeEquals(200);
    $session->pageTextContains('my Test Homebox Type');
    // The "personalize" button should not be displayed, as long, as the user
    // does not have the permission for it:
    $session->elementNotExists('css', '.button--personalize');
    $session->pageTextNotContains('Personalize');
    // They shouldn't be able to access the personalize route either:
    $this->drupalGet('/test-homebox/' . $this->authenticatedUser->id() . '/personalize');
    $session->statusCodeEquals(403);
    // Let's add the appropriate permission and reload the page:
    $authenticatedRole->grantPermission('personalize own ' . $this->testHomeboxType->id() . ' homebox')->save();
    $this->drupalGet('/test-homebox');
    // The "personalize" button should exist now:
    $session->elementExists('css', '.button--personalize');
    $session->pageTextContains('Personalize');
    // See if the button works:
    $page->clickLink('Personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Personalize my Test Homebox Type');
    // Manually access the personalize route, to see if that works as well:
    $this->drupalGet('/test-homebox/' . $this->authenticatedUser->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Personalize my Test Homebox Type');

    // The user shouldn't be able to access the homebox of other users:
    $this->drupalGet('/test-homebox/' . $this->rootUser->id());
    $session->statusCodeEquals(403);

    // If the user has the view any homebox permission, they should be able to:
    $authenticatedRole->grantPermission('view any ' . $this->testHomeboxType->id() . ' homebox')->save();
    $this->drupalGet('/test-homebox/' . $this->rootUser->id());
    $session->statusCodeEquals(200);
    $session->pageTextContains($this->rootUser->getDisplayName() . "'s Test Homebox Type");
    // But they shouldn't be able to see the "personalize" button:
    $session->elementNotExists('css', '.button--personalize');
    $session->pageTextNotContains('Personalize');
    // Nor should they be able to access the personalize route:
    $this->drupalGet('/test-homebox/' . $this->rootUser->id() . '/personalize');
    $session->statusCodeEquals(403);
    // If the user has the personalize any homebox permission, they should be
    // able to see the button:
    $authenticatedRole->grantPermission('personalize any ' . $this->testHomeboxType->id() . ' homebox')->save();
    $this->drupalGet('/test-homebox/' . $this->rootUser->id());
    $session->elementExists('css', '.button--personalize');
    $session->pageTextContains('Personalize');
    // See if the button works:
    $page->clickLink('Personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains("You are personalizing " . $this->rootUser->getDisplayName() . "'s Homebox");
    $session->pageTextContains("Personalize " . $this->rootUser->getDisplayName() . "'s Test Homebox Type");
    // They should also be able to access the route directly:
    $this->drupalGet('/test-homebox/' . $this->rootUser->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains("You are personalizing " . $this->rootUser->getDisplayName() . "'s Homebox");
    $session->pageTextContains("Personalize " . $this->rootUser->getDisplayName() . "'s Test Homebox Type");
  }

  /**
   * Tests the homebox page routes as an admin user without homebox instance.
   */
  public function testHomeboxPageRoutesAsAdminUserWithoutInstance() {
    // We are already logged in as root User here.
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalLogin($this->rootUser);

    // Go to the homebox page and check if the page is accessible.
    $this->drupalGet('/test-homebox');
    $session->statusCodeEquals(200);
    $session->pageTextContains('my Test Homebox Type');

    // The "personalize" button should exist:
    $session->elementExists('css', '.button--personalize');
    $session->pageTextContains('Personalize');

    // See if the button works.
    $page->clickLink('Personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Personalize my Test Homebox Type');

    // Manually access the personalize route, to see if that works as well.
    $this->drupalGet('/test-homebox/' . $this->rootUser->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Personalize my Test Homebox Type');

    // The admin user should be able to access the homebox of other users.
    $this->drupalGet('/test-homebox/' . $this->authenticatedUser->id());
    $session->statusCodeEquals(200);
    $session->pageTextContains($this->authenticatedUser->getDisplayName() . "'s Test Homebox Type");

    // They should also be able to see the "personalize" button:
    $session->elementExists('css', '.button--personalize');
    $session->pageTextContains('Personalize');

    // They should be able to access the personalize route of other users:
    $this->drupalGet('/test-homebox/' . $this->authenticatedUser->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains("You are personalizing " . $this->authenticatedUser->getDisplayName() . "'s Homebox");
    $session->pageTextContains("Personalize " . $this->authenticatedUser->getDisplayName() . "'s Test Homebox Type");
  }

  /**
   * Tests homebox user tab routes as auth. user without homebox instance.
   */
  public function testHomeboxUserTabRoutesAsAuthenticatedUserWithoutInstance() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $authenticatedRole = Role::load('authenticated');

    // Login as authenticated user:
    $this->drupalLogin($this->authenticatedUser);

    // Without any permissions, the authenticated user shouldn't be able to see
    // their homebox:
    $this->drupalGet('/user/' . $this->authenticatedUser->id() . '/homebox/' . $this->testHomeboxType->id());
    $session->statusCodeEquals(403);
    // Give the authenticated role the permission to view their own homebox:
    $authenticatedRole->grantPermission('view own ' . $this->testHomeboxType->id() . ' homebox')->save();
    // Go to the homebox page and check if the page is accessible now:
    $this->drupalGet('/user/' . $this->authenticatedUser->id() . '/homebox/' . $this->testHomeboxType->id());
    $session->statusCodeEquals(200);
    $session->pageTextContains('my Test Homebox Type');
    // The "personalize" button should not be displayed, as long, as the user
    // does not have the permission for it:
    $session->elementNotExists('css', '.button--personalize');
    $session->pageTextNotContains('Personalize');
    // They shouldn't be able to access the personalize route either:
    $this->drupalGet('/user/' . $this->authenticatedUser->id() . '/homebox/' . $this->testHomeboxType->id() . '/personalize');
    $session->statusCodeEquals(403);
    // Let's add the appropriate permission and see if the button appears now:
    $authenticatedRole->grantPermission('personalize own ' . $this->testHomeboxType->id() . ' homebox')->save();
    $this->drupalGet('/user/' . $this->authenticatedUser->id() . '/homebox/' . $this->testHomeboxType->id());
    // The "personalize" button should exist now:
    $session->elementExists('css', '.button--personalize');
    $session->pageTextContains('Personalize');
    // See if the button works:
    $page->clickLink('Personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Personalize my Test Homebox Type');
    // Manually access the personalize route, to see if that works now as well:
    $this->drupalGet('/user/' . $this->authenticatedUser->id() . '/homebox/' . $this->testHomeboxType->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Personalize my Test Homebox Type');

    // The user shouldn't be able to access the homebox of other users:
    $this->drupalGet('/user/' . $this->rootUser->id() . '/homebox/' . $this->testHomeboxType->id());
    $session->statusCodeEquals(403);

    // If the user has the view any homebox permission, they should be able to:
    $authenticatedRole->grantPermission('view any ' . $this->testHomeboxType->id() . ' homebox')->save();
    $this->drupalGet('/test-homebox/' . $this->rootUser->id());
    $session->statusCodeEquals(200);
    $session->pageTextContains($this->rootUser->getDisplayName() . "'s Test Homebox Type");
    // But they shouldn't be able to see the "personalize" button:
    $session->elementNotExists('css', '.button--personalize');
    $session->pageTextNotContains('Personalize');
    // Nor should they be able to access the personalize route:
    $this->drupalGet('/user/' . $this->rootUser->id() . '/homebox/' . $this->testHomeboxType->id() . '/personalize');
    $session->statusCodeEquals(403);
    // If the user has the personalize any homebox permission, they should be
    // able to see the button:
    $authenticatedRole->grantPermission('personalize any ' . $this->testHomeboxType->id() . ' homebox')->save();
    $this->drupalGet('/test-homebox/' . $this->rootUser->id());
    $session->elementExists('css', '.button--personalize');
    $session->pageTextContains('Personalize');
    // See if the button works:
    $page->clickLink('Personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains("You are personalizing " . $this->rootUser->getDisplayName() . "'s Homebox");
    $session->pageTextContains("Personalize " . $this->rootUser->getDisplayName() . "'s Test Homebox Type");
    // They should also be able to access the route directly:
    $this->drupalGet('/user/' . $this->rootUser->id() . '/homebox/' . $this->testHomeboxType->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains("You are personalizing " . $this->rootUser->getDisplayName() . "'s Homebox");
    $session->pageTextContains("Personalize " . $this->rootUser->getDisplayName() . "'s Test Homebox Type");
  }

  /**
   * Tests the homebox page routes as an admin user without homebox instance.
   */
  public function testHomeboxUserTabRoutesAsAdminUserWithoutInstance() {
    // We are already logged in as root User here.
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalLogin($this->rootUser);

    // Go to the homebox page and check if the page is accessible.
    $this->drupalGet('/user/' . $this->rootUser->id() . '/homebox/' . $this->testHomeboxType->id());
    $session->statusCodeEquals(200);
    $session->pageTextContains('my Test Homebox Type');

    // The "personalize" button should exist:
    $session->elementExists('css', '.button--personalize');
    $session->pageTextContains('Personalize');

    // See if the button works.
    $page->clickLink('Personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Personalize my Test Homebox Type');

    // Manually access the personalize route, to see if that works as well.
    $this->drupalGet('/user/' . $this->rootUser->id() . '/homebox/' . $this->testHomeboxType->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Personalize my Test Homebox Type');

    // The admin user should be able to access the homebox of other users.
    $this->drupalGet('/user/' . $this->authenticatedUser->id() . '/homebox/' . $this->testHomeboxType->id());
    $session->statusCodeEquals(200);

    // They should also be able to see the "personalize" button:
    $session->elementExists('css', '.button--personalize');
    $session->pageTextContains('Personalize');

    // They should be able to access the personalize route of other users:
    $this->drupalGet('/user/' . $this->authenticatedUser->id() . '/homebox/' . $this->testHomeboxType->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains("You are personalizing " . $this->authenticatedUser->getDisplayName() . "'s Homebox");
    $session->pageTextContains("Personalize " . $this->authenticatedUser->getDisplayName() . "'s Test Homebox Type");
  }

  /**
   * Tests homebox page routes as authenticated user with homebox instances.
   */
  public function testHomeboxPageRoutesAsAuthenticatedUserWithInstance() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $authenticatedRole = Role::load('authenticated');

    // Programmatically create a homebox for both the authenticated as well
    // as the root user:
    $this->createHomebox($this->authenticatedUser->id(), $this->authenticatedUser->id());
    $this->createHomebox($this->rootUser->id(), $this->rootUser->id());

    // Login as authenticated user:
    $this->drupalLogin($this->authenticatedUser);

    // Without any permissions, the authenticated user shouldn't be able to see
    // their homebox:
    $this->drupalGet('/test-homebox');
    $session->statusCodeEquals(403);
    // Give the authenticated role the permission to view their own homebox:
    $authenticatedRole->grantPermission('view own ' . $this->testHomeboxType->id() . ' homebox')->save();
    // Go to the homebox page and check if the page is accessible:
    $this->drupalGet('/test-homebox');
    $session->statusCodeEquals(200);
    $session->pageTextContains('my Test Homebox Type');
    // The "personalize" button should not be displayed, as long, as the user
    // does not have the permission for it:
    $session->elementNotExists('css', '.button--personalize');
    $session->pageTextNotContains('Personalize');
    // They shouldn't be able to access the personalize route either:
    $this->drupalGet('/test-homebox/' . $this->authenticatedUser->id() . '/personalize');
    $session->statusCodeEquals(403);
    // Let's add the appropriate permission and reload the page:
    $authenticatedRole->grantPermission('personalize own ' . $this->testHomeboxType->id() . ' homebox')->save();
    $this->drupalGet('/test-homebox');
    // The "personalize" button should exist now:
    $session->elementExists('css', '.button--personalize');
    $session->pageTextContains('Personalize');
    // See if the button works:
    $page->clickLink('Personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Personalize my Test Homebox Type');
    // Manually access the personalize route, to see if that works as well:
    $this->drupalGet('/test-homebox/' . $this->authenticatedUser->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Personalize my Test Homebox Type');

    // The user shouldn't be able to access the homebox of other users:
    $this->drupalGet('/test-homebox/' . $this->rootUser->id());
    $session->statusCodeEquals(403);

    // If the user has the view any homebox permission, they should be able to:
    $authenticatedRole->grantPermission('view any ' . $this->testHomeboxType->id() . ' homebox')->save();
    $this->drupalGet('/test-homebox/' . $this->rootUser->id());
    $session->statusCodeEquals(200);
    $session->pageTextContains($this->rootUser->getDisplayName() . "'s Test Homebox Type");
    // But they shouldn't be able to see the "personalize" button:
    $session->elementNotExists('css', '.button--personalize');
    $session->pageTextNotContains('Personalize');
    // Nor should they be able to access the personalize route:
    $this->drupalGet('/test-homebox/' . $this->rootUser->id() . '/personalize');
    $session->statusCodeEquals(403);
    // If the user has the personalize any homebox permission, they should be
    // able to see the button:
    $authenticatedRole->grantPermission('personalize any ' . $this->testHomeboxType->id() . ' homebox')->save();
    $this->drupalGet('/test-homebox/' . $this->rootUser->id());
    $session->elementExists('css', '.button--personalize');
    $session->pageTextContains('Personalize');
    // See if the button works:
    $page->clickLink('Personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains("You are personalizing " . $this->rootUser->getDisplayName() . "'s Homebox");
    $session->pageTextContains("Personalize " . $this->rootUser->getDisplayName() . "'s Test Homebox Type");
    // They should also be able to access the route directly:
    $this->drupalGet('/test-homebox/' . $this->rootUser->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains("You are personalizing " . $this->rootUser->getDisplayName() . "'s Homebox");
    $session->pageTextContains("Personalize " . $this->rootUser->getDisplayName() . "'s Test Homebox Type");
  }

  /**
   * Tests the homebox page routes as an admin user with homebox instance.
   */
  public function testHomeboxPageRoutesAsAdminUserWithInstance() {
    // We are already logged in as root User here.
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Programmatically create a homebox for both the authenticated as well
    // as the root user:
    $this->createHomebox($this->authenticatedUser->id(), $this->authenticatedUser->id());
    $this->createHomebox($this->rootUser->id(), $this->rootUser->id());

    $this->drupalLogin($this->rootUser);

    // Go to the homebox page and check if the page is accessible.
    $this->drupalGet('/test-homebox');
    $session->statusCodeEquals(200);
    $session->pageTextContains('my Test Homebox Type');

    // The "personalize" button should exist:
    $session->elementExists('css', '.button--personalize');
    $session->pageTextContains('Personalize');

    // See if the button works.
    $page->clickLink('Personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Personalize my Test Homebox Type');

    // Manually access the personalize route, to see if that works as well.
    $this->drupalGet('/test-homebox/' . $this->rootUser->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Personalize my Test Homebox Type');

    // The admin user should be able to access the homebox of other users.
    $this->drupalGet('/test-homebox/' . $this->authenticatedUser->id());
    $session->statusCodeEquals(200);
    $session->pageTextContains($this->authenticatedUser->getDisplayName() . "'s Test Homebox Type");

    // They should also be able to see the "personalize" button:
    $session->elementExists('css', '.button--personalize');
    $session->pageTextContains('Personalize');

    // They should be able to access the personalize route of other users:
    $this->drupalGet('/test-homebox/' . $this->authenticatedUser->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains("You are personalizing " . $this->authenticatedUser->getDisplayName() . "'s Homebox");
    $session->pageTextContains("Personalize " . $this->authenticatedUser->getDisplayName() . "'s Test Homebox Type");
  }

  /**
   * Tests homebox user tab routes as auth. user with homebox instance.
   */
  public function testHomeboxUserTabRoutesAsAuthenticatedUserWithInstance() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $authenticatedRole = Role::load('authenticated');

    // Programmatically create a homebox for both the authenticated as well
    // as the root user:
    $this->createHomebox($this->authenticatedUser->id(), $this->authenticatedUser->id());
    $this->createHomebox($this->rootUser->id(), $this->rootUser->id());

    // Login as authenticated user:
    $this->drupalLogin($this->authenticatedUser);

    // Without any permissions, the authenticated user shouldn't be able to see
    // their homebox:
    $this->drupalGet('/user/' . $this->authenticatedUser->id() . '/homebox/' . $this->testHomeboxType->id());
    $session->statusCodeEquals(403);
    // Give the authenticated role the permission to view their own homebox:
    $authenticatedRole->grantPermission('view own ' . $this->testHomeboxType->id() . ' homebox')->save();
    // Go to the homebox page and check if the page is accessible now:
    $this->drupalGet('/user/' . $this->authenticatedUser->id() . '/homebox/' . $this->testHomeboxType->id());
    $session->statusCodeEquals(200);
    $session->pageTextContains('my Test Homebox Type');
    // The "personalize" button should not be displayed, as long, as the user
    // does not have the permission for it:
    $session->elementNotExists('css', '.button--personalize');
    $session->pageTextNotContains('Personalize');
    // They shouldn't be able to access the personalize route either:
    $this->drupalGet('/user/' . $this->authenticatedUser->id() . '/homebox/' . $this->testHomeboxType->id() . '/personalize');
    $session->statusCodeEquals(403);
    // Let's add the appropriate permission and see if the button appears now:
    $authenticatedRole->grantPermission('personalize own ' . $this->testHomeboxType->id() . ' homebox')->save();
    $this->drupalGet('/user/' . $this->authenticatedUser->id() . '/homebox/' . $this->testHomeboxType->id());
    // The "personalize" button should exist now:
    $session->elementExists('css', '.button--personalize');
    $session->pageTextContains('Personalize');
    // See if the button works:
    $page->clickLink('Personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Personalize my Test Homebox Type');
    // Manually access the personalize route, to see if that works now as well:
    $this->drupalGet('/user/' . $this->authenticatedUser->id() . '/homebox/' . $this->testHomeboxType->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Personalize my Test Homebox Type');

    // The user shouldn't be able to access the homebox of other users:
    $this->drupalGet('/user/' . $this->rootUser->id() . '/homebox/' . $this->testHomeboxType->id());
    $session->statusCodeEquals(403);

    // If the user has the view any homebox permission, they should be able to:
    $authenticatedRole->grantPermission('view any ' . $this->testHomeboxType->id() . ' homebox')->save();
    $this->drupalGet('/test-homebox/' . $this->rootUser->id());
    $session->statusCodeEquals(200);
    $session->pageTextContains($this->rootUser->getDisplayName() . "'s Test Homebox Type");
    // But they shouldn't be able to see the "personalize" button:
    $session->elementNotExists('css', '.button--personalize');
    $session->pageTextNotContains('Personalize');
    // Nor should they be able to access the personalize route:
    $this->drupalGet('/user/' . $this->rootUser->id() . '/homebox/' . $this->testHomeboxType->id() . '/personalize');
    $session->statusCodeEquals(403);
    // If the user has the personalize any homebox permission, they should be
    // able to see the button:
    $authenticatedRole->grantPermission('personalize any ' . $this->testHomeboxType->id() . ' homebox')->save();
    $this->drupalGet('/test-homebox/' . $this->rootUser->id());
    $session->elementExists('css', '.button--personalize');
    $session->pageTextContains('Personalize');
    // See if the button works:
    $page->clickLink('Personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains("You are personalizing " . $this->rootUser->getDisplayName() . "'s Homebox");
    $session->pageTextContains("Personalize " . $this->rootUser->getDisplayName() . "'s Test Homebox Type");
    // They should also be able to access the route directly:
    $this->drupalGet('/user/' . $this->rootUser->id() . '/homebox/' . $this->testHomeboxType->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains("You are personalizing " . $this->rootUser->getDisplayName() . "'s Homebox");
    $session->pageTextContains("Personalize " . $this->rootUser->getDisplayName() . "'s Test Homebox Type");
  }

  /**
   * Tests the homebox page routes as an admin user with homebox instance.
   */
  public function testHomeboxUserTabRoutesAsAdminUserWithInstance() {
    // We are already logged in as root User here.
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Programmatically create a homebox for both the authenticated as well
    // as the root user:
    $this->createHomebox($this->authenticatedUser->id(), $this->authenticatedUser->id());
    $this->createHomebox($this->rootUser->id(), $this->rootUser->id());

    $this->drupalLogin($this->rootUser);

    // Go to the homebox page and check if the page is accessible.
    $this->drupalGet('/user/' . $this->rootUser->id() . '/homebox/' . $this->testHomeboxType->id());
    $session->statusCodeEquals(200);
    $session->pageTextContains('my Test Homebox Type');

    // The "personalize" button should exist:
    $session->elementExists('css', '.button--personalize');
    $session->pageTextContains('Personalize');

    // See if the button works.
    $page->clickLink('Personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Personalize my Test Homebox Type');

    // Manually access the personalize route, to see if that works as well.
    $this->drupalGet('/user/' . $this->rootUser->id() . '/homebox/' . $this->testHomeboxType->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Personalize my Test Homebox Type');

    // The admin user should be able to access the homebox of other users.
    $this->drupalGet('/user/' . $this->authenticatedUser->id() . '/homebox/' . $this->testHomeboxType->id());
    $session->statusCodeEquals(200);

    // They should also be able to see the "personalize" button:
    $session->elementExists('css', '.button--personalize');
    $session->pageTextContains('Personalize');

    // They should be able to access the personalize route of other users:
    $this->drupalGet('/user/' . $this->authenticatedUser->id() . '/homebox/' . $this->testHomeboxType->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains("You are personalizing " . $this->authenticatedUser->getDisplayName() . "'s Homebox");
    $session->pageTextContains("Personalize " . $this->authenticatedUser->getDisplayName() . "'s Test Homebox Type");
  }

  /**
   * Tests the homebox direct routes as an authenticated user.
   */
  public function testHomeboxDirectRoutesAsAuthenticatedUser() {
    $session = $this->assertSession();
    $authenticatedRole = Role::load('authenticated');
    // Programmatically create a homebox for both the authenticated as well
    // as the root user:
    $authenticatedHomebox = $this->createHomebox($this->authenticatedUser->id(), $this->authenticatedUser->id());
    $rootHomebox = $this->createHomebox($this->rootUser->id(), $this->rootUser->id());

    // Login as authenticated user:
    $this->drupalLogin($this->authenticatedUser);

    // Without any permissions, the authenticated user shouldn't be able to see
    // their homebox:
    $this->drupalGet('/admin/homebox/' . $authenticatedHomebox->id());
    $session->statusCodeEquals(403);
    // Give the authenticated role the permission to view their own homebox:
    $authenticatedRole->grantPermission('view own ' . $this->testHomeboxType->id() . ' homebox')->save();
    // Go to the homebox page and check if the page is accessible now:
    $this->drupalGet('/admin/homebox/' . $authenticatedHomebox->id());
    $session->statusCodeEquals(200);
    // @improve Through the direct url, the homebox label is not shown for the
    // default view some reason, see here:
    // https://www.drupal.org/project/homebox/issues/3439655:
    // $session->pageTextContains('Personalize my Test Homebox Type');
    // They shouldn't be able to access the personalize route either:
    $this->drupalGet('/admin/homebox/' . $authenticatedHomebox->id() . '/personalize');
    $session->statusCodeEquals(403);
    // Let's add the appropriate permission and see if the button appears now:
    $authenticatedRole->grantPermission('personalize own ' . $this->testHomeboxType->id() . ' homebox')->save();
    // Access the personalize route, to see if that works now as well:
    $this->drupalGet('/admin/homebox/' . $authenticatedHomebox->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Personalize my Test Homebox Type');

    // The user shouldn't be able to access the homebox of other users:
    $this->drupalGet('/admin/homebox/' . $rootHomebox->id());
    $session->statusCodeEquals(403);

    // If the user has the view any homebox permission, they should be able to:
    $authenticatedRole->grantPermission('view any ' . $this->testHomeboxType->id() . ' homebox')->save();
    $this->drupalGet('/admin/homebox/' . $rootHomebox->id());
    $session->statusCodeEquals(200);

    // @improve Through the direct url, the homebox label is not shown for the
    // default view some reason, see here:
    // https://www.drupal.org/project/homebox/issues/3439655:
    // @codingStandardsIgnoreStart
    // $session->pageTextContains($this->rootUser->getDisplayName() . "'s Test Homebox Type");
    // @codingStandardsIgnoreEnd
    // They should not be able to access the personalize route:
    $this->drupalGet('/admin/homebox/' . $rootHomebox->id() . '/personalize');
    $session->statusCodeEquals(403);
    // If the user has the personalize any homebox permission, they should be
    // able to see the button:
    $authenticatedRole->grantPermission('personalize any ' . $this->testHomeboxType->id() . ' homebox')->save();
    // They should be able to access the route now:
    $this->drupalGet('/admin/homebox/' . $rootHomebox->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains("You are personalizing " . $this->rootUser->getDisplayName() . "'s Homebox");
    $session->pageTextContains("Personalize " . $this->rootUser->getDisplayName() . "'s Test Homebox Type");
  }

  /**
   * Tests the homebox direct routes as an admin user.
   */
  public function testHomeboxDirectRoutesAsAdminUser() {
    // We are already logged in as root User here.
    $session = $this->assertSession();
    $this->drupalLogin($this->rootUser);

    // Programmatically create a homebox for both the authenticated as well
    // as the root user:
    $authenticatedHomebox = $this->createHomebox($this->authenticatedUser->id(), $this->authenticatedUser->id());
    $rootHomebox = $this->createHomebox($this->rootUser->id(), $this->rootUser->id());

    // Go to the homebox page and check if the page is accessible.
    $this->drupalGet('/admin/homebox/' . $rootHomebox->id());
    $session->statusCodeEquals(200);
    // @improve Through the direct url, the homebox label is not shown for the
    // default view some reason, see here:
    // https://www.drupal.org/project/homebox/issues/3439655:
    // $session->pageTextContains('Personalize my Test Homebox Type');
    // Manually access the personalize route, to see if that works:
    $this->drupalGet('/admin/homebox/' . $rootHomebox->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Personalize my Test Homebox Type');

    // The admin user should be able to access the homebox of other users.
    $this->drupalGet('/admin/homebox/' . $authenticatedHomebox->id());
    $session->statusCodeEquals(200);
    // @improve Through the direct url, the homebox label is not shown for the
    // default view some reason, see here:
    // https://www.drupal.org/project/homebox/issues/3439655:
    // @codingStandardsIgnoreStart
    // $session->pageTextContains($this->authenticatedUser->getDisplayName() . "'s Test Homebox Type");
    // @codingStandardsIgnoreEnd
    // They should be able to access the personalize route of other users:
    $this->drupalGet('/admin/homebox/' . $authenticatedHomebox->id() . '/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains("You are personalizing " . $this->authenticatedUser->getDisplayName() . "'s Homebox");
    $session->pageTextContains("Personalize " . $this->authenticatedUser->getDisplayName() . "'s Test Homebox Type");
  }

  /**
   * Tests all kinds of homebox preset access as an authenticated user.
   */
  public function testHomeboxPresetAccessAsAuthenticated() {
    $session = $this->assertSession();
    // Login as the authenticated user:
    $this->drupalLogin($this->authenticatedUser);
    // Go to the test-homebox preset page (At first, this is always the homebox
    // with the id 1.). The user should not have access:
    $this->drupalGet('/admin/homebox/1');
    $session->statusCodeEquals(403);

    // Neither on the personalize route:
    $this->drupalGet('/admin/homebox/1/personalize');
    $session->statusCodeEquals(403);

    // Go to the test-homebox presets page and see if the user has access:
    $this->drupalGet('/admin/structure/homebox_type/' . $this->testHomeboxType->id() . '/presets');
    $session->statusCodeEquals(403);

    // Create a homebox owned by the authenticated user, and see, if they can
    // set it as the preset for the test homebox type:
    $userHomebox = $this->createHomebox($this->authenticatedUser->id(), $this->authenticatedUser->id());
    $this->drupalGet('/admin/homebox/' . $userHomebox->id() . '/set-default-preset');
    $session->statusCodeEquals(403);

    // See if after the user has created their own homebox, they still can not
    // access the preset:
    $this->drupalGet('/admin/homebox/1');
    $session->statusCodeEquals(403);

    // Give the authenticated user the permission to administer the presets:
    $authenticatedRole = Role::load('authenticated');
    $authenticatedRole->grantPermission('administer ' . $this->testHomeboxType->id() . ' presets')->save();
    // Now the user should be able to access the presets page:
    // They should also be able to access the test-homebox preset page now:
    $this->drupalGet('/admin/homebox/1');
    $session->statusCodeEquals(200);
    // @improve Through the direct url, the homebox label is not shown for the
    // default view some reason, see here:
    // https://www.drupal.org/project/homebox/issues/3439655:
    // $session->pageTextContains('Test Homebox Type default preset');
    // Now they should also be able to access the personalize page:
    $this->drupalGet('/admin/homebox/1/personalize');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Personalize the Test Homebox Type default preset');

    $this->drupalGet('/admin/structure/homebox_type/' . $this->testHomeboxType->id() . '/presets');
    $session->statusCodeEquals(200);
    // The user should also be able to set the preset now:
    $this->drupalGet('/admin/homebox/' . $userHomebox->id() . '/set-default-preset');
    $session->statusCodeEquals(200);
    // But not other people's homeboxes:
    $adminHomebox = $this->createHomebox($this->rootUser->id(), $this->rootUser->id());
    $this->drupalGet('/test-homebox/' . $this->rootUser->id());
    $session->statusCodeEquals(403);

    // Also not through the user tab:
    $this->drupalGet('/user/' . $this->rootUser->id() . '/homebox/' . $this->testHomeboxType->id());
    $session->statusCodeEquals(403);

    // And not accessing the homebox directly:
    $this->drupalGet('/admin/homebox/' . $adminHomebox->id());
    $session->statusCodeEquals(403);
  }

  /**
   * Tests the homebox portlet add route an authenticated user without instance.
   */
  public function testHomeboxPortletAddRouteAsAuthenticatedWithoutInstance() {
    $session = $this->assertSession();
    $authenticatedRole = Role::load('authenticated');

    // Login as authenticated user:
    $this->drupalLogin($this->authenticatedUser);

    // Without any permissions, the authenticated user shouldn't be able to add
    // a portlet, to their homebox:
    $this->drupalGet('/ajax/homebox/' . $this->testHomeboxType->id() . '/' . $this->authenticatedUser->id() . '/portlet/add');
    $session->statusCodeEquals(403);
    // Give the authenticated role the permission to view their own homebox:
    $authenticatedRole->grantPermission('view own ' . $this->testHomeboxType->id() . ' homebox')->save();
    // Check the add portlet route again, it still shouldn't be accessible:
    $this->drupalGet('/ajax/homebox/' . $this->testHomeboxType->id() . '/' . $this->authenticatedUser->id() . '/portlet/add');
    $session->statusCodeEquals(403);
    // Let's add the appropriate permission and see if they can now add a
    // portlet:
    $authenticatedRole->grantPermission('personalize own ' . $this->testHomeboxType->id() . ' homebox')->save();
    // Access the personalize route, to see if that works now as well:
    $this->drupalGet('/ajax/homebox/' . $this->testHomeboxType->id() . '/' . $this->authenticatedUser->id() . '/portlet/add');
    $session->statusCodeEquals(200);

    // The user shouldn't be able to add a portlet for other users:
    $this->drupalGet('/ajax/homebox/' . $this->testHomeboxType->id() . '/' . $this->rootUser->id() . '/portlet/add');
    $session->statusCodeEquals(403);

    // If the user has the view any homebox permission, they should still not
    // be able to add a portlet:
    $authenticatedRole->grantPermission('view any ' . $this->testHomeboxType->id() . ' homebox')->save();
    $this->drupalGet('/ajax/homebox/' . $this->testHomeboxType->id() . '/' . $this->rootUser->id() . '/portlet/add');
    $session->statusCodeEquals(403);
    // If the user has the personalize any homebox permission, they should be
    // able to add a portlet:
    $authenticatedRole->grantPermission('personalize any ' . $this->testHomeboxType->id() . ' homebox')->save();
    // They should be able to access the route now:
    $this->drupalGet('/ajax/homebox/' . $this->testHomeboxType->id() . '/' . $this->rootUser->id() . '/portlet/add');
    $session->statusCodeEquals(200);
  }

  /**
   * Tests the homebox portlet add route an authenticated user with instance.
   */
  public function testHomeboxPortletAddRouteAsAuthenticatedWithInstance() {
    $session = $this->assertSession();
    $authenticatedRole = Role::load('authenticated');

    // Programmatically create a homebox for both the authenticated as well
    // as the root user:
    $this->createHomebox($this->authenticatedUser->id(), $this->authenticatedUser->id());
    $this->createHomebox($this->rootUser->id(), $this->rootUser->id());

    // Login as authenticated user:
    $this->drupalLogin($this->authenticatedUser);

    // Without any permissions, the authenticated user shouldn't be able to add
    // a portlet, to their homebox:
    $this->drupalGet('/ajax/homebox/' . $this->testHomeboxType->id() . '/' . $this->authenticatedUser->id() . '/portlet/add');
    $session->statusCodeEquals(403);
    // Give the authenticated role the permission to view their own homebox:
    $authenticatedRole->grantPermission('view own ' . $this->testHomeboxType->id() . ' homebox')->save();
    // Check the add portlet route again, it still shouldn't be accessible:
    $this->drupalGet('/ajax/homebox/' . $this->testHomeboxType->id() . '/' . $this->authenticatedUser->id() . '/portlet/add');
    $session->statusCodeEquals(403);
    // Let's add the appropriate permission and see if they can now add a
    // portlet:
    $authenticatedRole->grantPermission('personalize own ' . $this->testHomeboxType->id() . ' homebox')->save();
    // Access the personalize route, to see if that works now as well:
    $this->drupalGet('/ajax/homebox/' . $this->testHomeboxType->id() . '/' . $this->authenticatedUser->id() . '/portlet/add');
    $session->statusCodeEquals(200);

    // The user shouldn't be able to add a portlet for other users:
    $this->drupalGet('/ajax/homebox/' . $this->testHomeboxType->id() . '/' . $this->rootUser->id() . '/portlet/add');
    $session->statusCodeEquals(403);

    // If the user has the view any homebox permission, they should still not
    // be able to add a portlet:
    $authenticatedRole->grantPermission('view any ' . $this->testHomeboxType->id() . ' homebox')->save();
    $this->drupalGet('/ajax/homebox/' . $this->testHomeboxType->id() . '/' . $this->rootUser->id() . '/portlet/add');
    $session->statusCodeEquals(403);
    // If the user has the personalize any homebox permission, they should be
    // able to add a portlet:
    $authenticatedRole->grantPermission('personalize any ' . $this->testHomeboxType->id() . ' homebox')->save();
    // They should be able to access the route now:
    $this->drupalGet('/ajax/homebox/' . $this->testHomeboxType->id() . '/' . $this->rootUser->id() . '/portlet/add');
    $session->statusCodeEquals(200);
  }

}

<?php

namespace Drupal\Tests\homebox\Functional;

use Drupal\homebox\Entity\Homebox;
use Drupal\homebox\Entity\HomeboxType;

/**
 * A Test Helper class for Homebox Tests.
 */
trait HomeboxTestHelper {

  /**
   * Creates a homebox type.
   */
  public function createTestHomeboxType() {
    $homeboxType = HomeboxType::create([
      'status' => TRUE,
      'id' => 'test_homebox_type',
      'label' => 'Test Homebox Type',
      'pageEnabled' => TRUE,
      'pagePath' => '/test-homebox',
      'userTabEnabled' => TRUE,
      'showTitle' => TRUE,
      'layoutIds' => ['homebox_default' => 'homebox_default'],
      'homeboxTypePortletTypesSettings' => [
        "homebox_portlet_type_examples_simple_form" => [
          "label" => '',
          "description" => '',
          "status" => TRUE,
          "singleton" => FALSE,
          "draggable" => TRUE,
          "hideable" => TRUE,
          "removable" => TRUE,
          "weight" => 0,
        ],
        "homebox_portlet_type_examples_locked_settings" => [
          "label" => '',
          "description" => '',
          "status" => TRUE,
          "singleton" => FALSE,
          "draggable" => TRUE,
          "hideable" => TRUE,
          "removable" => TRUE,
          "weight" => 1,
        ],
        "homebox_portlet_type_examples_simple" => [
          "label" => '',
          "description" => '',
          "status" => TRUE,
          "singleton" => FALSE,
          "draggable" => TRUE,
          "hideable" => TRUE,
          "removable" => TRUE,
          "weight" => 2,
        ],
      ],
    ]);
    $homeboxType->save();
    \Drupal::service('router.builder')->rebuild();
    return $homeboxType;
  }

  /**
   * Creates a homebox.
   */
  public function createHomebox(int $ownerId, int $creatorId, bool $isPreset = FALSE) {
    $homebox = Homebox::create([
      'type' => 'test_homebox_type',
      'uid' => $ownerId,
      'layoutId' => 'homebox_default',
      'creator' => $creatorId,
      'isPreset' => $isPreset,
      'portlets' => [],
    ]);
    $homebox->save();
    \Drupal::service('router.builder')->rebuild();
    return $homebox;
  }

}

<?php

namespace Drupal\homebox\Controller;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\homebox\Entity\Homebox;
use Drupal\homebox\Entity\HomeboxInterface;
use Drupal\homebox\Entity\HomeboxTypeInterface;
use Drupal\homebox\HomeboxPortletTypePluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class HomeboxController.
 *
 *  Returns responses for Homebox  routes.
 */
class HomeboxController extends ControllerBase {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * The Homebox portlet type plugin manager.
   *
   * @var \Drupal\homebox\HomeboxPortletTypePluginManager
   */
  protected $homeboxPortletTypePluginManager;

  /**
   * Constructs a HomeboxController object.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\homebox\HomeboxPortletTypePluginManager $homeboxPortletTypePluginManager
   *   The Homebox portlet type plugin manager.
   */
  public function __construct(RendererInterface $renderer, HomeboxPortletTypePluginManager $homeboxPortletTypePluginManager) {
    $this->renderer = $renderer;
    $this->homeboxPortletTypePluginManager = $homeboxPortletTypePluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer'),
      $container->get('plugin.manager.homebox_portlet_type'),
    );
  }

  /**
   * The build function for rendering the homebox page.
   *
   * @param \Drupal\homebox\Entity\HomeboxTypeInterface $homebox_type
   *   The homebox type of the homebox.
   * @param \Drupal\Core\Session\AccountInterface $owner
   *   The owner of the homebox.
   */
  public function buildPage(HomeboxTypeInterface $homebox_type, AccountInterface $owner = NULL) {
    return $this->build($homebox_type, $owner);
  }

  /**
   * The build function for rendering the homebox user tab.
   *
   * @param \Drupal\homebox\Entity\HomeboxTypeInterface $homebox_type
   *   The homebox type of the homebox.
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The owner of the homebox.
   */
  public function buildUserTab(HomeboxTypeInterface $homebox_type, AccountInterface $user) {
    // In this case we need to name the $owner parameter as $user to match the
    // Drupal core user path requirements!
    $owner = $user;
    return $this->build($homebox_type, $owner);
  }

  /**
   * The build function for rendering the homebox.
   *
   * @param \Drupal\homebox\Entity\HomeboxTypeInterface $homebox_type
   *   The homebox type of the homebox.
   * @param \Drupal\Core\Session\AccountInterface $owner
   *   The owner of the homebox.
   */
  public function build(HomeboxTypeInterface $homebox_type, AccountInterface $owner = NULL) {
    if ($owner === NULL) {
      // If no explicit account parameter is given, fall back to the current
      // user:
      $owner = $this->currentUser();
    }
    $homebox = Homebox::retrieveInstance($homebox_type, $owner);
    return $this->entityTypeManager()->getViewBuilder('homebox')->view($homebox);
  }

  /**
   * The title callback function for the homebox page title.
   *
   * @param \Drupal\homebox\Entity\HomeboxTypeInterface $homebox_type
   *   The homebox type of the homebox.
   * @param \Drupal\Core\Session\AccountInterface $owner
   *   The owner of the homebox.
   */
  public function buildPageTitle(HomeboxTypeInterface $homebox_type, AccountInterface $owner = NULL) {
    if ($owner === NULL) {
      // If no explicit account parameter is given, fall back to the current
      // user:
      $owner = $this->currentUser();
    }
    if ($homebox_type->getShowTitle()) {
      $homebox = Homebox::retrieveInstance($homebox_type, $owner);
      return $homebox->viewLabel();
    }
    else {
      // Show no title if disabled.
      return NULL;
    }
  }

  /**
   * The build function for rendering the homebox page personalize.
   *
   * @param \Drupal\homebox\Entity\HomeboxInterface $homebox
   *   The homebox.
   */
  public function buildAdminPersonalize(HomeboxInterface $homebox) {
    $build = $this->entityTypeManager()->getViewBuilder('homebox')->view($homebox, 'personalize');
    return $build;
  }

  /**
   * The build function for rendering the homebox page personalize.
   *
   * @param \Drupal\homebox\Entity\HomeboxTypeInterface $homebox_type
   *   The homebox type of the homebox.
   * @param \Drupal\Core\Session\AccountInterface $owner
   *   The owner of the homebox.
   */
  public function buildPagePersonalize(HomeboxTypeInterface $homebox_type, AccountInterface $owner = NULL) {
    if ($owner === NULL) {
      // If no explicit account parameter is given, fall back to the current
      // user:
      $owner = $this->currentUser();
    }
    return $this->buildPersonalize($homebox_type, $owner);
  }

  /**
   * The build function for rendering the homebox user tab personalize.
   *
   * @param \Drupal\homebox\Entity\HomeboxTypeInterface $homebox_type
   *   The homebox type of the homebox.
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The owner of the homebox.
   */
  public function buildUserTabPersonalize(HomeboxTypeInterface $homebox_type, AccountInterface $user = NULL): array {
    // In this case we need to name the $owner parameter as $user to match the
    // Drupal core user path requirements!
    $owner = $user;
    return $this->buildPersonalize($homebox_type, $owner);
  }

  /**
   * The build function for rendering the homebox personalize.
   *
   * @param \Drupal\homebox\Entity\HomeboxTypeInterface $homebox_type
   *   The homebox type of the homebox.
   * @param \Drupal\Core\Session\AccountInterface $owner
   *   The owner of the homebox.
   */
  public function buildPersonalize(HomeboxTypeInterface $homebox_type, AccountInterface $owner = NULL): array {
    if ($owner === NULL) {
      // If no explicit account parameter is given, fall back to the current
      // user:
      $owner = $this->currentUser();
    }
    $homebox = Homebox::retrieveInstance($homebox_type, $owner);
    return $this->entityTypeManager()->getViewBuilder('homebox')->view($homebox, 'personalize');
  }

  /**
   * Build the "Add portlet" render array.
   *
   * @param \Drupal\homebox\Entity\HomeboxTypeInterface $homebox_type
   *   The homebox type of the homebox.
   * @param \Drupal\Core\Session\AccountInterface $owner
   *   The owner of the homebox.
   */
  public function buildPersonalizePortletAdd(HomeboxTypeInterface $homebox_type, AccountInterface $owner = NULL) {
    if ($owner === NULL) {
      // If no explicit account parameter is given, fall back to the current
      // user:
      $owner = $this->currentUser();
    }
    $homebox = Homebox::retrieveInstance($homebox_type, $owner);

    /**
     * @var \Drupal\homebox\Entity\HomeboxTypeInterface $homeboxType
     */
    $homeboxType = $homebox->getHomeboxType();

    $homeboxPorletTypesDefinitions = $this->homeboxPortletTypePluginManager->getDefinitions();
    $portletTypesGrouped = [];
    foreach ($homeboxPorletTypesDefinitions as $plugin_id => $homeboxPortletTypeDefinition) {
      $currentHomeboxPortletTypeSettings = $homeboxType->getHomeboxTypePortletTypeSettings($plugin_id);
      if (empty($currentHomeboxPortletTypeSettings['status'])) {
        continue;
      }
      if (!empty($currentHomeboxPortletTypeSettings['singleton']) && $homebox->hasPortletTypeInstance($plugin_id)) {
        // @note: We could instead mark these as disabled?
        continue;
      }

      $pluginInstance = $this->homeboxPortletTypePluginManager->createInstance($plugin_id, $homeboxPortletTypeDefinition);
      $group = (string) $homeboxPortletTypeDefinition['group'];
      $weight = (string) $currentHomeboxPortletTypeSettings['weight'] ?: 0;
      $portletTypesGrouped[$group][$plugin_id] = [
        '#theme' => 'homebox_add_portlet',
        '#label' => $currentHomeboxPortletTypeSettings['label'] ?: $homeboxPortletTypeDefinition['label'] ?: '',
        '#description' => $currentHomeboxPortletTypeSettings['description'] ?: $homeboxPortletTypeDefinition['label'] ?: '',
        '#group' => $group,
        '#weight' => $weight,
        '#plugin_id' => $plugin_id,
        '#portlet_type_definition' => $homeboxPortletTypeDefinition,
        '#add_icon_url' => $pluginInstance->getPortletAddIconUrl(),
        '#type_settings' => $currentHomeboxPortletTypeSettings,
        '#attributes' => [
          'data-portlet-type' => $plugin_id,
        ],
      ];
    }

    // Sort the portlets within the groups by weight:
    foreach ($portletTypesGrouped as $group => &$portlets) {
      uasort($portlets, function ($a, $b) {
        return SortArray::sortByWeightProperty($a, $b);
      });
    }

    // Sort the groups naturally.
    // @improve: Allow to sort groups. Currently this needs to be done in
    // HOOK_preprocess_homebox_add_portlet_groups().
    ksort($portletTypesGrouped, SORT_NATURAL);

    // Important: To make sortableJS work, we need two levels of wrappers around
    // the portlets!
    $build = [
      '#theme' => 'homebox_add_portlet_groups',
      '#groups' => $portletTypesGrouped,
    ];

    return $build;
  }

  /**
   * The _title_callback for the set_default_preset route.
   *
   * @param \Drupal\homebox\Entity\HomeboxInterface $homebox
   *   The homebox to set as default preset.
   *
   * @return string
   *   The page title.
   */
  public function defaultPresetTitle(HomeboxInterface $homebox) {
    return $this->t(
      'Set %homebox Homebox as the default preset for the %homeboxTypeLabel homebox type.', [
        '%homebox' => $homebox->label(),
        '%homeboxTypeLabel' => $homebox->getHomeboxType()->label(),
      ]
    );
  }

  /**
   * Check access to the homebox.page.
   *
   * CAREFUL! "$homebox_type" needs to be written as the route defines it!
   * In this case "homebox_type", if we use "$homebox_type" itself, the route
   * can not be resolved!
   *
   * @param \Drupal\homebox\Entity\HomeboxTypeInterface $homebox_type
   *   Homebox type.
   * @param \Drupal\Core\Session\AccountInterface $owner
   *   The homebox owner user entity.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   Access result.
   */
  public function accessPage(HomeboxTypeInterface $homebox_type, AccountInterface $owner = NULL) {
    if (!$homebox_type->getPageEnabled()) {
      return AccessResult::forbidden()->addCacheableDependency($homebox_type);
    }
    if ($owner === NULL) {
      // If no explicit account parameter is given, fallback to
      // current users homebox:
      $owner = $this->currentUser();
    }

    $homebox = Homebox::retrieveInstance($homebox_type, $owner);
    return $homebox->access('view', $this->currentUser(), TRUE);
  }

  /**
   * Check access to the homebox.user_tab.
   *
   * CAREFUL! "$homebox_type" needs to be written as the route defines it!
   * In this case "homebox_type", if we use "$homebox_type" itself, the route
   * can not be resolved!
   *
   * @param \Drupal\homebox\Entity\HomeboxTypeInterface $homebox_type
   *   Homebox entity.
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The homebox owner user entity.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   Access result.
   */
  public function accessUserTab(HomeboxTypeInterface $homebox_type, AccountInterface $user = NULL) {
    if (!$homebox_type->getUserTabEnabled()) {
      return AccessResult::forbidden()->addCacheableDependency($homebox_type);
    }
    if ($user === NULL) {
      // If no explicit account parameter is given, fallback to
      // current users homebox:
      $user = $this->currentUser();
    }
    $homebox = Homebox::retrieveInstance($homebox_type, $user);
    // Check, if the current user has access to the homebox:
    return $homebox->access('view', $this->currentUser(), TRUE);
  }

  /**
   * Check access to the homebox.page.personalize.
   *
   * CAREFUL! "$homebox_type" needs to be written as the route defines it!
   * In this case "homebox_type", if we use "$homebox_type" itself, the route
   * can not be resolved!
   *
   * @param \Drupal\homebox\Entity\HomeboxTypeInterface $homebox_type
   *   Homebox type.
   * @param \Drupal\Core\Session\AccountInterface $owner
   *   The homebox owner user entity.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   Access result.
   */
  public function accessPagePersonalize(HomeboxTypeInterface $homebox_type, AccountInterface $owner = NULL) {
    if (!$homebox_type->getPageEnabled()) {
      return AccessResult::forbidden()->addCacheableDependency($homebox_type);
    }
    if ($owner === NULL) {
      // If no explicit account parameter is given, fallback to
      // current users homebox:
      $owner = $this->currentUser();
    }
    // Check by entity access on the homebox instance:
    $homebox = Homebox::retrieveInstance($homebox_type, $owner);
    return $homebox->access('personalize', $this->currentUser(), TRUE);
  }

  /**
   * Check access to the homebox.user_tab.personalize.
   *
   * CAREFUL! "$homebox_type" needs to be written as the route defines it!
   * In this case "homebox_type", if we use "$homebox_type" itself, the route
   * can not be resolved!
   *
   * @param \Drupal\homebox\Entity\HomeboxTypeInterface $homebox_type
   *   Homebox entity.
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The homebox owner user entity.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   Access result.
   */
  public function accessUserTabPersonalize(HomeboxTypeInterface $homebox_type, AccountInterface $user = NULL) {
    if (!$homebox_type->getUserTabEnabled()) {
      return AccessResult::forbidden()->addCacheableDependency($homebox_type);
    }
    if ($user === NULL) {
      // If no explicit account parameter is given, fallback to
      // current users homebox:
      $user = $this->currentUser();
    }
    // Check by entity access on the homebox instance:
    $homebox = Homebox::retrieveInstance($homebox_type, $user);
    return $homebox->access('personalize', $this->currentUser(), TRUE);
  }

}

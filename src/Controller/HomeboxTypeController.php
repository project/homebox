<?php

namespace Drupal\homebox\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\homebox\Entity\HomeboxTypeInterface;

/**
 * The homebox type controller.
 *
 * @package homebox\Controller
 */
class HomeboxTypeController extends ControllerBase {

  /**
   * The title callback for the presets tab.
   *
   * @param \Drupal\homebox\Entity\HomeboxTypeInterface $homebox_type
   *   The homebox type.
   *
   * @return string
   *   The presets page title
   */
  public function presetsTitle(HomeboxTypeInterface $homebox_type) {
    return $this->t('Homebox Type %homeboxType Presets', ['%homeboxType' => $homebox_type->label()]);
  }

  /**
   * The title callback for the homeboxes tab.
   *
   * @param \Drupal\homebox\Entity\HomeboxTypeInterface $homebox_type
   *   The homebox type.
   *
   * @return string
   *   The presets page title
   */
  public function homeboxesTitle(HomeboxTypeInterface $homebox_type) {
    return $this->t('Homebox Type %homeboxType User Homeboxes', ['%homeboxType' => $homebox_type->label()]);
  }

  /**
   * Prepares the homeboxes list in the homebox type admin area.
   *
   * @param \Drupal\homebox\Entity\HomeboxTypeInterface $homebox_type
   *   The homebox type.
   */
  public function buildHomeboxPresetsList(HomeboxTypeInterface $homebox_type) {
    $handler = $this->entityTypeManager()->getHandler('homebox', 'presets_list_builder');
    return $handler->render();
  }

}

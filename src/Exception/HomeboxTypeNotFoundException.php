<?php

namespace Drupal\homebox\Exception;

/**
 * An Exception class for handeling a missing homebox type.
 */
class HomeboxTypeNotFoundException extends \Exception {

}

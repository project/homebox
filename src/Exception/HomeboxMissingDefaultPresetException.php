<?php

namespace Drupal\homebox\Exception;

/**
 * An Exception class for handeling a missing homebox type's default preset.
 */
class HomeboxMissingDefaultPresetException extends \Exception {

}

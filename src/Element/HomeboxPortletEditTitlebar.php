<?php

namespace Drupal\homebox\Element;

use Drupal\Core\Render\Element\Container;

/**
 * Provides a homebox portlet edit form titlebar element container.
 *
 * Allows to render the form homebox portlet edit form through the
 * homebox-portlet-edit.html.twig.
 *
 * This isn't possible like for
 *
 * Properties:
 *   '#type' => 'homebox_portlet_edit_titlebar',
 *   '#children' => $portletInstance->buildConfigurationForm([], $form_state),
 *   '#attributes' => [],
 *   '#portlet' => $portletInstance->build(),
 *   '#portlet_label' => $portletArray['portletLabel'],
 *   '#portlet_type' => $portletArray['portletTypeId'],
 *   '#portlet_delta' => $delta,
 *   '#portlet_instance' => $portletInstance,
 *   '#portlet_settings' => $homeboxType->getHomeboxTypePortletTypesSettings($portletArray['portletTypeId']),
 *
 * Usage Example:
 * @code
 * $build['homebox_portlet_edit_titlebar'] = [
 *   '#type' => 'homebox_portlet_edit',
 *   '#children' => $portletInstance->buildConfigurationForm([], $form_state),
 *   '#attributes' => [],
 *   '#portlet' => $portletInstance->build(),
 *   '#portlet_label' => $portletArray['portletLabel'],
 *   '#portlet_type' => $portletArray['portletTypeId'],
 *   '#portlet_delta' => $delta,
 *   '#portlet_instance' => $portletInstance,
 *   '#portlet_settings' => $homeboxType->getHomeboxTypePortletTypesSettings($portletArray['portletTypeId']),
 * ];
 * @endcode
 *
 * @RenderElement("homebox_portlet_edit_titlebar")
 */
final class HomeboxPortletEditTitlebar extends Container {

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    $getInfo = parent::getInfo();
    // Use 'homebox_portlet_edit' theme instead of 'container':
    $getInfo['#theme_wrappers'] = ['homebox_portlet_edit_titlebar'];
    return $getInfo;
  }

}

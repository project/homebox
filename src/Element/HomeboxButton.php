<?php

namespace Drupal\homebox\Element;

use Drupal\Core\Render\Element\Button;

/**
 * Provides a 'homebox_button' element.
 *
 * @RenderElement("homebox_button")
 */
final class HomeboxButton extends Button {

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    $getInfo = parent::getInfo();
    // Use 'homebox_button' theme instead of 'sunmit':
    $getInfo['#theme_wrappers'] = ['homebox_button'];
    return $getInfo;
  }

  /**
   * {@inheritdoc}
   */
  public static function preRenderButton($element) {
    $element = parent::preRenderButton($element);
    unset($element['#attributes']['type']);
    return $element;
  }

}

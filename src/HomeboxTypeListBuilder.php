<?php

namespace Drupal\homebox;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Homebox Types.
 */
class HomeboxTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Homebox Type');
    $header['id'] = $this->t('Machine Name');
    $header['userTabEnabled'] = $this->t('Homebox User Tab');
    $header['pagePath'] = $this->t('Homebox Page Path');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /**
* @var \Drupal\homebox\Entity\HomeboxTypeInterface $entity
*/
    $pagePath = $entity->getPagePath();
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['userTabEnabled'] = $entity->getUserTabEnabled() ? $this->t('Enabled') : $this->t('Disabled');
    $row['pagePath'] = !empty($pagePath) ? $pagePath : $this->t('Disabled');
    return $row + parent::buildRow($entity);
  }

}

<?php

namespace Drupal\homebox\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Homebox Types.
 */
interface HomeboxTypeInterface extends ConfigEntityInterface {

  /**
   * Returns true, if the title should be shown, false otherwise.
   *
   * @return bool
   *   The title visibility.
   */
  public function getShowTitle(): bool;

  /**
   * Set the layout id.
   *
   * @param bool $showTitle
   *   The title visibility.
   *
   * @return $this
   */
  public function setShowTitle(bool $showTitle): self;

  /**
   * Gets the allowed layout IDs.
   *
   * @return array
   *   Allowed layout ids.
   */
  public function getLayoutIds(): array;

  /**
   * Set the allowed layout IDs.
   *
   * @param array $layoutIds
   *   The layout ids to set.
   *
   * @return $this
   */
  public function setLayoutIds(array $layoutIds): self;

  /**
   * Returns if there is a custom page enabled for this entity.
   *
   * @return bool
   *   True if there is a custom page enabled for this entity.
   */
  public function getPageEnabled(): bool;

  /**
   * Returns if there is a user tab page enabled for this entity.
   *
   * @return bool
   *   True, if there is a user tab page enabled for this entity.
   */
  public function getUserTabEnabled(): bool;

  /**
   * Returns the custom page path.
   *
   * @return string
   *   The custom page path.
   */
  public function getPagePath(): string;

  /**
   * Set the homebox type path.
   *
   * @param string $pagePath
   *   The homebox type path.
   *
   * @return self
   *   The homebox type.
   */
  public function setPagePath(string $pagePath): self;

  /**
   * Get an array of homebox type portlet types settings, keyed by their id.
   *
   * @param bool $enabledOnly
   *   Only return the enabled portlets.
   *
   * @return array
   *   The portlet types.
   */
  public function getHomeboxTypePortletTypesSettings(bool $enabledOnly = FALSE): array;

  /**
   * Set the homeboxTypePortletTypesSettings.
   *
   * @param array $homeboxTypePortletTypesSettings
   *   The  homeboxTypePortletTypesSettings.
   *
   * @return self
   *   This entity.
   */
  public function setHomeboxTypePortletTypesSettings(array $homeboxTypePortletTypesSettings): self;

  /**
   * Get a homebox type portlet type settings.
   *
   * @param string $pluginId
   *   The plugin id (portlet type) to get the setting for.
   *
   * @return array
   *   The settings value or NULL if not existing.
   */
  public function getHomeboxTypePortletTypeSettings(string $pluginId): array;

  /**
   * Add a homebox type portlet type settings array for the given plugin id.
   *
   * @param string $pluginId
   *   The plugin id (portlet type) to add the settings for.
   * @param array $homeboxPortletTypeSettings
   *   The homeboxPortletTypeSettings to add.
   *
   * @return self
   *   This entity.
   */
  public function setHomeboxTypePortletTypeSettings(string $pluginId, array $homeboxPortletTypeSettings): self;

  /**
   * Get a specific homebox type portlet type setting.
   *
   * @param string $pluginId
   *   The plugin id (portlet type) to get the setting for.
   * @param string $setting
   *   The id of the setting to get (e.g. "status", "label", ...).
   *
   * @return mixed
   *   The setting value or NULL if not existing.
   */
  public function getHomeboxTypePortletTypeSetting(string $pluginId, string $setting): mixed;

}

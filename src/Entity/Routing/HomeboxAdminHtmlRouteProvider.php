<?php

namespace Drupal\homebox\Entity\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides (admin) routes for Homebox.
 */
class HomeboxAdminHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritDoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    $entity_type_id = $entity_type->id();

    if ($personalizeRoute = $this->getAdminPersonalizeFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.personalize", $personalizeRoute);
    }

    return $collection;
  }

  /**
   * Gets the personalize ("Build") route in the administration area.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getAdminPersonalizeFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('personalize')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('personalize'));
      $route->setDefaults(
            [
              '_controller' => '\Drupal\homebox\Controller\HomeboxController::buildAdminPersonalize',
              '_title' => 'Build',
            ]
        )
        ->setRequirement('_entity_access', "{$entity_type_id}.personalize")
        ->setOption(
          'parameters', [
            $entity_type_id => [
              'type' => 'entity:' . $entity_type_id,
            ],
          ]
        );

      // Entity types with serial IDs can specify this in their route
      // requirements, improving the matching process.
      if ($this->getEntityTypeIdKeyType($entity_type) === 'integer'
        ) {
        $route
          ->setRequirement($entity_type_id, '\\d+');
      }
      return $route;
    }
    return NULL;
  }

}

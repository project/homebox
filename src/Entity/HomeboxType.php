<?php

namespace Drupal\homebox\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines the Homebox Type entity.
 *
 * @ConfigEntityType(
 *   id = "homebox_type",
 *   label = @Translation("Homebox Type"),
 *   label_collection = @Translation("Homebox types"),
 *   label_singular = @Translation("Homebox type"),
 *   label_plural = @Translation("Homebox types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Homebox type",
 *     plural = "@count Homebox types",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\homebox\HomeboxTypeAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\homebox\Form\HomeboxTypeForm",
 *       "edit" = "Drupal\homebox\Form\HomeboxTypeForm",
 *       "delete" = "Drupal\homebox\Form\HomeboxTypeDeleteForm",
 *     },
 *     "list_builder" = "Drupal\homebox\HomeboxTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *       "permissions" = "Drupal\user\Entity\EntityPermissionsRouteProvider",
 *     },
 *   },
 *   config_prefix = "homebox_type",
 *   admin_permission = "administer homebox types",
 *   bundle_of = "homebox",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "showTitle" = "showTitle",
 *     "layoutIds" = "layoutIds",
 *     "pageEnabled" = "pageEnabled",
 *     "pagePath" = "pagePath",
 *     "userTabEnabled" = "userTabEnabled",
 *     "homeboxTypePortletTypesSettings" = "homeboxTypePortletTypesSettings",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "showTitle",
 *     "layoutIds",
 *     "pageEnabled",
 *     "pagePath",
 *     "userTabEnabled",
 *     "homeboxTypePortletTypesSettings",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/homebox_type/{homebox_type}",
 *     "add-form" = "/admin/structure/homebox_type/add",
 *     "edit-form" = "/admin/structure/homebox_type/{homebox_type}/edit",
 *     "delete-form" = "/admin/structure/homebox_type/{homebox_type}/delete",
 *     "collection" = "/admin/structure/homebox_type",
 *     "presets" = "/admin/structure/homebox_type/{homebox_type}/presets",
 *     "homeboxes" = "/admin/structure/homebox_type/{homebox_type}/homeboxes",
 *     "entity-permissions-form" = "/admin/structure/homebox_type/manage/{homebox_type}/permissions",
 *   }
 * )
 */
class HomeboxType extends ConfigEntityBundleBase implements HomeboxTypeInterface {
  use StringTranslationTrait;

  /**
   * The Homebox Type ID.
   *
   * @var string
   */
  protected string $id;

  /**
   * The Homebox Type label.
   *
   * @var string
   */
  protected string $label;

  /**
   * The allowed layout IDs.
   *
   * @var array
   */
  protected array $layoutIds = [];

  /**
   * Determines if the page title should be shown.
   *
   * @var bool
   */
  protected bool $showTitle = FALSE;

  /**
   * Whether or not this entity has a page.
   *
   * @var bool
   */
  protected bool $pageEnabled = FALSE;

  /**
   * Whether or not this entity has a user tab page.
   *
   * @var bool
   */
  protected bool $userTabEnabled = FALSE;

  /**
   * The homebox type path alias.
   *
   * @var string
   */
  protected string $pagePath = '';

  /**
   * The general homeboxTypePortletTypesSettings.
   *
   * @var array
   */
  protected array $homeboxTypePortletTypesSettings = [];

  /**
   * {@inheritdoc}
   */
  public function getShowTitle(): bool {
    return $this->showTitle;
  }

  /**
   * {@inheritdoc}
   */
  public function setShowTitle(bool $showTitle): self {
    $this->showTitle = $showTitle;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLayoutIds(): array {
    return $this->layoutIds;
  }

  /**
   * {@inheritdoc}
   */
  public function setLayoutIds(array $layoutIds): self {
    $this->layoutIds = $layoutIds;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPageEnabled(): bool {
    return $this->pageEnabled;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserTabEnabled(): bool {
    return $this->userTabEnabled;
  }

  /**
   * {@inheritDoc}
   */
  public function getPagePath(): string {
    return $this->pagePath;
  }

  /**
   * {@inheritDoc}
   */
  public function setPagePath(string $pagePath): self {
    $this->pagePath = $pagePath;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHomeboxTypePortletTypesSettings(bool $enabledOnly = FALSE): array {
    $homeboxPortletTypesSettings = $this->homeboxTypePortletTypesSettings;
    if (!$enabledOnly) {
      return $homeboxPortletTypesSettings;
    }
    $enabledHomeboxPortlets = [];
    foreach ($homeboxPortletTypesSettings as $homeboxPortletId => $homeboxThirdPartySettings) {
      if ((bool) $homeboxThirdPartySettings['status']) {
        $enabledHomeboxPortlets[$homeboxPortletId] = $homeboxThirdPartySettings;
      }
    }
    return $enabledHomeboxPortlets;
  }

  /**
   * {@inheritdoc}
   */
  public function getHomeboxTypePortletTypeSettings(string $pluginId): array {
    return $this->homeboxTypePortletTypesSettings[$pluginId] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function setHomeboxTypePortletTypesSettings(array $homeboxTypePortletTypesSettings): self {
    $this->homeboxTypePortletTypesSettings = $homeboxTypePortletTypesSettings;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHomeboxTypePortletTypeSetting(string $pluginId, string $setting): mixed {
    return $this->homeboxTypePortletTypesSettings[$pluginId][$setting] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setHomeboxTypePortletTypeSettings(string $pluginId, array $homeboxPortletTypeSettings): self {
    $this->homeboxTypePortletTypesSettings[$pluginId] = $homeboxPortletTypeSettings;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    // Convert emtpy settings (no type at all or empty string) to an empty
    // array:
    if (empty($this->homeboxTypePortletTypesSettings)) {
      $this->homeboxTypePortletTypesSettings = [];
    }
    return parent::save();
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    $homeboxStorage = $this->entityTypeManager()->getStorage('homebox');
    // Check if there already is a default preset and if there isn't, create
    // one:
    $homeboxPresets = $homeboxStorage->getQuery()->accessCheck()
      ->condition('type', $this->id())
      ->condition('isPreset', TRUE)
      ->condition('isPresetDefault', TRUE)
      ->execute();

    if (empty($homeboxPresets)) {
      $layoutIds = $this->getLayoutIds();
      Homebox::create(
            [
              'id' => $this->id() . '_preset_0',
              'label' => $this->label() . ' Preset 0',
              'uid' => \Drupal::currentUser()->id(),
              'creator' => \Drupal::currentUser()->id(),
              'status' => TRUE,
              'isPreset' => TRUE,
              'isPresetDefault' => TRUE,
              'layoutId' => reset($layoutIds),
              'type' => $this->id(),
            ]
        )->save();
      \Drupal::messenger()->addMessage(
            $this->t(
                'Created the %label homebox type default preset.', [
                  '%label' => $this->label(),
                ]
            )
        );
    }
  }

}

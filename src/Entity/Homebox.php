<?php

namespace Drupal\homebox\Entity;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\homebox\Exception\HomeboxTypeNotFoundException;
use Drupal\user\Entity\User;
use Drupal\user\EntityOwnerTrait;
use Drupal\user\UserInterface;

/**
 * Defines the Homebox entity.
 *
 * @ingroup homebox
 *
 * @ContentEntityType(
 *   id = "homebox",
 *   label = @Translation("Homebox"),
 *   label_collection = @Translation("Homebox"),
 *   label_singular = @Translation("homebox item"),
 *   label_plural = @Translation("homebox items"),
 *   label_count = @PluralTranslation(
 *     singular = "@count homebox item",
 *     plural = "@count homebox items"
 *   ),
 *   bundle_label = @Translation("Homebox type"),
 *   handlers = {
 *     "view_builder" = "Drupal\homebox\HomeboxViewBuilder",
 *     "list_builder" = "Drupal\homebox\HomeboxListBuilder",
 *     "presets_list_builder" = "Drupal\homebox\HomeboxPresetsListBuilder",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *     "access" = "Drupal\homebox\HomeboxAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\homebox\Form\HomeboxForm",
 *       "add" = "Drupal\homebox\Form\HomeboxForm",
 *       "edit" = "Drupal\homebox\Form\HomeboxForm",
 *       "delete" = "Drupal\homebox\Form\HomeboxDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\homebox\Entity\Routing\HomeboxAdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer homeboxes",
 *   permission_granularity = "bundle",
 *   common_reference_target = TRUE,
 *   bundle_entity_type = "homebox_type",
 *   base_table = "homebox",
 *   data_table = "homebox_field_data",
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "owner" = "uid",
 *     "creator" = "creator",
 *     "langcode" = "langcode",
 *     "isPreset" = "isPreset",
 *     "isPresetDefault" = "isPresetDefault",
 *     "layoutId" = "layoutId",
 *     "portlets" = "portlets",
 *   },
 *   links = {
 *     "canonical" = "/admin/homebox/{homebox}",
 *     "add-form" = "/admin/homebox/add/{homebox_type}",
 *     "edit-form" = "/admin/homebox/{homebox}/edit",
 *     "personalize" = "/admin/homebox/{homebox}/personalize",
 *     "delete-form" = "/admin/homebox/{homebox}/delete",
 *     "set_default_preset" = "/admin/homebox/{homebox}/set-default-preset",
 *     "collection" = "/admin/content/homebox",
 *   },
 * )
 */
class Homebox extends ContentEntityBase implements HomeboxInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * This is a private static instance cache for homeboxes.
   *
   * It allows returning the homebox instance from here from the 2nd call
   * to retrieveInstance() on.
   * Therefore the already generated homebox instances are cached by the
   * keys:
   * - type
   * - uid.
   *
   * If this is not a good idea or leads to large memory footprint, it can be
   * safely removed from retrieveInstance() as it's just internal!
   * Until that, we think it's a good idea to not initialize the "same" homebox
   * on each call in the same request.
   *
   * @var array
   */
  private static array $homeboxInstanceCache = [];

  /**
   * Returns the homebox for the given $account of the given $homebox_type.
   *
   * Static helper to return the correct homebox instance for the given
   * $account of the given $homebox_type. If there is a personalized
   * instance of the homebox, that one is returned. If not, the preset is
   * returned. If no such $homebox_type is present, throws an Exception.
   *
   * @param \Drupal\homebox\Entity\HomeboxTypeInterface $homebox_type
   *   The homebox type.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The homebox owner account.
   *
   * @return \Drupal\homebox\Entity\HomeboxInterface
   *   The homebox.
   */
  public static function retrieveInstance(HomeboxTypeInterface $homebox_type, AccountInterface $account): HomeboxInterface {
    if (isset(self::$homeboxInstanceCache[$homebox_type->id()][$account->id()])) {
      // Return from static cache, if it was already loaded:
      return self::$homeboxInstanceCache[$homebox_type->id()][$account->id()];
    }

    $homeboxStorage = \Drupal::entityTypeManager()->getStorage('homebox');
    // First look for the users individual homebox instance:
    $userPersonalizedHomebox = $homeboxStorage->loadByProperties(
          [
            'type' => $homebox_type->id(),
            'isPreset' => FALSE,
            'uid' => $account->id(),
          ]
      );
    if (!empty($userPersonalizedHomebox)) {
      $userPersonalizedHomebox = reset($userPersonalizedHomebox);
      // Store in static cache:
      self::$homeboxInstanceCache[$homebox_type->id()][$account->id()] = $userPersonalizedHomebox;
      return $userPersonalizedHomebox;
    }

    // Otherwise return the general preset:
    $presetHomeboxes = $homeboxStorage->loadByProperties(
          [
            'type' => $homebox_type->id(),
            'isPreset' => TRUE,
            'isPresetDefault' => TRUE,
          ]
      );
    if (!empty($presetHomeboxes)) {
      // There can be only one default preset at once, but we get an array with
      // one entry here, so we need to load that entry here:
      $presetHomebox = reset($presetHomeboxes);
      // Create a clone of the homebox:
      /**
       * @var \Drupal\homebox\Entity\HomeboxInterface $userPersonalizedHomebox
       */
      $userPersonalizedHomebox = $presetHomebox->createDuplicate();
      $userPersonalizedHomebox->setPreset(FALSE);
      $userPersonalizedHomebox->setPresetDefault(FALSE);
      $user = User::load($account->id());
      $userPersonalizedHomebox->setOwner($user);
      $userPersonalizedHomebox->setCreator($user);
      $userPersonalizedHomebox->setCreatedTime(time());
      // Set internal label for the instance:
      $userPersonalizedHomebox->setLabel($homebox_type->label() . '(UID: ' . $user->id() . ')');
      // Store in static cache:
      self::$homeboxInstanceCache[$homebox_type->id()][$account->id()] = $userPersonalizedHomebox;

      return $userPersonalizedHomebox;
    }

    // No such homebox could be found at all:
    throw new HomeboxTypeNotFoundException();
  }

  /**
   * Retrieve the default preset homebox for the given $homebox_type.
   */
  public static function retrievePresetDefaultHomebox(HomeboxTypeInterface $homebox_type): ?HomeboxInterface {
    $homeboxStorage = \Drupal::entityTypeManager()->getStorage('homebox');
    $presetHomeboxes = $homeboxStorage->loadByProperties(
      [
        'type' => $homebox_type->id(),
        'isPreset' => TRUE,
        'isPresetDefault' => TRUE,
      ]
    );
    return reset($presetHomeboxes);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (empty($translation->getOwner())) {
        $translation->setOwnerId(0);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function label(): TranslatableMarkup {
    $homeboxType = HomeboxType::load($this->bundle());
    $homeboxTypeLabel = $homeboxType->label();
    if ($this->isPresetDefault()) {
      return new TranslatableMarkup("%homeboxType default preset", ['%homeboxType' => $homeboxTypeLabel]);
    }
    elseif ($this->isPreset()) {
      // Presets are always suffixed with a number, so simply render their label
      // here:
      return new TranslatableMarkup("%homeboxType", ['%homeboxType' => $homeboxTypeLabel]);
    }
    return new TranslatableMarkup("%user %homeboxType",
    [
      '%user' => $this->getOwner()->getDisplayName() . "'s",
      '%homeboxType' => $homeboxTypeLabel,
    ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewLabel(): TranslatableMarkup {
    $homeboxType = HomeboxType::load($this->bundle());
    $homeboxTypeLabel = $homeboxType->label();
    if ($this->isPresetDefault()) {
      return new TranslatableMarkup("%homeboxType default preset", ['%homeboxType' => $homeboxTypeLabel]);
    }
    elseif ($this->isPreset()) {
      // Presets are always suffixed with a number, so simply render their label
      // here:
      return new TranslatableMarkup("%homeboxType", ['%homeboxType' => $homeboxTypeLabel]);
    }
    return new TranslatableMarkup("%user %homeboxType",
    [
      '%user' => ($this->getOwnerId() === \Drupal::currentUser()->id()) ? new TranslatableMarkup('My', [], ['context' => 'HomeboxType']) : $this->getOwner()->getDisplayName() . "'s",
      '%homeboxType' => $homeboxTypeLabel,
    ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function personalizeLabel(): TranslatableMarkup {
    $homeboxType = HomeboxType::load($this->bundle());
    $homeboxTypeLabel = $homeboxType->label();
    if ($this->isPresetDefault()) {
      return new TranslatableMarkup("Personalize the %homeboxType default preset", ['%homeboxType' => $homeboxTypeLabel]);
    }
    elseif ($this->isPreset()) {
      // Presets are always suffixed with a number, so simply render their label
      // here:
      return new TranslatableMarkup("Personalize %homeboxType", ['%homeboxType' => $homeboxTypeLabel]);
    }
    return new TranslatableMarkup("Personalize %user %homeboxType", [
      '%user' => ($this->getOwnerId() === \Drupal::currentUser()->id()) ? new TranslatableMarkup('my') : $this->getOwner()->getDisplayName() . "'s",
      '%homeboxType' => $homeboxTypeLabel,
    ],
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel(string $label): self {
    $this->set('label', $label);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp): self {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPreset(): bool {
    return (bool) $this->get('isPreset')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPreset(bool $isPreset): self {
    $this->set('isPreset', $isPreset);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPresetDefault(): bool {
    return (bool) $this->get('isPresetDefault')->value;

  }

  /**
   * {@inheritdoc}
   */
  public function setPresetDefault(bool $isPresetDefault): self {
    $this->set('isPresetDefault', $isPresetDefault);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLayoutId(): string {
    return $this->get('layoutId')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLayoutId(string $layoutId): self {
    $this->set('layoutId', $layoutId);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatorId(): int {
    return $this->getEntityKey('creator');
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatorId($uid): self {
    $key = $this->getEntityType()->getKey('creator');
    $this->set($key, $uid);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreator(): UserInterface {
    $key = $this->getEntityType()->getKey('creator');
    return $this->get($key)->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreator(UserInterface $account): self {
    $key = $this->getEntityType()->getKey('creator');
    $this->set($key, $account);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPortlet(int $delta): FieldItemListInterface {
    return $this->get('portlets')->get($delta);
  }

  /**
   * {@inheritdoc}
   */
  public function getPortlets(): FieldItemListInterface {
    return $this->get('portlets');
  }

  /**
   * {@inheritdoc}
   */
  public function getPortletsArrays($onlyEnabled = TRUE): array {
    // @improve getPortlets() return the incorrect types:
    // See https://www.drupal.org/project/homebox/issues/3437832 for more infos.
    $portletsArray = $this->getPortlets()->getValue();
    foreach ($portletsArray as $key => &$portlet) {
      if ($onlyEnabled && !$portlet['enabled']) {
        unset($portletsArray[$key]);
      }
      if ($portlet['thirdPartySettings'] == NULL) {
        continue;
      }
      $thirdPartySettingsArray = Json::decode($portlet['thirdPartySettings']);
      $portlet['thirdPartySettings'] = $thirdPartySettingsArray;
    }
    return $portletsArray;
  }

  /**
   * {@inheritdoc}
   */
  public function setPortletsArrays(array $portlets): self {
    foreach ($portlets as &$portlet) {
      $thirdPartySettings = $portlet['thirdPartySettings'];
      if ($thirdPartySettings == NULL) {
        continue;
      }
      $settingsJson = Json::encode($thirdPartySettings);
      $portlet['thirdPartySettings'] = $settingsJson;
    }
    $this->set('portlets', $portlets);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPortletArray(int $delta): array {
    $portletsArray = $this->getPortletsArrays(FALSE);
    // Return NULL, if the portlet could not be found:
    if (!isset($portletsArray[$delta]) || empty($portletsArray[$delta])) {
      return NULL;
    }
    return $portletsArray[$delta];
  }

  /**
   * {@inheritdoc}
   */
  public function setPortletArrayByValues(int $delta, string $portletTypeId, string $layoutRegion, int $layoutRegionWeight, string $portletLabel = '', array $thirdPartySettings = NULL, $enabled = TRUE): self {
    $portletsArray = $this->getPortletsArrays(FALSE);
    $modifiedPortletArray = [
      'portletTypeId' => $portletTypeId,
      'layoutRegion' => $layoutRegion,
      'layoutRegionWeight' => $layoutRegionWeight,
      'enabled' => $enabled,
      'portletLabel' => $portletLabel,
      'thirdPartySettings' => $thirdPartySettings,
    ];
    $portletsArray[$delta] = $modifiedPortletArray;
    $this->setPortletsArrays($portletsArray);
    // Finally recalculate weights, see
    // https://www.drupal.org/project/homebox/issues/3399048.
    $this->portletArrayRecalculateWeights($modifiedPortletArray, $delta);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addPortletByValues(string $portletTypeId, string $layoutRegion, int $layoutRegionWeight, string $portletLabel = '', array $thirdPartySettings = NULL, $enabled = TRUE): self {
    $newPortletArray = [
      'portletTypeId' => $portletTypeId,
      'layoutRegion' => $layoutRegion,
      'layoutRegionWeight' => $layoutRegionWeight,
      'enabled' => $enabled,
      'portletLabel' => $portletLabel,
      'thirdPartySettings' => $thirdPartySettings !== NULL ? Json::encode($thirdPartySettings) : NULL,
    ];
    $this->getPortlets()->appendItem($newPortletArray);
    // Finally recalculate weights, see
    // https://www.drupal.org/project/homebox/issues/3399048.
    $this->portletArrayRecalculateWeights($newPortletArray);
    return $this;
  }

  /**
   * Recalculate the portlet weights.
   *
   * This is needed, as the portlet weights
   * might differ between client side (e.g. through drag and drop) and server
   * side.
   *
   * @param array $targetPortletArray
   *   The new / modified portlet array, on which we need to recalculate the
   *   the weights around.
   * @param int $portletPosition
   *   The portlet position inside the portlets array (delta).
   */
  protected function portletArrayRecalculateWeights(array $targetPortletArray, $portletPosition = NULL): self {
    $portletsArray = $this->getPortletsArrays(FALSE);

    // Sort the portlets by region, then by weight:
    if ($portletPosition === NULL) {
      // If the old delta is not set, we fallback to last key of the array:
      $portletPosition = array_key_last($portletsArray);
    }
    foreach ($portletsArray as $delta => &$portletArray) {
      // Only recalculate weights of the same layout region:
      if ($portletArray['layoutRegion'] === $targetPortletArray['layoutRegion']) {
        // Do not recalculate the weight of the new portlet itself:
        if ($delta != $portletPosition) {
          // @improve We should do a hard typed comparison here, but we can't right
          // now, as "getPortletsArrays()" does not return the correct types
          // yet. See https://www.drupal.org/project/homebox/issues/3437832
          // for more infos.
          if ($portletArray['layoutRegionWeight'] >= $targetPortletArray['layoutRegionWeight']) {
            // Increase the layoutRegionWeight of the current portlet and any
            // other portlets, with a higher weight than the new / modified
            // portlet:
            $portletArray['layoutRegionWeight'] = $portletArray['layoutRegionWeight'] + 1;
          }
        }
      }
    }
    $this->setPortletsArrays($portletsArray);
    // Finally zero index the portlet weights:
    $this->zeroIndexPortletWeights();
    return $this;
  }

  /**
   * Zero index the portlet weights.
   *
   * This is needed, as the portlet weights will get messed when portlets are
   * moved, added or removed.
   */
  public function zeroIndexPortletWeights(): self {
    $portletsArray = $this->getPortletsArrays(FALSE);
    // Temporarily create a array, sorted by layoutRegionWeight:
    $portletsArrayClone = $portletsArray;
    // Save the original delta value, so we can set it back after sorting:
    foreach ($portletsArrayClone as $delta => $portletClone) {
      $portletsArrayClone[$delta]['delta'] = $delta;
    }
    // Sort the portlets by region:
    $sortedPortletsArrayByRegion = [];
    foreach ($portletsArrayClone as $portletClone) {
      $sortedPortletsArrayByRegion[$portletClone['layoutRegion']][] = $portletClone;
    }
    // Sort by layoutRegionWeight in each region:
    foreach ($sortedPortletsArrayByRegion as $region => $sortedPortletsArray) {
      usort($sortedPortletsArrayByRegion[$region], function ($a, $b) {
        return $a['layoutRegionWeight'] <=> $b['layoutRegionWeight'];
      });
    }
    foreach ($sortedPortletsArrayByRegion as $sortedPortletsArray) {
      // Now set the new weights back to the original array:
      foreach ($sortedPortletsArray as $newLayoutRegionWeight => $sortedPortletEntry) {
        $portletsArray[$sortedPortletEntry['delta']]['layoutRegionWeight'] = $newLayoutRegionWeight;
      }
    }
    $this->setPortletsArrays($portletsArray);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removePortlet(int $delta): self {
    $this->getPortlets()->removeItem($delta);
    $this->zeroIndexPortletWeights();
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function hasPortletTypeInstance(string $portletTypeId): bool {
    $portletsArrays = $this->getPortletsArrays(FALSE);
    foreach ($portletsArrays as $portletArray) {
      if ($portletArray['portletTypeId'] === $portletTypeId) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function enablePortlet(int $delta): self {
    /**
     * @var \Drupal\homebox\Plugin\Field\FieldType\PortletItem $portletItem
     */
    $portletItem = $this->getPortlets()->get($delta);
    $portletItem->set('enabled', TRUE);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function disablePortlet(int $delta): self {
    /**
     * @var \Drupal\homebox\Plugin\Field\FieldType\PortletItem $portletItem
     */
    $portletItem = $this->getPortlets()->get($delta);
    $portletItem->set('enabled', FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setThirdPartySettings(int $delta, array $thirdPartySettings): self {
    $portletsArray = $this->getPortletsArrays(FALSE);
    $portletsArray[$delta]['thirdPartySettings'] = $thirdPartySettings;
    $this->setPortletsArrays($portletsArray);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getThirdPartySettings(int $delta): array {
    return $this->getPortletArray($delta)['thirdPartySettings'] ?? [];
  }

  /**
   * Returns an array of available layouts for this Homebox's Homebox Type.
   *
   * @param \Drupal\homebox\Entity\HomeboxType $homeboxType
   *   The homeboxtype to check the allowed layouts for.#.
   */
  private static function getAvailableLayoutIds(HomeboxType $homeboxType): array {
    $layoutIds = $homeboxType->getLayoutIds();
    $layoutOptions = \Drupal::service('plugin.manager.core.layout')->getLayoutOptions();
    $availableLayouts = [];
    foreach ($layoutOptions as $columnKey => $layoutOption) {
      foreach ($layoutOption as $layoutId => $layoutLabel) {
        if (in_array($layoutId, $layoutIds)) {
          $availableLayouts[$columnKey][$layoutId] = $layoutLabel;
        }
      }
    }
    return $availableLayouts;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions(
              'view', [
                'label' => 'hidden',
                'type' => 'string',
                'weight' => -10,
              ]
          )
      ->setDisplayOptions(
              'form', [
                'type' => 'string_textfield',
                'weight' => -10,
              ]
          )
      ->setDisplayConfigurable('form', TRUE);

    $fields['layoutId'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Layout'))
      ->setDescription(t('The Layout ID of the Homebox entity.'))
      ->setDisplayOptions(
              'view', [
                'label' => 'above',
                'type' => 'string',
                'weight' => -9,
              ]
          )
      ->setDisplayOptions(
              'form', [
                'type' => 'options_select',
                'weight' => -9,
              ]
          )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['uid']
      ->setLabel(t('Owned by'))
      ->setDescription(t('The owner of the Homebox.'))
      ->setDisplayOptions(
              'view', [
                'label' => 'hidden',
                'type' => 'author',
                'weight' => 0,
              ]
          )
      ->setDisplayOptions(
              'form', [
                'type' => 'entity_reference_autocomplete',
                'weight' => 5,
                'settings' => [
                  'match_operator' => 'CONTAINS',
                  'size' => '60',
                  'placeholder' => '',
                ],
              ]
          )
      ->setDisplayConfigurable('form', TRUE);

    // Set the owner required:
    $fields['uid']->setRequired(TRUE);

    $fields['creator'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Created by'))
      ->setRequired(TRUE)
      ->setDescription(t('The creator of the homebox.'))
      ->setSetting('target_type', 'user')
      ->setTranslatable($entity_type->isTranslatable())
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setDisplayOptions(
              'view', [
                'label' => 'hidden',
                'type' => 'author',
                'weight' => 0,
              ]
          )
      ->setDisplayOptions(
              'form', [
                'type' => 'entity_reference_autocomplete',
                'weight' => 5,
                'settings' => [
                  'match_operator' => 'CONTAINS',
                  'size' => '60',
                  'placeholder' => '',
                ],
              ]
          )
      ->setDisplayConfigurable('form', TRUE);

    $fields['isPreset'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Is preset'))
      ->setDescription(t('Whether or not this entity is used as a preset. A preset defines a homebox which is initially used by any user before customizing it, if selected as default preset.'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions(
              'form', [
                'type' => 'boolean_checkbox',
                'weight' => -4,
              ]
          );

    $fields['isPresetDefault'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Is default preset'))
      ->setDescription(t('Whether or not this entity is the default preset. A default preset can only exist once per homebox type and defines the preset which is currently the initially used homebox for every user.'))
      ->setDefaultValue(FALSE)
      ->addConstraint('uniqueDefaultPreset')
      ->setDisplayOptions(
              'form', [
                'type' => 'boolean_checkbox',
                'weight' => -4,
              ]
          );

    $fields['portlets'] = BaseFieldDefinition::create('homebox_portlet')
      ->setLabel(t('Portlets'))
      ->setDescription(t('The portlets displayed on the homebox page.'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setTranslatable(FALSE)
      ->setDisplayOptions(
              'form', [
                'type' => 'homebox_portlet_widget',
                'weight' => -4,
              ]
          );

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the homebox was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions(
              'view', [
                'label' => 'hidden',
                'type' => 'timestamp',
                'weight' => 0,
              ]
          )
      ->setDisplayOptions(
              'form', [
                'type' => 'datetime_timestamp',
                'weight' => 10,
              ]
          )
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the homebox was last edited.'))
      ->setTranslatable(TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions): array {
    $homeboxType = HomeboxType::load($bundle);
    // We need to override the allowed values here, as we do not have access to
    // the homebox type inside the base field definition, and we can not
    // override this setting on the HomeboxForm:
    /** @var \Drupal\Core\Field\BaseFieldDefinition $layoutIdBasefieldDefinition */
    $layoutIdBasefieldDefinition = $base_field_definitions['layoutId'];
    $layoutIdBasefieldDefinition->setSettings(['allowed_values' => self::getAvailableLayoutIds($homeboxType)]);

    return $base_field_definitions;
  }

  /**
   * {@inheritDoc}
   */
  public function getHomeboxType(): HomeboxTypeInterface {
    return HomeboxType::load($this->bundle());
  }

  /**
   * Returns the homebox URL from the breadcrumb.
   *
   * We need to use the breadcrumbs, as Homeboxes have multiple URLs
   * to appear on and the canonical is only for admins!
   */
  public static function getHomeboxUrlFromBreadcrumbs(): Url {
    // Redirect to parent page based on the breadcrumbs.
    $breadcrumbs = \Drupal::service('breadcrumb')->build(\Drupal::service('current_route_match'));
    $breadcrumbsLinks = $breadcrumbs->getLinks();
    $parentBreadcrumb = end($breadcrumbsLinks);
    return $parentBreadcrumb->getUrl();
  }

}

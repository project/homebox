<?php

namespace Drupal\homebox\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface for defining Homebox entities.
 *
 * @ingroup homebox
 */
interface HomeboxInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Set the label of the homebox.
   *
   * @param string $label
   *   The label of the homebox.
   *
   * @return $this
   */
  public function setLabel(string $label): self;

  /**
   * Gets the Homebox creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Homebox.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the Homebox creation timestamp.
   *
   * @param int $timestamp
   *   The Homebox creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp): self;

  /**
   * Get, whether or not this entity is a preset.
   *
   * @return bool
   *   True if this is a preset, else false.
   */
  public function isPreset(): bool;

  /**
   * Set, whether or not this entity is a preset.
   *
   * @param bool $isPreset
   *   True, if this entity should be flagged as a preset.
   *
   * @return $this
   */
  public function setPreset(bool $isPreset): self;

  /**
   * Get, whether or not this entity is the default preset.
   *
   * @return bool
   *   True if this is the default preset, else false.
   */
  public function isPresetDefault(): bool;

  /**
   * Set, whether or not this entity is the default preset.
   *
   * @param bool $isPresetDefault
   *   True, if this entity should be flagged as the default preset.
   *
   * @return $this
   */
  public function setPresetDefault(bool $isPresetDefault): self;

  /**
   * Get the layout id.
   *
   * @return string
   *   The layout id.
   */
  public function getLayoutId(): string;

  /**
   * Set the layout id.
   *
   * @param string $layoutId
   *   The layout id to set.
   *
   * @return $this
   */
  public function setLayoutId(string $layoutId): self;

  /**
   * Returns the entity's creator.
   *
   * @return \Drupal\user\UserInterface
   *   The creator user entity.
   */
  public function getCreator(): UserInterface;

  /**
   * Sets the entity's creator.
   *
   * @param \Drupal\user\UserInterface $account
   *   The creator user entity.
   *
   * @return $this
   */
  public function setCreator(UserInterface $account): self;

  /**
   * Returns the entity creator's user ID.
   *
   * @return ?int
   *   The creator user ID, or NULL in case the user ID field has not been set
   *   on the entity.
   */
  public function getCreatorId(): ?int;

  /**
   * Sets the entity creator's user ID.
   *
   * @param int $uid
   *   The creator user id.
   *
   * @return $this
   */
  public function setCreatorId(int $uid): self;

  /**
   * Returns the field values of the portlet field as FieldItemListInterface.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface
   *   The portlets field values.
   */
  public function getPortlets(): FieldItemListInterface;

  /**
   * Returns the entity's portlets.
   *
   * @param bool $onlyEnabled
   *   Only return enabled portlets.
   *
   * @return array
   *   The homebox portlets.
   */
  public function getPortletsArrays(bool $onlyEnabled = TRUE): array;

  /**
   * Sets the entity's portlets.
   *
   * @param array $portlets
   *   The portlets.
   *
   * @return $this
   */
  public function setPortletsArrays(array $portlets): self;

  /**
   * Get a single portlet from the homebox.
   *
   * @param int $delta
   *   The portlet delta for identification purposes.
   *
   * @return array
   *   The portlet values.
   */
  public function getPortletArray(int $delta): array;

  /**
   * Adds a portlet on the homebox entity..
   *
   * @param string $portletTypeId
   *   The portlet id.
   * @param string $layoutRegion
   *   The portlet'S layout region.
   * @param int $layoutRegionWeight
   *   The portlet's region weight.
   * @param string $portletLabel
   *   The portlet's (overwritten) label.
   * @param array $thirdPartySettings
   *   The portlet's settings.
   * @param bool $enabled
   *   The portlet's enabled status.
   *
   * @return $this
   */
  public function addPortletByValues(string $portletTypeId, string $layoutRegion, int $layoutRegionWeight, string $portletLabel = '', array $thirdPartySettings = NULL, $enabled = TRUE): self;

  /**
   * Set a single portlet on the homebox entity..
   *
   * @param int $delta
   *   The delta value for portlet identification.
   * @param string $portletTypeId
   *   The portlet id.
   * @param string $layoutRegion
   *   The portlet'S layout region.
   * @param int $layoutRegionWeight
   *   The portlet's region weight.
   * @param string $portletLabel
   *   The portlet's (overwritten) label.
   * @param array $thirdPartySettings
   *   The portlet's settings.
   * @param bool $enabled
   *   The portlet's enabled status.
   *
   * @return $this
   */
  public function setPortletArrayByValues(int $delta, string $portletTypeId, string $layoutRegion, int $layoutRegionWeight, string $portletLabel = '', array $thirdPartySettings = NULL, $enabled = TRUE): self;

  /**
   * Helper function to tell if a portlet type is placed in this portlet.
   *
   * @param string $portletTypeId
   *   The ID of the portlet type to check for.
   *
   * @return bool
   *   TRUE if there's a portlet of this type placed, FALSE if not.
   */
  public function hasPortletTypeInstance(string $portletTypeId): bool;

  /**
   * Update settings of a single portlet on the homebox entity.
   *
   * @param int $delta
   *   The delta value for portlet identification.
   * @param array $thirdPartySettings
   *   The portlet's settings.
   *
   * @return $this
   */
  public function setThirdPartySettings(int $delta, array $thirdPartySettings): self;

  /**
   * Returns the settings from a single portlet on the homebox entity.
   *
   * @param int $delta
   *   The delta value for portlet identification.
   *
   * @return array
   *   The portlet settings array.
   */
  public function getThirdPartySettings(int $delta): array;

  /**
   * Returns the parent HomeboxTypeInterface ConfigEntity.
   *
   * @return \Drupal\homebox\Entity\HomeboxTypeInterface
   *   The parent HomeboxTypeInterface ConfigEntity.
   */
  public function getHomeboxType(): HomeboxTypeInterface;

  /**
   * Returns the dynamic label of a homebox.
   *
   * Based on whether this homebox is a default preset, a preset or a
   * user homebox, the label is generated.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The dynamic label of the homebox.
   */
  public function label(): TranslatableMarkup;

  /**
   * Returns the dynamic personalize label of the homebox.
   *
   * Based on whether this homebox is a default preset, a preset or a
   * user homebox, the personalize label is generated.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The dynamic personalize label of the homebox.
   */
  public function personalizeLabel(): TranslatableMarkup;

  /**
   * Return the view label of the homebox.
   *
   * Based on whether this homebox is a default preset, a preset or a
   * user homebox, the view label is generated.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The dynamic personalize label of the homebox.
   */
  public function viewLabel(): TranslatableMarkup;

}

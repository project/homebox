<?php

namespace Drupal\homebox;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\homebox\Entity\HomeboxInterface;
use Drupal\homebox\Plugin\HomeboxPortletTypeInterface;

/**
 * Manages homebox portlet types plugins.
 */
class HomeboxPortletTypePluginManager extends DefaultPluginManager {

  /**
   * Constructs a HomeboxPortletTypePluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/HomeboxPortletType', $namespaces, $module_handler, 'Drupal\homebox\Plugin\HomeboxPortletTypeInterface', 'Drupal\homebox\Annotation\HomeboxPortletType');
    $this->setCacheBackend($cache_backend, 'homebox_portlet_type_plugins');
    $this->alterInfo('homebox_portlet_type_info');
  }

  /**
   * Creates a homebox portlet instance.
   *
   * @param string $plugin_id
   *   The plugin id.
   * @param array $configuration
   *   The plugin configuration array.
   * @param \Drupal\homebox\Entity\HomeboxInterface $homebox
   *   The Homebox.
   * @param int $delta
   *   The delta of the portlet.
   *
   * @return \Drupal\homebox\Plugin\HomeboxPortletTypeInterface
   *   The plugin instance.
   */
  public function createInstance($plugin_id, ?array $configuration = [], HomeboxInterface $homebox = NULL, int $delta = NULL): HomeboxPortletTypeInterface {
    $pluginInstance = parent::createInstance($plugin_id, $configuration);
    if ($homebox !== NULL) {
      $pluginInstance->setHomebox($homebox);
    }
    if ($delta !== NULL) {
      $pluginInstance->setDelta($delta);
    }

    return $pluginInstance;
  }

}

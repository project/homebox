<?php

namespace Drupal\homebox;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Url;
use Drupal\homebox\Entity\HomeboxType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines a class to build a listing of Homebox entities.
 *
 * @ingroup homebox
 */
class HomeboxListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The homebox type for the presets collection.
   *
   * @var \Drupal\homebox\Entity\HomeboxType|null
   *   The homebox type or NULL if we are on the normal homebox collection.
   */
  protected $homeboxType;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new HomeboxListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The current route match service object.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter, CurrentRouteMatch $current_route_match, RequestStack $request_stack, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
    $this->homeboxType = $current_route_match->getParameter('homebox_type');
    $this->requestStack = $request_stack;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('current_route_match'),
      $container->get('request_stack'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['homebox_type'] = $this->t('Homebox Type');
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Homebox ID');
    $header['isPreset'] = $this->t('Is Preset');
    $header['isPresetDefault'] = $this->t('Is Default Preset');
    $header['owner'] = $this->t('Owner');
    $header['creator'] = $this->t('Creator');
    $header['changed'] = $this->t('Changed');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /**
     * @var \Drupal\homebox\Entity\Homebox $entity
     */
    $homeboxTypeLabel = $entity->getHomeboxType()->label();
    $isPreset = $entity->isPreset();
    if ($isPreset) {
      // Highlight presets:
      $row['homebox_type']['data']['#plain_text'] = $homeboxTypeLabel;
      $row['homebox_type']['data']['#prefix'] = '<strong>';
      $row['homebox_type']['data']['#suffix'] = '</strong>';

      $row['name']['data']['#markup'] = $entity->label();
      $row['name']['data']['#prefix'] = '<strong>';
      $row['name']['data']['#suffix'] = '</strong>';
    }
    else {
      $row['homebox_type'] = $homeboxTypeLabel;
      $row['name'] = $entity->label();
    }
    $row['id'] = $entity->id();
    $row['isPreset']['data']['#markup'] = $isPreset ? '<strong>' . $this->t('Yes') . '</strong>' : $this->t('No');
    $row['isPresetDefault']['data']['#markup'] = $entity->isPresetDefault() ? '<strong>' . $this->t('Yes') . '</strong>' : $this->t('No');
    $row['owner'] = $entity->getOwner() ? $entity->getOwner()->label() : 'None';
    $row['creator'] = $entity->getCreator() ? $entity->getCreator()->label() : 'None';
    $row['changed'] = $entity->getChangedTime() ? $this->dateFormatter->format($entity->getChangedTime(), 'short') : '-';
    return $row + parent::buildRow($entity);
  }

  /**
   * Builds the query to retrieve the homebox presets.
   */
  protected function buildQuery() {
    // We are on the preset homebox collection, only load homeboxes with the
    // current homebox type and is_preset true:
    $query = $this->getStorage()->getQuery()
      ->accessCheck(TRUE)
      ->condition('type', $this->homeboxType->id())
      ->condition('isPreset', FALSE);

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }

    return $query;
  }

  /**
   * {@inheritDoc}
   */
  public function load() {
    if (empty($this->homeboxType)) {
      // We are on the "normal" homebox collection, just load the entities
      // normally:
      return parent::load();
    }

    $query = $this->buildQuery();

    $homeboxPresets = $query->execute();
    return $this->storage->loadMultiple($homeboxPresets);
  }

  /**
   * {@inheritDoc}
   */
  protected function getEntityIds() {
    $query = $this->entityTypeManager->getStorage($this->entityTypeId)->getQuery();
    $request = $this->requestStack->getCurrentRequest();

    $homeboxType = $request->get('homeboxType');
    if (!empty($homeboxType)) {
      $query->accessCheck(TRUE)->condition('type', $homeboxType);
    }

    $isPreset = $request->get('isPreset');
    if (!empty($isPreset)) {
      $query->accessCheck(TRUE)->condition('isPreset', $isPreset);
    }

    if ($this->limit) {
      $query->pager($this->limit);
    }

    return $query->accessCheck(TRUE)->execute();
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    // @phpstan-ignore-next-line
    $build['filter'] = \Drupal::formBuilder()->getForm('Drupal\homebox\Form\HomeboxListFilterForm', $this->homeboxType);
    $build += parent::render();
    return $build;
  }

  /**
   * Gets this list's default operations.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity the operations are for.
   *
   * @return array
   *   The array structure is identical to the return value of
   *   self::getOperations().
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    /** @var \Drupal\homebox\Entity\Homebox $entity */
    if ($entity->access('view')) {
      $homeboxType = HomeboxType::load($entity->bundle());
      // Add admin view:
      $operations['view'] = [
        'title' => $this->t('View'),
        'weight' => 10,
        'url' => Url::fromRoute(
            'entity.homebox.canonical', [
              'homebox' => $entity->id(),
              'homebox_type' => $homeboxType->id(),
              'owner' => $entity->getOwnerId(),
            ]
        ),
      ];
      // Add view page link:
      if ($homeboxType->getPageEnabled()) {
        $operations['view_page'] = [
          'title' => $this->t('Preview (My page)'),
          'weight' => 20,
          'url' => Url::fromRoute(
              'homebox.page_route.' . $homeboxType->id(), [
                'owner' => $entity->getOwnerId(),
              ]
          ),
        ];
      }
      // Add view user tab link:
      if ($homeboxType->getUserTabEnabled()) {
        $operations['view_user_tab'] = [
          'title' => $this->t('Preview (My user tab)'),
          'weight' => 21,
          'url' => Url::fromRoute(
              'homebox.user_tab', [
                'homebox_type' => $homeboxType->id(),
                'user' => $entity->getOwnerId(),
              ]
          ),
        ];
      }
    }
    // Add view page link:
    if ($entity->access('personalize') && $entity->hasLinkTemplate('personalize')) {
      $operations['personalize'] = [
        'title' => $entity->isPreset() ? $this->t('Build') : $this->t('Personalize'),
        'weight' => 10,
        'url' => $this->ensureDestination($entity->toUrl('personalize')),
      ];
    }
    if ($entity->isPreset() && $entity->access('set_default_preset') && $entity->hasLinkTemplate('set_default_preset')) {
      $operations['set_default_preset'] = [
        'title' => $this->t('Set default preset'),
        'weight' => 12,
        'url' => $this->ensureDestination($entity->toUrl('set_default_preset')),
      ];
    }

    if (isset($operations['edit']['weight'])) {
      $operations['edit']['weight'] = 11;
    }
    if (isset($operations['delete']['weight'])) {
      $operations['delete']['weight'] = 13;
    }

    return $operations;
  }

}

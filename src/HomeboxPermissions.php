<?php

namespace Drupal\homebox;

use Drupal\Core\Entity\BundlePermissionHandlerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\homebox\Entity\HomeboxType;

/**
 * Provides dynamic permissions for homeboxes of different types.
 */
class HomeboxPermissions {
  use BundlePermissionHandlerTrait;
  use StringTranslationTrait;

  /**
   * Returns an array of homebox type permissions.
   *
   * @return array
   *   The homebox type permissions.
   *
   * @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function homeboxTypePermissions() {
    return $this->generatePermissions(
          HomeboxType::loadMultiple(), [
            $this,
            'buildPermissions',
          ]
      );
  }

  /**
   * Returns a list of homebox permissions for a given homebox type.
   *
   * @param \Drupal\homebox\Entity\HomeboxType $type
   *   The homebox type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(HomeboxType $type) {
    $homeboxType = $type->id();
    $type_params = ['%type_name' => $type->label()];

    return [
      "view own $homeboxType homebox" => [
        'title' => $this->t('%type_name: View own Homebox', $type_params),
        'description' => $this->t('Allows to access and view your own %type_name homebox', $type_params),
      ],
      "view any $homeboxType homebox" => [
        'title' => $this->t('%type_name: View any Homebox', $type_params),
        'description' => $this->t('Allows to access and view any %type_name homebox, including the homeboxes from other users.', $type_params),
        'restrict access' => TRUE,
      ],
      "personalize own $homeboxType homebox" => [
        'title' => $this->t('%type_name: Personalize own Homebox', $type_params),
        'description' => $this->t('Allows to add, edit, drag & drop, remove or reset portlets in your own %type_name homebox', $type_params),
      ],
      "personalize any $homeboxType homebox" => [
        'title' => $this->t('%type_name: Personalize any Homebox', $type_params),
        'description' => $this->t('Allows to add, edit, drag & drop, remove or reset portlets on any %type_name homebox, including the homeboxes from other users.', $type_params),
        'restrict access' => TRUE,
      ],
      "administer $homeboxType presets" => [
        'title' => $this->t('%type_name: Administer presets', $type_params),
        'description' => $this->t('Allows to add, edit, drag & drop, remove or reset portlets on any %type_name homebox preset.', $type_params),
        'restrict access' => TRUE,
      ],
    ];
  }

}

<?php

namespace Drupal\homebox\Form;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\homebox\Entity\HomeboxType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Homebox form.
 */
class HomeboxListFilterForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  protected const HOMEBOX_TYPE_FILTER_EMPTY_VALUE = '- Select a homebox type -';

  /**
   * Constructs a new NodeListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager object.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('entity_type.manager'),
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'homebox_homebox_list_filter';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, HomeboxType $homeboxType = NULL) {
    // If there is a homeboxType defined, we do not need any filters:
    if (!empty($homeboxType)) {
      return [];
    }

    // Load all available homeboxTypes for the homeboxType filter:
    $homeboxOptionsList = [];
    $homeboxTypeObjects = $this->entityTypeManager->getStorage('homebox_type')->loadMultiple();
    foreach ($homeboxTypeObjects as $homeboxTypeObjectId => $homeboxTypeObject) {
      $homeboxOptionsList[$homeboxTypeObjectId] = $homeboxTypeObject->label();
    }

    $form['filter'] = [
      '#type' => 'container',
    ];

    $form['filter']['homeboxType'] = [
      '#type' => 'select',
      '#title' => $this->t('Homebox Type'),
      '#options' => $homeboxOptionsList,
      '#empty_value' => static::HOMEBOX_TYPE_FILTER_EMPTY_VALUE,
      '#default_value' => $this->getRequest()->get('homeboxType') ?? '',
    ];

    // Only show the preset filter on the "normal" homebox collection:
    $form['filter']['isPreset'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only show presets'),
      '#default_value' => $this->getRequest()->get('isPreset') ?? FALSE,
    ];

    $form['actions']['wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['form-item']],
    ];

    $form['actions']['wrapper']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
    ];

    if ($this->getRequest()->getQueryString()) {
      $form['actions']['wrapper']['reset'] = [
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
        '#submit' => ['::resetForm'],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $query = [];

    $homeboxType = $form_state->getValue('homeboxType');
    if (!empty($homeboxType) && $homeboxType != static::HOMEBOX_TYPE_FILTER_EMPTY_VALUE) {
      $query['homeboxType'] = $homeboxType;
    }

    $isPreset = $form_state->getValue('isPreset');
    if (!empty($isPreset)) {
      $query['isPreset'] = $isPreset;
    }

    // Only redirect to the homebox collection, if we were on the collection
    // before:
    $form_state->setRedirect('entity.homebox.collection', $query);
  }

  /**
   * Callback to reset the form.
   */
  public function resetForm(array $form, FormStateInterface &$form_state) {
    // Only redirect to the homebox collection, if we were on the collection
    // before:
    $form_state->setRedirect('entity.homebox.collection');
  }

}

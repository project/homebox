<?php

declare(strict_types=1);

namespace Drupal\homebox\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxFormHelperTrait;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\Core\Url;
use Drupal\homebox\Entity\Homebox;
use Drupal\homebox\Entity\HomeboxInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use WebDriver\Exception\UnexpectedParameters;

/**
 * Provides a Homebox - Individual user dashboards form.
 */
final class HomeboxPortletAddForm extends FormBase {

  use AjaxFormHelperTrait;

  /**
   * The layout plugin manager.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutPluginManager;

  /**
   * Constructs a new HomeboxPortletAddForm object.
   *
   * @param \Drupal\Core\Layout\LayoutPluginManagerInterface $layoutPluginManager
   *   The layout plugin manager.
   */
  public function __construct(LayoutPluginManagerInterface $layoutPluginManager) {
    $this->layoutPluginManager = $layoutPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.core.layout')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'homebox_portlet_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, HomeboxInterface $homebox = NULL): array {
    $form['homebox_portlet_actions'] = [
      '#type' => 'actions',
      '#attributes' => [
        'class' => [
          'container-inline',
          'homebox-actions',
          'homebox-actions--portlet-actions',
        ],
      ],
      'homebox_actions_add' => [
        'portlet_add' => [
          '#type' => 'link',
          '#title' => $this->t('Add Portlet'),
          // If the homebox is not saved yet, we pass the ID of the preset!
          // So allowed portlets, singleton etc. can be determined without
          // having to save the homebox copy yet!
          '#url' => Url::fromRoute('homebox.personalize.portlet_add', [
            'homebox_type' => $homebox->bundle(),
            'owner' => $homebox->getOwnerId(),
          ]),
          '#attributes' => [
            'class' => [
              'button',
              'button--portlet-add',
              'button--primary',
              'use-ajax',
            ],
            // See https://www.drupal.org/docs/drupal-apis/ajax-api/ajax-dialog-boxes:
            // We are using a regular dialog and our own class to style it as
            // off-canvas bar, because the core off-canvas implementation
            // contains a CSS reset that makes it basically unusable for third
            // parties.
            'data-dialog-type' => 'dialog',
            'data-dialog-options' => Json::encode([
              // Width will be overridden in CSS - so its just fallback here.
              'width' => '25%',
              'classes' => [
                "ui-dialog" => "homebox-sidebar",
                'ui-dialog-titlebar' => 'homebox-sidebar__titlebar',
                'ui-dialog-title' => 'homebox-sidebar__title',
                'ui-dialog-titlebar-close' => 'homebox-sidebar__close',
                'ui-dialog-content' => 'homebox-sidebar__content',
              ],
            ]),
          ],
        ],
        '#attached' => [
          'library' => [
            'core/drupal.dialog.ajax',
            'homebox/builder',
          ],
        ],
      ],
      // The hidden add portlet form that gets populated by JS:
      'homebox_portlet_add_hidden' => [
        'add_homebox_portlet_type_id' => [
          '#type' => 'hidden',
          '#attributes' => [
            'id' => 'add-homebox-portlet-type-id',
            'class' => ['hidden'],
          ],
        ],
        'add_homebox_portlet_layout_region' => [
          '#type' => 'hidden',
          '#attributes' => [
            'id' => 'add-homebox-portlet-layout-region',
            'class' => ['hidden'],
          ],
        ],
        'add_homebox_portlet_layout_region_weight' => [
          '#type' => 'hidden',
          '#attributes' => [
            'id' => 'add-homebox-portlet-layout-region-weight',
            'class' => ['hidden'],
          ],
        ],
        'add_homebox_portlet_submit' => [
          '#type' => 'submit',
          '#value' => $this->t('Add portlet'),
          '#name' => 'addPortlet',
          '#attributes' => [
            'class' => ['hidden', 'add-homebox-portlet-submit'],
          ],
          '#ajax' => [
            'callback' => [$this, 'addPortletAjaxCallback'],
            // 'wrapper' => 'layout-wrapper',
            'event' => 'click',
          ],
        ],
      ],
      'homebox_reset' => [
        '#type' => 'submit',
        // Reset: Use icon, as inputs can't be styled by CSS properly:
        '#value' => '⎌ ' . $this->t('Reset'),
        '#access' => !$homebox->isPreset() && $homebox->access('delete'),
        '#attributes' => [
          'class' => [
            'button',
            'button--homebox-reset',
            'button--danger',
          ],
          'title' => $this->t('Reset to defaults (discards personalizations).'),
          'onClick' => 'return window.confirm(Drupal.t("Do you really want to reset this to defaults?") + "\n\n" + Drupal.t("This action can not be undone."))',
        ],
        '#submit' => ['::resetToPreset'],
          // No validation on reset:
        '#limit_validation_errors' => [],
      ],
    ];

    return $form;
  }

  /**
   * Form callback to reset the homebox to preset values.
   *
   * Technically this means, the personalized homebox is being deleted, so that
   * the preset homebox is used again instead.
   */
  public function resetToPreset(array $form, FormStateInterface $form_state) {
    // We need to get the homebox object from the form build information:
    /** @var \Drupal\homebox\Entity\HomeboxInterface $homebox */
    $homebox = $form_state->getBuildInfo()['args'][0];
    if (!$homebox instanceof HomeboxInterface) {
      throw new UnexpectedParameters('Homebox expected');
    }
    if (!$homebox->access('reset')) {
      // Safety net implementation:
      // @improve Perhaps this can be solved better in a more Drupal way?
      throw new AccessDeniedHttpException('You are not permitted to execute reset operations on this homebox!');
    }
    $form_state->setRedirectUrl(Homebox::getHomeboxUrlFromBreadcrumbs());

    $homebox->delete();
    $this->messenger()->addStatus(
      $this->t(
        'Your personalizations have been removed. @label has been reset.',
        [
          '@type' => $homebox->bundle(),
          '@label' => $homebox->label(),
        ]
      ),
      TRUE
    );

  }

  /**
   * {@inheritDoc}
   */
  protected function successfulAjaxSubmit(array $form, FormStateInterface $form_state) {
    // @improve It would be even better to do no redirect and load the new content
    // via AJAX but that might be super complex or even impossible?
    $response = new AjaxResponse();
    $command = new RedirectCommand(Url::fromRoute('<current>')->toString());
    $response->addCommand($command);
    return $response;
  }

  /**
   * Callback function for AJAX actions to refresh the layout on ajax calls.
   *
   * @return array
   *   The form part to refresh.
   */
  public function addPortletAjaxCallback(array &$form, FormStateInterface $form_state) {
    // Reload the updated page:
    return $this->ajaxSubmit($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // We need to get the homebox object from the form build information:
    /** @var \Drupal\homebox\Entity\HomeboxInterface $homebox */
    $homebox = $form_state->getBuildInfo()['args'][0];
    if (!$homebox instanceof HomeboxInterface) {
      throw new UnexpectedParameters('Homebox expected');
    }
    if (!$homebox->access('update')) {
      $form_state->setErrorByName('add_homebox_portlet_submit', $this->t('You are not permitted to execute @operation operations on this homebox!', ['%operation' => 'update']));
    }

    // @improve: Move all these validations into the HomeboxValidPortletConstraint:
    $pluginId = $form_state->getValue('add_homebox_portlet_type_id');
    $enabledPortletTypes = array_keys($homebox->getHomeboxType()->getHomeboxTypePortletTypesSettings(TRUE));
    if (!in_array($pluginId, $enabledPortletTypes)) {
      $form_state->setErrorByName('add_homebox_portlet_type_id', $this->t('The portlet type "@pluginId" is not allowed in this homebox!', [
        '@pluginId' => $pluginId,
      ]));
    }
    $region = $form_state->getValue('add_homebox_portlet_layout_region');
    $allowedLayoutRegions = $this->layoutPluginManager->getDefinition($homebox->getLayoutId())->getRegionNames();
    if (!in_array($region, $allowedLayoutRegions)) {
      $form_state->setErrorByName('layout_region', $this->t('The layout region "@layout_region" does not exist!', [
        '@layout_region' => $region,
      ]));
    }
    // Ensure weight is numeric:
    $weight = (int) $form_state->getValue('add_homebox_portlet_layout_region_weight');
    $homebox->addPortletByValues($pluginId, $region, $weight);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // We need to get the homebox object from the form build information:
    /** @var \Drupal\homebox\Entity\HomeboxInterface $homebox */
    $homebox = $form_state->getBuildInfo()['args'][0];
    if (!$homebox instanceof HomeboxInterface) {
      throw new UnexpectedParameters('Homebox expected');
    }
    if (!$homebox->access('update')) {
      // Safety net implementation:
      // @improve Perhaps this can be solved better in a more Drupal way?
      throw new AccessDeniedHttpException('You are not permitted to execute update operations on this homebox!');
    }

    // Save the values set in validation, if everything is fine:
    $homebox->save();
  }

}

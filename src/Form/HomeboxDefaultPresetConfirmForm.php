<?php

namespace Drupal\homebox\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\homebox\Entity\Homebox;
use Drupal\homebox\Entity\HomeboxType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The confirmation form for setting the default preset for a homebox type.
 */
class HomeboxDefaultPresetConfirmForm extends ConfirmFormBase {

  /**
   * The homebox preset to set as the default preset.
   *
   * @var \Drupal\homebox\Entity\Homebox
   */
  protected $homebox;

  /**
   * The homebox type of the homebox.
   *
   * @var \Drupal\homebox\Entity\HomeboxTypeInterface
   */
  protected $homeboxType;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new BanDelete object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('entity_type.manager')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'homebox_default_preset_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t(
          'Are you sure you want to set this homebox preset %homebox as the default preset for the %homeboxType homebox type?', [
            '%homebox' => $this->homebox->label(),
            '%homeboxType' => $this->homeboxType->label(),
          ]
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('entity.homebox.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Homebox $homebox = NULL) {
    $this->homebox = $homebox;
    $this->homeboxType = HomeboxType::load($homebox->bundle());
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($this->homebox->isPresetDefault()) {
      $form_state->setErrorByName(
            'actions', $this->t(
                'This homebox is already the default preset for the %homeboxType homebox type', [
                  '%homeboxType' => $this->homeboxType->label(),
                ]
            )
        );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $homeboxStorage = $this->entityTypeManager->getStorage('homebox');
    $defaultHomeboxes = $homeboxStorage->loadByProperties(
          [
            'type' => $this->homeboxType->id(),
            'isPreset' => TRUE,
            'isPresetDefault' => TRUE,
          ]
      );
    $countDefaultHomeboxes = count($defaultHomeboxes);
    if ($countDefaultHomeboxes > 1  || $countDefaultHomeboxes < 1) {
      throw new \Exception("There should be exactly one default preset per homebox type.");
    }

    /**
    * @var \Drupal\homebox\Entity\Homebox $defaultHomebox
    */
    $defaultHomebox = reset($defaultHomeboxes);
    $defaultHomebox->setPresetDefault(FALSE)->save();
    $this->homebox->setPresetDefault(TRUE)->save();
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}

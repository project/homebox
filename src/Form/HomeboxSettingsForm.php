<?php

namespace Drupal\homebox\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritdoc}
 */
class HomeboxSettingsForm extends ConfigFormBase {

  /**
   * The render cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $renderCache;

  /**
   * The dynamic page cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $dynamicPageCache;

  /**
   * PhotoswipeAdminSettings constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Cache\CacheBackendInterface $render_cache
   *   Constructor.
   * @param \Drupal\Core\Cache\CacheBackendInterface $dynamic_page_cache
   *   The dynamic page cache.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheBackendInterface $render_cache, CacheBackendInterface $dynamic_page_cache) {
    parent::__construct($config_factory);
    $this->renderCache = $render_cache;
    $this->dynamicPageCache = $dynamic_page_cache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache.render'),
      $container->get('cache.dynamic_page_cache')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'homebox_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('homebox.settings');

    $form['portletColors'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Available Portlet Color Options'),
      '#resizable' => 'vertical',
      '#default_value' => $this->portletColorsString($config->get('portletColors')),
      '#description' => $this->t('The list of portlet color options available to users.<br><strong>Enter one color per line in the format <code>color|Name</code>.</strong><br>All CSS-compatible values are allowed. (e.g. <code>green</code>, <code>#00127F</code>, <code>linear-gradient()</code>, etc.)'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Generates a string representation of an array of 'allowed values'.
   *
   * This string format is suitable for edition in a textarea.
   *
   * @param array $values
   *   An array of values, where array keys are values and array values are
   *   labels.
   *
   * @return string
   *   The string representation of the $values array:
   *    - Values are separated by a carriage return.
   *    - Each value is in the format "value|label" or "value".
   */
  protected function portletColorsString($values) {
    $lines = [];
    foreach ($values as $key => $value) {
      $lines[] = "{$key}|{$value}";
    }
    return implode("\n", $lines);
  }

  /**
   * Extracts the allowed values array from the allowed_values element.
   *
   * @param string $string
   *   The raw string to extract values from.
   *
   * @return array|null
   *   The array of extracted key/value pairs, or NULL if the string is invalid.
   *
   * @see \Drupal\options\Plugin\Field\FieldType\ListItemBase::allowedValuesString()
   */
  protected static function portletColorsArray($string) {
    $values = [];
    $list = explode("\n", $string);
    $list = array_map('trim', $list);
    $list = array_filter($list, 'strlen');
    foreach ($list as $text) {
      // Check for an explicit key.
      $matches = [];
      if (preg_match('/(.*)\|(.*)/', $text, $matches)) {
        // Trim key and value to avoid unwanted spaces issues.
        $key = trim($matches[1]);
        $value = trim($matches[2]);
        $values[$key] = $value;
      }
    }
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('homebox.settings')
      ->set('portletColors', $this->portletColorsArray($form_state->getValue('portletColors')))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['homebox.settings'];
  }

}

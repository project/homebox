<?php

namespace Drupal\homebox\Form;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutPluginManager;
use Drupal\Core\Path\PathValidator;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\ProxyClass\Routing\RouteBuilder;
use Drupal\homebox\HomeboxPortletTypePluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * General settings for a Homebox Type.
 */
class HomeboxTypeForm extends EntityForm {

  /**
   * The layout plugin manager.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManager
   */
  protected $layoutPluginManager;

  /**
   * A plugin cache clear instance.
   *
   * @var \Drupal\Core\Plugin\CachedDiscoveryClearerInterface
   */
  protected $pluginCacheClearer;

  /**
   * A cache backend interface instance.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRender;

  /**
   * The route builder instance.
   *
   * @var \Drupal\Core\ProxyClass\Routing\RouteBuilder
   */
  protected $routeBuilder;

  /**
   * The path validator instance.
   *
   * @var \Drupal\Core\Path\PathValidator
   */
  protected $pathValidator;

  /**
   * The homebox portlet type plugin manager.
   *
   * @var \Drupal\homebox\HomeboxPortletTypePluginManager
   */
  protected $homeboxPortletTypePluginManager;

  /**
   * HomeboxForm constructor.
   *
   * @param \Drupal\Core\Layout\LayoutPluginManager $layoutPluginManager
   *   The layout plugin manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
   *   A plugin cache clear instance.
   * @param \Drupal\Core\ProxyClass\Routing\RouteBuilder $routeBuilder
   *   The route builder instance.
   * @param \Drupal\Core\Path\PathValidator $pathValidator
   *   The path validator instance.
   * @param \Drupal\homebox\HomeboxPortletTypePluginManager $homeboxPortletTypePluginManager
   *   The homebox portlet type plugin manager.
   */
  public function __construct(LayoutPluginManager $layoutPluginManager, CacheBackendInterface $cacheRender, CachedDiscoveryClearerInterface $plugin_cache_clearer, RouteBuilder $routeBuilder, PathValidator $pathValidator, HomeboxPortletTypePluginManager $homeboxPortletTypePluginManager) {
    $this->layoutPluginManager = $layoutPluginManager;
    $this->cacheRender = $cacheRender;
    $this->pluginCacheClearer = $plugin_cache_clearer;
    $this->routeBuilder = $routeBuilder;
    $this->pathValidator = $pathValidator;
    $this->homeboxPortletTypePluginManager = $homeboxPortletTypePluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /**
     * @var \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $plugin_cache_clearer
     */
    $plugin_cache_clearer = $container->get('plugin.cache_clearer');
    /**
     * @var \Drupal\Core\Cache\CacheBackendInterface $cache_render
     */
    $cache_render = $container->get('cache.render');
    /**
     * @var \Drupal\Core\Layout\LayoutPluginManager $layout_plugin_manager
     */
    $layout_plugin_manager = $container->get('plugin.manager.core.layout');
    /**
     * @var \Drupal\Core\ProxyClass\Routing\RouteBuilder $route_builder
     */
    $route_builder = $container->get('router.builder');
    /**
     * @var \Drupal\Core\Path\PathValidator $path_validator
     */
    $path_validator = $container->get('path.validator');
    /**
     * @var \Drupal\homebox\HomeboxPortletTypePluginManager $homeboxPortletTypePluginManager
     */
    $homeboxPortletTypePluginManager = $container->get('plugin.manager.homebox_portlet_type');
    return new static(
      $layout_plugin_manager,
      $cache_render,
      $plugin_cache_clearer,
      $route_builder,
      $path_validator,
      $homeboxPortletTypePluginManager
    );
  }

  /**
   * Provide layout plugin instance.
   *
   * @param string $id
   *   Layout id.
   * @param array $layout_settings
   *   Layout settings.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return mixed|object
   *   Layout plugin.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getLayout($id, array $layout_settings, FormStateInterface $form_state) {
    if (!$layout_plugin = $form_state->get('layout_plugin')) {
      $layout_plugin = $this->layoutPluginManager->createInstance($id, $layout_settings);
      $form_state->set('layout_plugin', $layout_plugin);
    }

    return $layout_plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /**
     * @var \Drupal\homebox\Entity\HomeboxTypeInterface $homeboxType
     */
    $homeboxType = $this->entity;

    if ($this->operation == 'edit') {
      $form['#title'] = $this->t(
        '<em>Edit Homebox Type:</em> @type', [
          '@type' => $homeboxType->label(),
        ]
      );
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Homebox Type Label'),
      '#maxlength' => 255,
      '#default_value' => $homeboxType->label(),
      '#description' => $this->t("Label for the current homebox type entity. Leave empty to use the default."),
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $homeboxType->getOriginalId(),
      '#machine_name' => [
        'exists' => '\Drupal\homebox\Entity\HomeboxType::load',
      ],
      '#disabled' => !$homeboxType->isNew(),
    ];

    $form['homebox_vertical_tabs'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Settings'),
    ];

    $form['general_form'] = [
      '#type' => 'details',
      '#title' => $this->t('General'),
      '#description' => $this->t('General homebox type settings'),
      '#description_display' => 'after',
      '#group' => 'homebox_vertical_tabs',
    ];

    $form['general_form']['showTitle'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show page title'),
      '#description' => $this->t("Show the page title above the homebox."),
      '#default_value' => $homeboxType->getShowTitle(),
    ];

    $form['general_form']['layoutIds'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Allowed Layouts'),
      '#options' => $this->layoutPluginManager->getLayoutOptions(),
      '#description' => $this->t("Set the allowed column layouts for this type's homeboxes. Leave empty to allow all layouts."),
      '#default_value' => $homeboxType->getLayoutIds() ?: ['homebox_default'],
    ];

    $homeboxPorletTypesDefinitions = $this->homeboxPortletTypePluginManager->getDefinitions();

    if (!empty($homeboxPorletTypesDefinitions)) {
      $form['portlet_form'] = [
        '#type' => 'details',
        '#title' => $this->t('Portlet Types Settings'),
        '#description' => $this->t("
          Define the allowed portlet types, for all homeboxes of this type.<br>
          Note, that disabled values are not overridable, because the specific portlet definition overrides these values.<br><br>
          <dl>
           <dt>Enabled</dt><dd>Whether this portlet type is enabled. (Default: TRUE)</dd>
           <dt>Singleton</dt><dd>Whether only one instance of this portlet type can be used in the homebox personalization. (Default: FALSE)</dd>
           <dt>Draggable</dt><dd>Whether instances of this portlet type are draggable in the homebox personalization. (Default: TRUE)</dd>
           <dt>Hideable</dt><dd>Whether instances of this portlet type can be hidden / disable din the homebox personalization. (Default: TRUE)</dd>
           <dt>Removable</dt><dd>Whether instances of this portlet type can be removed in the homebox personalization. Especially useful for required portlets combined with \"Singleton\". (Default: TRUE)</dd>
           <dt>Weight</dt><dd>The weight of this portlet type (esp. in the \"Add portlet\" widget). Keep in mind that this only affects sorting within groups and groups are for now ordered by label. This can currently only be overridden programmatically using <code>HOOK_preprocess_homebox_add_portlet_groups()</code> (Default: 0)</dd>
        "),
        '#description_display' => 'after',
        '#group' => 'homebox_vertical_tabs',
      ];

      // We FIRST have to build the values array without the render array
      // # properties, so we can sort them:
      // The portlet type definitions are coming from the specific
      // homeboxPorletType Annotation, usually only the id and label are set
      // there. But some portlet type creators might want to set their portlet
      // settings hard in the annotations (e.g. singleton = TRUE).
      // Therefore, these definitions have the highest priority when checking
      // for the form value:
      foreach ($homeboxPorletTypesDefinitions as $plugin_id => $homeboxPortletTypeDefinition) {
        $currentHomeboxPortletType = $homeboxType->getHomeboxTypePortletTypeSettings($plugin_id);
        $form['portlet_form']['homeboxTypePortletTypesSettings'][$plugin_id]['id'] = [
          '#type' => 'markup',
          '#markup' => $homeboxPortletTypeDefinition['id'],
          '#title_display' => 'invisible',
          '#attributes' => [
            'class' => [],
          ],
        ];
        $form['portlet_form']['homeboxTypePortletTypesSettings'][$plugin_id]['group'] = [
          '#type' => 'markup',
          '#markup' => $homeboxPortletTypeDefinition['group'],
          '#title_display' => 'invisible',
          '#attributes' => [
            'class' => [
              RESPONSIVE_PRIORITY_MEDIUM,
            ],
          ],
        ];
        $form['portlet_form']['homeboxTypePortletTypesSettings'][$plugin_id]['label'] = [
          // We need to set this as a disabled textfield, otherwise the value
          // won't be saved in the entity (when using markup instead):
          '#type' => 'textfield',
          '#value' => $currentHomeboxPortletType['label'] ?? '',
          '#size' => 24,
          '#placeholder' => $homeboxPortletTypeDefinition['label'],
          '#title_display' => 'invisible',
        ];
        $form['portlet_form']['homeboxTypePortletTypesSettings'][$plugin_id]['description'] = [
          // We need to set this as a disabled textfield, otherwise the value
          // won't be saved in the entity (when using markup instead):
          '#type' => 'textfield',
          '#value' => $currentHomeboxPortletType['description'] ?? '',
          '#size' => 24,
          '#placeholder' => $homeboxPortletTypeDefinition['description'],
          '#title_display' => 'invisible',
        ];
        $form['portlet_form']['homeboxTypePortletTypesSettings'][$plugin_id]['status'] = [
          '#type' => 'checkbox',
          '#default_value' => $currentHomeboxPortletType['status'] ?? TRUE,
          '#title_display' => 'invisible',
        ];
        $form['portlet_form']['homeboxTypePortletTypesSettings'][$plugin_id]['singleton'] = [
          '#type' => 'checkbox',
          '#default_value' => $homeboxPortletTypeDefinition['singleton'] ?? $currentHomeboxPortletType['singleton'] ?? FALSE,
          '#title_display' => 'invisible',
          '#disabled' => isset($homeboxPortletTypeDefinition['singleton']),
        ];
        $form['portlet_form']['homeboxTypePortletTypesSettings'][$plugin_id]['draggable'] = [
          '#type' => 'checkbox',
          '#default_value' => $homeboxPortletTypeDefinition['draggable'] ?? $currentHomeboxPortletType['draggable'] ?? TRUE,
          '#title_display' => 'invisible',
          '#disabled' => isset($homeboxPortletTypeDefinition['draggable']),
        ];
        $form['portlet_form']['homeboxTypePortletTypesSettings'][$plugin_id]['hideable'] = [
          '#type' => 'checkbox',
          '#default_value' => $homeboxPortletTypeDefinition['hideable'] ?? $currentHomeboxPortletType['hideable'] ?? TRUE,
          '#title_display' => 'invisible',
          '#disabled' => isset($homeboxPortletTypeDefinition['hideable']),
        ];
        $form['portlet_form']['homeboxTypePortletTypesSettings'][$plugin_id]['removable'] = [
          '#type' => 'checkbox',
          '#default_value' => $homeboxPortletTypeDefinition['removable'] ?? $currentHomeboxPortletType['removable'] ?? TRUE,
          '#title_display' => 'invisible',
          '#disabled' => isset($homeboxPortletTypeDefinition['removable']),
        ];
        $form['portlet_form']['homeboxTypePortletTypesSettings'][$plugin_id]['weight'] = [
          '#type' => 'weight',
          '#default_value' => !empty($homeboxPortletTypeDefinition['weight']) ? $homeboxPortletTypeDefinition['weight'] : $currentHomeboxPortletType['weight'] ?? 0,
          '#title_display' => 'invisible',
          '#attributes' => ['class' => ['table-order-weight']],
        ];
        $form['portlet_form']['homeboxTypePortletTypesSettings'][$plugin_id]['#attributes']['class'][] = 'draggable';
        $form['portlet_form']['homeboxTypePortletTypesSettings'][$plugin_id]['#weight'] = $form['portlet_form']['homeboxTypePortletTypesSettings'][$plugin_id]['weight']['#default_value'];
      }

      // Now sort the table values by #weight:
      uasort($form['portlet_form']['homeboxTypePortletTypesSettings'], function ($a, $b) {
        return SortArray::sortByWeightProperty($a, $b);
      });

      // AFTER sorting we can now merge in the render array structure:
      $form['portlet_form']['homeboxTypePortletTypesSettings'] = array_merge($form['portlet_form']['homeboxTypePortletTypesSettings'], [
        '#type' => 'table',
        '#responsive' => TRUE,
        '#header' => [
          [
            'data' => $this->t('Portlet Type ID'),
            'class' => [RESPONSIVE_PRIORITY_LOW],
          ],
          [
            'data' => $this->t('Group'),
            'class' => [RESPONSIVE_PRIORITY_MEDIUM],
          ],
          $this->t('Label'),
          [
            'data' => $this->t('Description'),
            'class' => [RESPONSIVE_PRIORITY_MEDIUM],
          ],
          $this->t('Enabled'),
          $this->t('Singleton'),
          $this->t('Draggable'),
          $this->t('Hideable'),
          $this->t('Removable'),
          $this->t('Weight'),
        ],
        '#tableselect' => FALSE,
        '#id' => 'homebox-portlet-type-settings-table',
        '#tabledrag' => [
          [
            // The HTML ID of the table to make draggable. See #id above.
            'table_id' => 'homebox-portlet-type-settings-table',
            // The action to be done on the form item. Either 'match' 'depth',
            // or 'order'.
            'action' => 'order',
            // String describing where the "action" option should be performed.
            // Either 'parent', 'sibling', 'group', or 'self'.
            'relationship' => 'sibling',
            // Class name applied on all related form elements for this action.
            // See below.
            'group' => 'table-order-weight',
          ],
        ],
      ]);
    }
    else {
      $this->messenger()->addError('There are no portlet types available. Please install a module that provides portlet types. Check out the "Homebox Portlet Type: Examples" submodule for example implementations.');
    }

    $form['page_form'] = [
      '#type' => 'details',
      '#title' => $this->t('Homebox Page'),
      '#description_display' => 'after',
      '#group' => 'homebox_vertical_tabs',
    ];

    $form['page_form']['pageEnabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Homebox Page'),
      '#default_value' => $homeboxType->getPageEnabled(),
      '#description' => $this->t("Enables a homebox page."),
    ];

    $form['page_form']['pagePath'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Homebox Page Path'),
      '#maxlength' => 255,
      '#default_value' => $homeboxType->getPagePath(),
      '#description' => $this->t("Specify a URL by which this page can be accessed from. Use a relative path, for example '/homebox' or '/dashboard'."),
      '#element_validate' => [[$this, 'validatePath']],
      '#states' => [
        'visible' => [
          ':input[name="pageEnabled"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="pageEnabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['user_tab_form'] = [
      '#type' => 'details',
      '#title' => $this->t('Homebox User Tab'),
      '#description_display' => 'after',
      '#group' => 'homebox_vertical_tabs',
    ];

    $form['user_tab_form']['userTabEnabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Homebox User Tab'),
      '#default_value' => $homeboxType->getUserTabEnabled(),
      '#description' => $this->t('Enables a homebox user tab page.'),
    ];
    return $form;
  }

  /**
   * Validation callback to validate the 'path' form element.
   *
   * @param array $element
   *   The form element to validate.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validatePath(array &$element, FormStateInterface $form_state) {
    if (!empty($form_state->getValue('pageEnabled')) && !empty($element['#value'])) {
      /**
       * @var \Drupal\homebox\Entity\HomeboxTypeInterface $homeboxType
       */
      $homeboxType = $this->entity;
      // Ensure the path has a leading slash.
      if ($element['#value'][0] !== '/') {
        $form_state->setErrorByName('pagePath', $this->t('Manually entered paths should start with a leading slash ("/").'));
      }
      if ($value = trim($element['#value'], '/')) {
        $value = '/' . $value;
        $form_state->setValueForElement($element, $value);

        // Ensure that the path is unique:
        if ($this->pathValidator->getUrlIfValidWithoutAccessCheck($value) !== FALSE && $homeboxType->getpagePath() != $value) {
          $form_state->setErrorByName('pagePath', $this->t('The path exists already, and is therefore not unique.'));
        }
      }
      // Check to make sure the path exists after stripping slashes.
      else {
        $form_state->setErrorByName('pagePath', $this->t("Path is required."));
      }
    }
  }

  /**
   * Retreives the user roles.
   *
   * @return array
   *   The user array.
   */
  protected function getRolesList(): array {
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    $role_list = [];
    foreach ($roles as $role) {
      if (!$role->isAdmin() && $role->id() !== 'anonymous') {
        $role_list[$role->id()] = $role->label();
      }
    }
    return $role_list;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /**
     * @var \Drupal\homebox\Entity\HomeboxTypeInterface $homeboxType
     */
    $homeboxType = $this->entity;

    // Set show title:
    $homeboxType->setShowTitle(!empty($form_state->getValue('showTitle')));

    // Bring layout ids in the correct array structure:
    if (empty($form_state->getValue('layoutIds'))) {
      $layoutOptions = $this->layoutPluginManager->getLayoutOptions();
      $layoutKeys = [];
      foreach ($layoutOptions as $layouts) {
        $layoutKeys = array_merge($layoutKeys, array_keys($layouts));
      }
      $homeboxType->setLayoutIds($layoutKeys);
    }
    // If the homebox page is disabled we need to clear the homebox page path:
    if (!$homeboxType->getPageEnabled()) {
      $homeboxType->setPagePath('');
    }
    $status = parent::save($form, $form_state);
    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage(
          $this->t(
            'Created the @label Homebox Type.', [
              '@label' => $homeboxType->label(),
            ]
          )
        );
        break;

      default:
        $this->messenger()->addMessage(
          $this->t(
            'Saved the @label Homebox Type.', [
              '@label' => $homeboxType->label(),
            ]
          )
        );
    }
    // We need to let our RouteSubscriber know,
    // that there could be a new page path change:
    $this->routeBuilder->setRebuildNeeded();
    // Clear cache to rebuild tasks links.
    $this->pluginCacheClearer->clearCachedDefinitions();
    $this->cacheRender->invalidateAll();
    $form_state->setRedirectUrl($homeboxType->toUrl('collection'));
    return $status;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate the portlet type settings.
    $homeboxPorletTypesDefinitions = $this->homeboxPortletTypePluginManager->getDefinitions();
    if (empty($homeboxPorletTypesDefinitions)) {
      $form_state->setErrorByName('homebox_vertical_tabs', $this->t('No portlet types available.'));
    }
  }

}

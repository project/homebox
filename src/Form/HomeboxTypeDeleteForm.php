<?php

namespace Drupal\homebox\Form;

use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\homebox\Entity\Homebox;

/**
 * Provides a form for deleting Homebox entities.
 *
 * @ingroup homebox
 */
class HomeboxTypeDeleteForm extends EntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $numHomeboxes = $this->entityTypeManager->getStorage('homebox')->getQuery()
      ->condition('type', $this->entity->id())
      ->accessCheck(FALSE)
      ->count()
      ->execute();
    if ($numHomeboxes) {
      $caption = '<p>' . $this->formatPlural($numHomeboxes, '%type Homeboxes type is used by 1 piece of content on your site. You can not remove this %type Homeboxes type until you have removed all from the content.', '%type Homeboxes type is used by @count pieces of content on your site. You may not remove %type Homeboxes type until you have removed all from the content.', ['%type' => $this->entity->label()]) . '</p>';
      $form['#title'] = $this->getQuestion();
      $form['description'] = ['#markup' => $caption];

      // Optional to delete existing entities.
      $form['delete_entities'] = [
        '#type' => 'submit',
        '#submit' => [[$this, 'deleteExistingEntities']],
        '#value' => $this->formatPlural($numHomeboxes, 'Delete existing Homebox', 'Delete all @count existing Homeboxes'),
      ];
      return $form;
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Form submit callback to delete homeboxes.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function deleteExistingEntities(array $form, FormStateInterface $form_state) {
    $storage = $this->entityTypeManager->getStorage('homebox');
    $ids = $storage->getQuery()
      ->condition('type', $this->entity->id())
      ->accessCheck(FALSE)
      ->execute();

    if (!empty($ids)) {
      $homeboxes = Homebox::loadMultiple($ids);

      // Delete existing entities.
      $storage->delete($homeboxes);
      $this->messenger()->addMessage($this->formatPlural(count($homeboxes), 'Entity is successfully deleted.', 'All @count entities are successfully deleted.'));
    }

    // Set form to rebuild.
    $form_state->setRebuild();
  }

}

<?php

namespace Drupal\homebox\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Homebox Form.
 *
 * @ingroup homebox
 */
class HomeboxForm extends ContentEntityForm {

  /**
   * The Current User object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The context of the form.
   *
   * @var string|null
   */
  protected $context;

  /**
   * {@inheritDoc}
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL, AccountInterface $current_user) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->currentUser = $current_user;

    // Get the context from the request query (if it isn't set this will be
    // NULL):
    $this->context = $this->getRequest()->query->get('context');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('entity.repository'),
          $container->get('entity_type.bundle.info'),
          $container->get('datetime.time'),
          $container->get('current_user'),
      );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    // If the current operation is add, we are creating a new Homebox preset.
    // or creating a homebox for another user.
    if ($this->operation !== 'add') {
      $this->messenger()->addWarning("This form is mainly used for debugging and selecting the layout to use.\n Therefore, most of the values here cannot be changed!\n Please use the \"View\" tab to configure the Portlets.");
    }

    /**
    * @var \Drupal\homebox\Entity\HomeboxTypeInterface $homeboxType
    */
    $homeboxType = $this->entity->getHomeboxType();

    if ($this->operation == 'edit') {
      $form['#title'] = $this->t(
            '<em>Edit @type Homebox:</em> @title', [
              '@type' => $homeboxType->label(),
              '@title' => $this->entity->label(),
            ]
        );
    }
    elseif ($this->operation == 'add') {
      switch ($this->context) {
        case 'preset_homebox':
          $form['#title'] = $this->t('Add @type Homebox preset', ['@type' => $homeboxType->label()]);
          break;

        case 'user_homebox':
          $form['#title'] = $this->t('Add @type Homebox', ['@type' => $homeboxType->label()]);

        default:
          // Do nothing.
          break;
      }
    }

    // Changed must be sent to the client, for later overwrite error checking.
    $form['changed'] = [
      '#type' => 'hidden',
      '#default_value' => $this->entity->getChangedTime(),
    ];
    $form = parent::form($form, $form_state);

    switch ($this->operation) {
      // If we add a homebox, we don't want to show most fields:
      case 'add':
        $form['label']['#access'] = FALSE;
        $form['isPresetDefault']['widget']['value']['#default_value'] = FALSE;
        $form['isPresetDefault']['#disabled'] = TRUE;
        $form['creator']['#access'] = FALSE;
        $form['created']['#access'] = FALSE;
        $form['portlets']['#access'] = FALSE;
        if ($this->context == 'user_homebox') {
          $form['isPreset']['widget']['value']['#default_value'] = FALSE;
          $form['isPreset']['#disabled'] = TRUE;
        }
        elseif ($this->context == 'preset_homebox') {
          $form['isPreset']['widget']['value']['#default_value'] = TRUE;
          $form['isPreset']['#disabled'] = TRUE;
        }
        break;

      // On edit, we simply want to disable all of the fields:
      case 'edit':
        $form['owner']['#disabled'] = TRUE;
        $form['isPreset']['#disabled'] = TRUE;
        $form['label']['#disabled'] = TRUE;
        $form['isPresetDefault']['#disabled'] = TRUE;
        $form['creator']['#disabled'] = TRUE;
        $form['created']['#disabled'] = TRUE;
        $form['portlets']['#disabled'] = TRUE;
        break;
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /**
    * @var \Drupal\homebox\Entity\HomeboxTypeInterface $homeboxType
    */
    $homeboxType = $this->entity->getHomeboxType();

    $isPreset = reset($form_state->getValue('isPreset'));
    if (!$isPreset) {
      $ownerId = reset($form_state->getValue('uid'))['target_id'];
      $existingHomeboxes = $this->entityTypeManager->getStorage('homebox')
        ->getQuery()
        ->accessCheck(FALSE)
        ->condition('type', $homeboxType->id())
        ->condition('uid', $ownerId)
        ->condition('isPreset', FALSE)
        ->range(0, 1)
        ->execute();
      if (!empty($existingHomeboxes)) {
        $form_state->setErrorByName('uid', $this->t('A @homeboxType homebox already exists for this owner.', ['@homeboxType' => $homeboxType->label()]));
      }
    }
    return parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);

    /**
    * @var \Drupal\homebox\Entity\HomeboxTypeInterface $homeboxType
    */
    $homeboxType = $this->entity->getHomeboxType();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage(
              $this->t(
                  'Created the %label Homebox.', [
                    '%label' => $this->entity->label(),
                  ]
              )
          );
        break;

      default:
        $this->messenger()->addMessage(
              $this->t(
                  'Saved the %label Homebox.', [
                    '%label' => $this->entity->label(),
                  ]
              )
          );
    }
    switch ($this->context) {
      case 'preset_homebox':
        $form_state->setRedirect('entity.homebox_type.presets', ['homebox_type' => $homeboxType->id()]);
        break;

      case 'user_homebox':
        $form_state->setRedirect('entity.homebox_type.homeboxes', ['homebox_type' => $homeboxType->id()]);
        break;

      default:
        $form_state->setRedirect('entity.homebox.collection');
        break;
    }
    return $status;
  }

}

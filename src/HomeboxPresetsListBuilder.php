<?php

namespace Drupal\homebox;

/**
 * Defines a class to build a listing of Homebox preset entities.
 *
 * @ingroup homebox
 */
class HomeboxPresetsListBuilder extends HomeboxListBuilder {

  /**
   * Builds the query to retrieve the homebox presets.
   */
  protected function buildQuery() {
    // We are on the preset homebox collection, only load homeboxes with the
    // current homebox type and is_preset true:
    $query = $this->getStorage()->getQuery()
      ->accessCheck(TRUE)
      ->condition('type', $this->homeboxType->id())
      ->condition('isPreset', TRUE);

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }

    return $query;
  }

}

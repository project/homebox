<?php

namespace Drupal\homebox\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a homebox portlet type annotation object.
 *
 * Plugin Namespace: Plugin\HomeboxPortletType.
 *
 * Note, any definition with variables that are not NULL, will override the
 * values set inside the HomeboxType portletType array.
 *
 * @see \Drupal\homebox\HomeboxPortletTypePluginManager
 * @see \Drupal\homebox\Plugin\HomeboxPortletTypeInterface
 * @see \Drupal\homebox\Plugin\HomeboxPortletTypeBase
 * @see plugin_api
 *
 * @Annotation
 */
class HomeboxPortletType extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The human-readable administration label of the homebox portlet type.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $admin_label;

  /**
   * Additional administrative information about the homebox portlet type.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $admin_description;

  /**
   * The human-readable user label of the homebox portlet type.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

  /**
   * Additional description for the homebox portlet type.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $description;

  /**
   * Additional administrative information about the homebox portlet type group.
   *
   * This property is required.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $group;

  /**
   * Whether this HomeboxPortletType can only be used once inside a homebox.
   *
   * This property is optional and it does not need to be declared.
   * If it is declared, its value can not be changed in the
   * Portlet Type Settings (disabled)!
   *
   * @var bool|null
   */
  public ?bool $singleton = NULL;

  /**
   * Whether this HomeboxPortletType is draggable.
   *
   * This property is optional and it does not need to be declared.
   * If it is declared, its value can not be changed in the
   * Portlet Type Settings (disabled)!
   *
   * @var bool|null
   */
  public ?bool $draggable = NULL;

  /**
   * Whether this HomeboxPortletType is hideable.
   *
   * This property is optional and it does not need to be declared.
   * If it is declared, its value can not be changed in the
   * Portlet Type Settings (disabled)!
   *
   * @var bool|null
   */
  public ?bool $hideable = NULL;

  /**
   * Whether this HomeboxPortletType is removable.
   *
   * This property is optional and it does not need to be declared.
   * If it is declared, its value can not be changed in the
   * Portlet Type Settings (disabled)!
   *
   * @var bool|null
   */
  public ?bool $removable = NULL;

  /**
   * The weight of this portlet type (esp. in the "add portlet" form=.
   *
   * @var int
   */
  public int $weight = 0;

}

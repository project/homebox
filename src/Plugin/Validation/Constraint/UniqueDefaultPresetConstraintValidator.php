<?php

namespace Drupal\homebox\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the UniqueInteger constraint.
 *
 * Note, this constraint validator is very specifc to the isPresetDefault field
 * as it won't make sense to use it with any other field!
 *
 * Cardinality unlimited is NOT supported!
 */
class UniqueDefaultPresetConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    /**
* @var \Drupal\Core\Field\FieldItemList $value
*/
    $homebox = $value->getParent()->getValue();
    $homeboxTypeId = $homebox->bundle();
    $homeboxStorage = $this->entityTypeManager->getStorage('homebox');
    // First look for the users individual homebox instance:
    $defaultPresets = $homeboxStorage->loadByProperties(
          [
            'type' => $homeboxTypeId,
            'isPreset' => TRUE,
            'isPresetDefault' => TRUE,
          ]
      );

    $checkboxValue = $value->getValue()[0]['value'];
    $defaultPreset = reset($defaultPresets);
    if (!empty($checkboxValue) && !empty($defaultPreset) && $defaultPreset->id() != $homebox->id()) {
      $this->context->addViolation($constraint->alreadyExists);
    }
    if ($checkboxValue == FALSE && !empty($defaultPreset) && $defaultPreset->id() == $homebox->id()) {
      $this->context->addViolation($constraint->lastRemainingDefaultPreset);
    }
  }

}

<?php

namespace Drupal\homebox\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the default preset value is only enabled once per homebox type.
 *
 * @Constraint(
 *   id = "uniqueDefaultPreset",
 *   label = @Translation("Unique Default Preset", context = "Validation"),
 *   type = "boolean"
 * )
 */
class UniqueDefaultPresetConstraint extends Constraint {

  /**
   * The message that will be shown if there already is a default preset set.
   *
   * @var string
   */
  public $alreadyExists = 'There is already an existing default preset set for this type.';

  /**
   * The message that will be shown if this is the last default preset.
   *
   * @var string
   */
  public $lastRemainingDefaultPreset = 'There needs to be atleast one default preset set for this type.';

}

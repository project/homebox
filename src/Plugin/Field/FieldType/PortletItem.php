<?php

namespace Drupal\homebox\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'homebox_portlet' field type.
 *
 * @FieldType(
 *   id = "homebox_portlet",
 *   label = @Translation("Portlet"),
 *   description = @Translation("Displays homebox portlets."),
 *   default_widget = "homebox_portlet_widget",
 * )
 */
class PortletItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('portletTypeId')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'portletTypeId';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['portletTypeId'] = DataDefinition::create('string')
      ->setLabel(t('Homebox portlet type ID'))
      ->setRequired(TRUE);
    $properties['layoutRegion'] = DataDefinition::create('string')
      ->setLabel(t('Layout region'))
      ->setRequired(TRUE);
    $properties['layoutRegionWeight'] = DataDefinition::create('integer')
      ->setLabel(t('Layout region weight'))
      ->setRequired(TRUE);
    $properties['portletLabel'] = DataDefinition::create('string')
      ->setLabel(t('Portlet label override'));
    $properties['thirdPartySettings'] = DataDefinition::create('string')
      ->setLabel(t('Portlet settings'));
    $properties['enabled'] = DataDefinition::create('boolean')
      ->setLabel(t('Enabled'));
    return $properties;
  }

  /**
   * Returns the portlet type ID of this portlet.
   *
   * @return string
   *   The portlet type ID of this portlet.
   */
  public function getPortletTypeId(): ?string {
    return $this->get('portletTypeId')->getValue();
  }

  /**
   * Returns the layout region of this portlet.
   *
   * @return string
   *   The layout region of this portlet.
   */
  public function getLayoutRegion(): ?string {
    return $this->get('layoutRegion')->getValue();
  }

  /**
   * Returns the layout region weight of this portlet..
   *
   * @return int
   *   The layout region weight of this portlet.
   */
  public function getLayoutRegionWeight(): ?int {
    return $this->get('layoutRegionWeight')->getValue();
  }

  /**
   * Returns the portlet label.
   *
   * @return ?string
   *   The overwritten label of this portlet.
   */
  public function getPortletLabel(): ?string {
    return $this->get('portletLabel')->getValue();
  }

  /**
   * Returns true if the portlet is enabled, else false.
   *
   * @return bool
   *   Whether the portlet is enabled.
   */
  public function isEnabled(): bool {
    return $this->get('enabled')->getValue() ?? TRUE;
  }

  /**
   * Returns the settings raw text of this portlet.
   *
   * @return ?string
   *   The settings raw text of this portlet.
   */
  public function getThirdPartySettings(): ?string {
    return $this->get('thirdPartySettings')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    parent::preSave();

    // Set NULL if any kind of empty:
    if (empty($this->thirdPartySettings) || $this->thirdPartySettings === '{}') {
      $this->set('thirdPartySettings', NULL);
    }
    else {
      $this->set('thirdPartySettings', trim($this->get('thirdPartySettings')->getValue()));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'portletTypeId' => [
        'type' => 'varchar',
        'not null' => TRUE,
        'description' => 'Homebox Portlet Type ID',
        'length' => 255,
      ],
      'layoutRegion' => [
        'type' => 'varchar',
        'not null' => TRUE,
        'description' => 'Layout Region',
        'length' => 255,
      ],
      'layoutRegionWeight' => [
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => FALSE,
        'description' => 'Layout Region Weight',
        'size' => 'tiny',
      ],
      'portletLabel' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'Portlet overwritten label',
        'length' => 255,
      ],
      'enabled' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1,
        'description' => 'Portlet enabled status',
      ],
      'thirdPartySettings' => [
        'type' => 'text',
        'not null' => FALSE,
        'description' => 'Portlet third party settings settings',
        'size' => 'normal',
      ],
    ];

    $schema = [
      'columns' => $columns,
    ];

    return $schema;
  }

}

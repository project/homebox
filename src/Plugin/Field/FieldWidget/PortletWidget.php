<?php

namespace Drupal\homebox\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\homebox\Entity\HomeboxType;
use Drupal\homebox\HomeboxPortletTypePluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the 'homebox_portlet_widget' field widget.
 *
 * @FieldWidget(
 *   id = "homebox_portlet_widget",
 *   label = @Translation("Portlet Widget"),
 *   field_types = {"homebox_portlet"},
 * )
 */
class PortletWidget extends WidgetBase {

  /**
   * The element info manager.
   */
  protected HomeboxPortletTypePluginManager $homeboxPortletTypeManager;

  /**
   * The layout plugin manager.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutManager;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, HomeboxPortletTypePluginManager $homeboxPortletTypeManager, LayoutPluginManagerInterface $layoutManager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->homeboxPortletTypeManager = $homeboxPortletTypeManager;
    $this->layoutManager = $layoutManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['third_party_settings'], $container->get('plugin.manager.homebox_portlet_type'), $container->get('plugin.manager.core.layout'));
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\homebox\Plugin\Field\FieldType\PortletItem $item */
    $item = $items[$delta];

    /** @var \Drupal\homebox\Entity\Homebox $homebox */
    $homebox = $form_state->getFormObject()->getEntity();
    $homeboxType = $homebox->getHomeboxType();

    // Note, that we might hide this through the states api, since Drupal core,
    // does not implement a delete button, see
    // https://www.drupal.org/project/drupal/issues/1038316.
    $element['delta'] = [
      '#type' => 'item',
      '#title' => $this->t('Delta'),
      '#markup' => $delta,
    ];
    $element['portletTypeId'] = [
      '#type' => 'select',
      '#title' => $this->t('Homebox Portlet Type'),
      '#description' => $this->t('Select the portlet type to use. Leave empty to remove.'),
      '#options' => $this->getAllowedPortletTypesAsOptions($homeboxType),
      '#empty_value' => '',
      '#default_value' => $item->getPortletTypeId() ?? '',
      // Not yet possible to use #required, until:
      // https://www.drupal.org/project/drupal/issues/2980806 is fixed.
      // '#required' => TRUE,.
    ];

    if (!empty($form['layoutId']['widget']['#default_value'][0])) {
      $layoutId = $form['layoutId']['widget']['#default_value'][0];
      // Show select if a layout has been selected:
      $allowedLayoutRegions = $this->layoutManager->getDefinition($layoutId)->getRegionLabels();
      $element['layoutRegion'] = [
        '#type' => 'select',
        '#title' => $this->t('Layout Region'),
        '#default_value' => $item->getLayoutRegion(),
        '#options' => $allowedLayoutRegions,
        // Not yet possible to use #required, until:
        // https://www.drupal.org/project/drupal/issues/2980806 is fixed.
        // '#required' => TRUE,.
        '#states' => [
          'visible' => [
            ':input[name="portlets[' . $delta . '][portletTypeId]"]' => ['filled' => TRUE],
          ],
        ],
      ];
    }
    else {
      // Else use a textfield:
      $element['layoutRegion'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Layout Region'),
        '#default_value' => $item->getLayoutRegion(),
        // Not yet possible to use #required, until:
        // https://www.drupal.org/project/drupal/issues/2980806 is fixed.
        // '#required' => TRUE,.
        '#states' => [
          'visible' => [
            ':input[name="portlets[' . $delta . '][portletTypeId]"]' => ['filled' => TRUE],
          ],
        ],
      ];
    }

    $element['layoutRegionWeight'] = [
      '#type' => 'number',
      '#title' => $this->t('Layout Region Weight'),
      '#default_value' => $item->getLayoutRegionWeight() ?? 0,
      '#step' => 1,
      // Not yet possible to use #required, until:
      // https://www.drupal.org/project/drupal/issues/2980806 is fixed.
      // '#required' => TRUE,.
      '#states' => [
        'visible' => [
          ':input[name="portlets[' . $delta . '][portletTypeId]"]' => ['filled' => TRUE],
        ],
      ],
    ];
    $element['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $item->isEnabled() ?? FALSE,
      '#states' => [
        'visible' => [
          ':input[name="portlets[' . $delta . '][portletTypeId]"]' => ['filled' => TRUE],
        ],
      ],
    ];
    $element['portletLabel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Portlet label override'),
      '#default_value' => $item->getPortletLabel() ?? '',
      '#states' => [
        'visible' => [
          ':input[name="portlets[' . $delta . '][portletTypeId]"]' => ['filled' => TRUE],
        ],
      ],
    ];
    $element['thirdPartySettings'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Portlet third party settings settings'),
      '#description' => $this->t('These settings are individual by their portlet type. Uses JSON object notation for dynamic use. Defined, handled and validated by the portlet type plugin.'),
      '#default_value' => $item->getThirdPartySettings() ?? '',
      '#states' => [
        'visible' => [
          ':input[name="portlets[' . $delta . '][portletTypeId]"]' => ['filled' => TRUE],
        ],
      ],
    ];

    $element['#element_validate'][] = [$this, 'validate'];

    return $element;
  }

  /**
   * Validate the json given inside "$element['thirdPartySettings']".
   */
  public function validate(&$element, FormStateInterface $form_state, $form) {
    // Skip validation, if the homebox portlet type id is empty:
    if (empty($element['portletTypeId']['#value'])) {
      return;
    }
    // @improve Instead of checking this form, set a validator on the PortletItem
    // so that it's also being checked, if filled programmatically!
    // See https://www.drupal.org/docs/drupal-apis/entity-api/entity-validation-api/defining-constraints-validations-on-entities-andor-fields
    // Validate, if the portletTypeId is allowed or not:
    $homebox = $form_state->getFormObject()->getEntity();
    $homeboxType = HomeboxType::load($homebox->bundle());
    $allowedPortlets = $homeboxType->getHomeboxTypePortletTypesSettings(TRUE);
    $portletTypeId = $element['portletTypeId']['#value'];
    if (!empty($portletTypeId) && !array_key_exists($portletTypeId, $allowedPortlets)) {
      $form_state->setError($element['portletTypeId'], $this->t('This homebox portlet type is not allowed!'));
    }
    // Validate if the layout region is valid:
    $layoutRegionValue = $element['layoutRegion']['#value'];
    $layoutId = $form_state->getValue('layoutId')[0]['value'];
    $allowedLayoutRegions = $this->layoutManager->getDefinition($layoutId)->getRegionNames();
    if (!in_array($layoutRegionValue, $allowedLayoutRegions)) {
      $allowedLayoutRegionsString = implode(", ", $allowedLayoutRegions);
      $form_state->setError($element['layoutRegion'], $this->t('The given portlet region is not valid for the selected layout. Allowed regions for the selected layout are: @allowedRegions.', ['@allowedRegions' => $allowedLayoutRegionsString]));
    }
    $thirdPartySettingsJson = trim($element['thirdPartySettings']['#value']);
    if (!empty($thirdPartySettingsJson) && $thirdPartySettingsJson != '{}') {
      if (!$this->validateJson($thirdPartySettingsJson)) {
        $form_state->setError($element['thirdPartySettings'], $this->t('This is not a valid JSON schema'));
      }
      // @codingStandardsIgnoreStart
      // Get the current homeboxPortletType id, to make portlet type specific
      // validation:
      // $portletTypeId = $element['portletTypeId']['#value'];
      // $delta = $element['#delta'];
      // $portletType = $this->homeboxPortletTypeManager->createInstance($portletTypeId, [], $homebox, $delta);
      // $thirdPartySettingsArray = Json::decode($thirdPartySettingsJson);
      // Create a thirdPartySettings form and validate it through the given portlet type:
      // @improve Call thirdPartySettings validation:
      // $portletType->validateConfigurationForm($portletType->buildConfigurationForm($thirdPartySettingsArray));
      // @codingStandardsIgnoreEnd
    }
  }

  /**
   * Validates the given JSON string to be valid json.
   *
   * @param string $json
   *   The json string to validate.
   *
   * @return bool
   *   Whether the given JSON string is valid.
   */
  protected function validateJson(string $json): bool {
    // @improve Remove this if case, once we *require* PHP >=8.3
    if (function_exists('json_validate')) {
      return json_validate($json);
    }
    else {
      @trigger_error('JSON was not validated, as the current PHP version < 8.3 doesn\'t support json_validate(). Assuming the JSON is correct.');
      return TRUE;
    }
  }

  /**
   * Helper function to recerive the allowed portlet types as an options array.
   *
   * @param \Drupal\homebox\Entity\HomeboxType $homeboxType
   *   The homebox type to get the allowed portlet types from.
   */
  protected function getAllowedPortletTypesAsOptions(HomeboxType $homeboxType) {
    $homeboxTypePortletTypesSettings = $homeboxType->getHomeboxTypePortletTypesSettings(TRUE);
    $allowedPortletTypesOptions = [];
    foreach ($homeboxTypePortletTypesSettings as $portletId => $homeboxTypePortletTypesSetting) {
      $allowedPortletTypesOptions[$portletId] = $homeboxTypePortletTypesSetting['label'];
    }
    return $allowedPortletTypesOptions;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // Remove values without a portletTypeId.
    // They are being added because Drupal adds an empty entry for cardinality
    // UNLIMITED fields.
    // Can probably be removed, once this issue:
    // https://www.drupal.org/project/drupal/issues/2980806 is fixed.
    $filled = array_filter($values, function ($value) {
      return !empty($value['portletTypeId']);
    });
    return $filled;
  }

}

<?php

namespace Drupal\homebox\Plugin;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Ajax\AjaxFormHelperTrait;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ChangedCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\homebox\Entity\Homebox;
use Drupal\homebox\Entity\HomeboxInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provide a base class for HomeboxPortletType plugins.
 */
abstract class HomeboxPortletTypeBase extends PluginBase implements HomeboxPortletTypeInterface {
  use StringTranslationTrait;
  // Required for AJAX serialization in the form:
  use DependencySerializationTrait;
  use AjaxFormHelperTrait;

  /**
   * The parent homebox of this portlet.
   *
   * @var \Drupal\homebox\Entity\Homebox
   */
  protected $homebox;

  /**
   * The portlet field value delta within the homebox.
   *
   * The homebox plus delta identifies this portlet (for the moment).
   *
   * @var int
   */
  protected $delta;

  /**
   * The module config (Homebox Settings).
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $homeboxSettings;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The layout plugin manager.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
    $configuration,
    $plugin_id,
    $plugin_definition,
    $container->get('config.factory'),
    $container->get('form_builder'),
    $container->get('renderer'),
    $container->get('module_handler'),
    $container->get('plugin.manager.core.layout')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, FormBuilderInterface $formBuilder, Renderer $renderer, ModuleHandlerInterface $moduleHandler, LayoutPluginManagerInterface $layoutPluginManager, Homebox $homebox = NULL, int $delta = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $formBuilder;
    $this->renderer = $renderer;
    $this->moduleHandler = $moduleHandler;
    $this->layoutPluginManager = $layoutPluginManager;
    $this->homeboxSettings = $config_factory->get('homebox.settings');
    $this->homebox = $homebox;
    $this->delta = $delta;

    // We have to call this again to merge the defaults
    // (like BlockPluginTrait does).
    $this->setConfiguration($configuration);
  }

  /**
   * Returns generic default configuration for homebox portlet plugins.
   *
   * @return array
   *   An associative array with the default configuration.
   */
  public function defaultConfiguration(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultThirdPartyConfiguration(): array {
    // The third party default configuration:
    return [
      'color' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  final public function buildDisplay(): array {
    if (!$this->getHomeboxTypePortletTypeSettingStatus()) {
      // Generally do not show portlets that are disabled in the homebox type.
      return [];
    }

    $build = [
      '#theme' => 'homebox_portlet',
      '#content' => $this->buildDisplayContent(),
      '#label' => $this->getLabel(),
      '#attributes' => [
        'class' => [
          'homebox-portlet',
        ],
        'style' => !empty($this->getThirdPartyConfigurationValue('color')) ? '--homebox-portlet-color: ' . Html::escape($this->getThirdPartyConfigurationValue('color')) : '',
      ],
      '#plugin_id' => $this->getPluginId(),
      '#delta' => $this->getDelta(),
      // Required for the layout sorting:
      '#layoutRegionWeight' => $this->getConfigurationLayoutRegionWeight(),
      '#configuration' => $this->getConfiguration(),
      '#type_settings' => $this->getHomeboxTypePortletTypeSettings(),
      '#instance' => $this,
    ];

    $this->renderer->addCacheableDependency($build, $this);

    return $build;
  }

  /**
   * Builds the individual portlet content render array.
   *
   * @return array
   *   The portlet content render array.
   */
  abstract protected function buildDisplayContent(): array;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'homebox_portlet_form' . $this->getDelta();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigurationForm(): array {
    return $this->formBuilder->getForm($this);
  }

  /**
   * {@inheritdoc}
   */
  final public function buildForm(array $form, FormStateInterface $form_state): array {
    if (!$this->getHomeboxTypePortletTypeSettingStatus()) {
      // Generally do not show portlets that are disabled in the homebox type.
      return [];
    }

    // We need a tree structure to determine the different settings by delta:
    $form['homebox_portlet_edit'] = [
      '#type' => 'homebox_portlet_edit',
      'titlebar_actions' => $this->buildFormTitlebarActions(),
      'portlet_form' => [
        '#tree' => TRUE,
        '#type' => 'container',
        $this->getDelta() => $this->buildFormBase($form, $form_state),
        '#attributes' => [
          'class' => ['homebox-portlet__content'],
        ],
      ],
      // '#content' => $this->build(),
      '#label' => $this->getLabel(),
      '#description' => $this->getDescription(),
      '#attributes' => [
        'class' => [
          'homebox-portlet',
        ],
        'style' => !empty($this->getThirdPartyConfigurationValue('color')) ? '--homebox-portlet-color: ' . Html::escape($this->getThirdPartyConfigurationValue('color')) : '',
      ],
      '#plugin_id' => $this->getPluginId(),
      '#delta' => $this->getDelta(),
      // Required for the layout sorting:
      '#layoutRegionWeight' => $this->getConfigurationLayoutRegionWeight(),
      '#configuration' => $this->getConfiguration(),
      '#type_settings' => $this->getHomeboxTypePortletTypeSettings(),
      '#instance' => $this,
    ];

    // Add portlet settings attributes:
    if (!$this->getConfigurationEnabledStatus()) {
      $form['homebox_portlet_edit']['#attributes']['class'][] = 'disabled';
    }

    // Add portlet type settings attributes:
    if ($this->getHomeboxTypePortletTypeSettingSingleton()) {
      $form['homebox_portlet_edit']['#attributes']['data-portlet-is-singleton'] = TRUE;
      $form['homebox_portlet_edit']['#attributes']['class'][] = 'singleton';
    }
    if ($this->getHomeboxTypePortletTypeSettingDraggable()) {
      $form['homebox_portlet_edit']['#attributes']['data-portlet-is-draggable'] = TRUE;
      // Important: The draggable class needs to be set on the
      // direct child of the region, which is the form:
      $form['#attributes']['class'][] = 'draggable';
    }
    if ($this->getHomeboxTypePortletTypeSettingHideable()) {
      $form['homebox_portlet_edit']['#attributes']['data-portlet-is-hideable'] = TRUE;
      $form['homebox_portlet_edit']['#attributes']['class'][] = 'hideable';
    }
    if ($this->getHomeboxTypePortletTypeSettingRemovable()) {
      $form['homebox_portlet_edit']['#attributes']['data-portlet-is-removable'] = TRUE;
      $form['homebox_portlet_edit']['#attributes']['class'][] = 'removable';
    }

    $form['#attributes']['class'][] = 'homebox-portlet-form';

    // Required for the layout sorting:
    $form['#layoutRegionWeight'] = $this->getConfigurationLayoutRegionWeight();

    // We need to disable the form autocomplete here, as otherwise we get an
    // incorrect result when reloading the page after moving / adding portlets:
    $form['#attributes']['autocomplete'] = 'off';

    // Disable form caching for this form.
    $form['#cache']['max-age'] = 0;

    $this->renderer->addCacheableDependency($form, $this);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // The values can be found in the portlet_form by the current portlet delta.
    $portletFormValues = $form_state->getValue('portlet_form')[$this->getDelta()];
    if (empty($portletFormValues)) {
      $form_state->setErrorByName('', $this->t('Unexpected portlet form values!'));
    }
    if ($portletFormValues['delta'] !== $this->getDelta()) {
      $form_state->setErrorByName('delta', $this->t('Delta does not match!'));
    }
    $color = $portletFormValues['color'];
    $colorOptions = $this->getColorOptions() + [''];
    if (!array_key_exists($color, $colorOptions)) {
      $form_state->setErrorByName('color', $this->t('The given color: "@color" is not allowed. Valid colors: @allowed_colors',
      [
        '@color' => $color,
        '@allowed_colors' => implode(', ', array_keys($colorOptions)),
      ]
      ));
    }
    $allowedLayoutRegions = $this->layoutPluginManager->getDefinition($this->getHomebox()->getLayoutId())->getRegionNames();
    if (!in_array($portletFormValues['layout_region'], $allowedLayoutRegions)) {
      $form_state->setErrorByName('layout_region', $this->t('The layout region "@layout_region" does not exist!', [
        '@layout_region' => $portletFormValues['layout_region'],
      ]));
    }

    // Delegate to th plugins subform:
    $subform = $form['homebox_portlet_edit']['portlet_form'][$this->getDelta()]['configuration'] ?? [];
    $subform_state = SubformState::createForSubform($subform, $form, $form_state);
    $this->validateThirdPartyConfigurationForm($subform, $subform_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // The values can be found in the portlet_form by the current portlet delta.
    $portletFormValues = $form_state->getValue('portlet_form')[$this->getDelta()];
    if (empty($portletFormValues)) {
      throw new \UnexpectedValueException('Unexpected portlet form values!');
    }
    if ($portletFormValues['delta'] !== $this->getDelta()) {
      throw new \UnexpectedValueException('Delta does not match!');
    }

    if ($form_state->hasAnyErrors()) {
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -1000,
      ];
      $form['#sorted'] = FALSE;
      $response = new AjaxResponse();
      $response->addCommand(new ReplaceCommand('[data-drupal-selector="' . $form['#attributes']['data-drupal-selector'] . '"]', $form));
      return $response;
    }

    $layoutRegion = $portletFormValues['layout_region'];
    $this->setConfigurationLayoutRegion($layoutRegion);
    $layoutRegionWeight = (int) $portletFormValues['layout_region_weight'];
    $this->setConfigurationLayoutRegionWeight($layoutRegionWeight);
    $label = $portletFormValues['label'];
    $this->setConfigurationPortletLabel($label);
    $this->getHomebox()->setPortletArrayByValues($this->getDelta(), $this->getPluginId(), $layoutRegion, $layoutRegionWeight, $label);

    // Third party settings example provided by HomeboxPortletTypeBase:
    $color = $portletFormValues['color'];
    $this->setThirdPartyConfigurationValue('color', $color);

    // Delegate to th plugins subform:
    $subform = $form['homebox_portlet_edit']['portlet_form'][$this->getDelta()]['configuration'] ?? [];
    $subform_state = SubformState::createForSubform($subform, $form, $form_state);
    $this->submitThirdPartyConfigurationForm($subform, $subform_state);

    $this->getHomebox()->setThirdPartySettings($this->delta, $this->getThirdPartyConfiguration());
    $this->getHomebox()->save();

    $userInput = $form_state->getUserInput();
    $keys = $form_state->getCleanValueKeys();
    $newInputArray = [];
    foreach ($keys as $key) {
      if ($key == "op") {
        continue;
      }
      $newInputArray[$key] = $userInput[$key];
    }

    $form_state->setUserInput($newInputArray);
    $form_state->setRebuild(TRUE);

    $response = new AjaxResponse();
    // NOTE: Until https://www.drupal.org/project/drupal/issues/2897377
    // is fixed, the patch from that issue is needed, otherwise
    // the selector has a non existing random value appended and this won't
    // work:
    $formSelector = '[data-drupal-selector="' . $form['#attributes']['data-drupal-selector'] . '"]';
    $response->addCommand(new ReplaceCommand($formSelector, $form));
    $response->addCommand(new ChangedCommand($formSelector, '.homebox-portlet__titlebar-label'));
    return $response;
  }

  /**
   * {@inheritDoc}
   */
  final public function buildFormBase(array $form, FormStateInterface $form_state): array {
    $form['individualizations'] = [
      '#type' => 'details',
      '#title' => $this->t('Individualizations'),
      '#collapsed' => TRUE,
      // Always display this at the end:
      '#weight' => 999,
    ];
    $form['individualizations']['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $this->getConfigurationPortletLabel() ?? '',
      '#description' => $this->t('Allows overriding the title. Leave empty to use the default title.'),
      '#placeholder' => $this->getLabel(),
      '#weight' => 999,
      // We need to override the parents because of the details tree:
      '#parents' => ['portlet_form', $this->getDelta(), 'label'],
    ];
    $form['individualizations']['color'] = [
      '#type' => 'radios',
      '#title' => $this->t('Color'),
      '#default_value' => $this->getThirdPartyConfigurationValue('color') ?? '',
      '#options' => $this->getColorOptions(),
      '#weight' => 998,
      '#description' => $this->t('Select a color for this portlet.'),
      '#attributes' => ['class' => ['fieldgroup--homebox-color']],
      // We need to override the parents because of the details tree:
      '#parents' => ['portlet_form', $this->getDelta(), 'color'],
    ];
    $form['homebox_portlet_type_id'] = [
      '#type' => 'hidden',
      // This may NOT be changed, so we're using #value:
      '#value' => $this->getPluginId(),
      '#weight' => 9999,
    ];
    $form['delta'] = [
      '#type' => 'hidden',
      // This may NOT be changed, so we're using #value:
      '#value' => $this->getDelta(),
      '#weight' => 9999,
    ];
    $form['layout_region'] = [
      '#type' => 'hidden',
      // This may be changed:
      '#default_value' => $this->getConfigurationLayoutRegion(),
      '#weight' => 9999,
    ];
    $form['layout_region_weight'] = [
      '#type' => 'hidden',
      // This may be changed:
      '#default_value' => $this->getConfigurationLayoutRegionWeight(),
      '#weight' => 9999,
    ];

    if (!isset($form['configuration'])) {
      $form['configuration'] = [];
    }
    $subform_state = SubformState::createForSubform($form['configuration'], $form, $form_state);
    $form['configuration'] = $this->buildThirdPartyConfigurationForm($form['configuration'], $subform_state);

    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 999999,
      '#attributes' => [
        'class' => [
          'container-inline',
        ],
      ],
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
        '#ajax' => [
          'callback' => '::submitForm',
          'disable-refocus' => TRUE,
          // Drupal AJAX form submits are triggered on mousedown!
          // @ee https://www.drupal.org/project/drupal/issues/634616
          // @see https://www.drupal.org/files/issues/interdiff-634616-94-99.txt
          // We change it to click here to make the AJAX autosubmit easier.
          // If we run into other issues with this, this also needs to be
          // changed to "mousedown" in JavaScript, which has other downsides,
          // but matches the Drupal default implementation for type submit!
          'event' => 'click',
        ],
        '#attributes' => [
          'class' => [
            // Hide the submit, as it's triggered by JS automatically:
            'hidden',
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * Returns the actions render array for a single portlet.
   *
   * @return array
   *   The titlebar actions form.
   */
  protected function buildFormTitlebarActions(): array {
    $portletActions = [
      '#type' => 'homebox_portlet_edit_titlebar',
      '#label' => $this->getLabel(),
      '#plugin_id' => $this->getPluginId(),
      '#delta' => $this->getDelta(),
      '#configuration' => $this->getConfiguration(),
      '#type_settings' => $this->getHomeboxTypePortletTypeSettings(),
      '#instance' => $this,
      '#weight' => '-100',
    ];
    if (!$this->getConfigurationEnabledStatus()) {
      $portletActions['portlet_enable_ajax'] = [
        '#type' => 'homebox_button',
        '#name' => 'enablePortlet',
        '#delta' => $this->getDelta(),
        '#attributes' => [
          'class' => [
            'homebox-portlet__action',
            'homebox-icon',
            'homebox-icon--eye',
          ],
          'title' => $this->t('Enable / show @label Portlet (#@delta)', [
            '@label' => $this->getLabel(),
            '@delta' => $this->getDelta(),
          ]),
        ],
        '#ajax' => [
          'callback' => [$this, 'homeboxPortletEnableAjaxCallback'],
          'event' => 'click',
        ],
        // Do not validate data on hide / show:
        '#limit_validation_errors' => [],
      ];
    }
    else {
      if ($this->getHomeboxTypePortletTypeSettingHideable()) {
        $portletActions['portlet_disable_ajax'] = [
          '#type' => 'homebox_button',
          '#name' => 'disablePortlet',
          '#delta' => $this->getDelta(),
          '#attributes' => [
            'class' => [
              'homebox-portlet__action',
              'homebox-icon',
              'homebox-icon--eye-invisible',
            ],
            'title' => $this->t('Disable / hide @label Portlet (#@delta)', [
              '@label' => $this->getLabel(),
              '@delta' => $this->getDelta(),
            ]),
          ],
          '#ajax' => [
            'callback' => [$this, 'homeboxPortletDisableAjaxCallback'],
            'event' => 'click',
          ],
          // Do not validate data on hide / show:
          '#limit_validation_errors' => [],
        ];
      }
    }

    if ($this->getHomeboxTypePortletTypeSettingRemovable()) {
      $portletActions['portlet_remove_ajax'] = [
        '#type' => 'homebox_button',
        '#name' => 'removePortlet',
        '#delta' => $this->getDelta(),
        '#attributes' => [
          'class' => [
            'homebox-portlet__action',
            'homebox-icon',
            'homebox-icon--delete',
          ],
          'title' => $this->t('Remove @label Portlet (#@delta)', [
            '@label' => $this->getLabel(),
            '@delta' => $this->getDelta(),
          ]),
          'onClick' => 'return window.confirm(Drupal.t("Do you really want to remove this portlet?") + "\n\n" + Drupal.t("This action can not be undone."))',
        ],
        '#ajax' => [
          'callback' => [$this, 'homeboxPortletRemoveAjaxCallback'],
          'event' => 'click',
        ],
        // Do not validate data on delete.
        '#limit_validation_errors' => [],
      ];
    }

    return $portletActions;
  }

  /**
   * {@inheritDoc}
   */
  protected function successfulAjaxSubmit(array $form, FormStateInterface $form_state) {
    // @improve It would be even better to do no redirect and load the new content
    // via AJAX but that might be complex or even impossible?
    $response = new AjaxResponse();
    $command = new RedirectCommand(Url::fromRoute('<current>')->toString());
    $response->addCommand($command);
    return $response;
  }

  /**
   * Callback function on AJAX portlet removal.
   *
   * @return array
   *   The form part to refresh.
   */
  public function homeboxPortletEnableAjaxCallback(array &$form, FormStateInterface $form_state) {
    $triggeringElement = $form_state->getTriggeringElement();
    $delta = $triggeringElement['#delta'] ?? NULL;
    if ($delta !== $this->getDelta()) {
      // Ensure the delta matches, otherwise throw an exception as this should
      // always be the case. Possible form manipulation otherwise?
      throw new \Exception('No matching delta');
    }
    /** @var \Drupal\homebox\Entity\Homebox $homebox */
    $homebox = $this->homebox;
    if (!$homebox->access('update')) {
      // Safety net implementation:
      // @improve Perhaps this can be solved better in a more Drupal way?
      throw new AccessDeniedHttpException('You are not permitted to execute update operations on this homebox!');
    }

    $homebox->enablePortlet($delta);
    $homebox->save();

    // Reload the updated page:
    return $this->ajaxSubmit($form, $form_state);
  }

  /**
   * Callback function on AJAX portlet removal.
   *
   * @return array
   *   The form part to refresh.
   */
  public function homeboxPortletDisableAjaxCallback(array &$form, FormStateInterface $form_state) {
    $triggeringElement = $form_state->getTriggeringElement();
    $delta = $triggeringElement['#delta'] ?? NULL;
    if ($delta !== $this->getDelta()) {
      // Ensure the delta matches, otherwise throw an exception as this should
      // always be the case. Possible form manipulation otherwise?
      throw new \Exception('No matching delta');
    }
    /** @var \Drupal\homebox\Entity\Homebox $homebox */
    $homebox = $this->homebox;
    if (!$homebox->access('update')) {
      // Safety net implementation:
      // @improve Perhaps this can be solved better in a more Drupal way?
      throw new AccessDeniedHttpException('You are not permitted to execute update operations on this homebox!');
    }

    $homebox->disablePortlet($delta);
    $homebox->save();

    // Reload the updated page:
    return $this->ajaxSubmit($form, $form_state);
  }

  /**
   * Callback function on AJAX portlet removal.
   *
   * @return array
   *   The form part to refresh.
   */
  public function homeboxPortletRemoveAjaxCallback(array &$form, FormStateInterface $form_state) {
    $triggeringElement = $form_state->getTriggeringElement();
    $delta = $triggeringElement['#delta'] ?? NULL;
    if ($delta !== $this->getDelta()) {
      // Ensure the delta matches, otherwise throw an exception as this should
      // always be the case. Possible form manipulation otherwise?
      throw new \Exception('No matching delta');
    }
    /** @var \Drupal\homebox\Entity\Homebox $homebox */
    $homebox = $this->homebox;
    if (!$homebox->access('update')) {
      // Safety net implementation:
      // @improve Perhaps this can be solved better in a more Drupal way?
      throw new AccessDeniedHttpException('You are not permitted to execute update operations on this homebox!');
    }

    $homebox->removePortlet($delta);
    $homebox->save();

    // Reload the updated page:
    return $this->ajaxSubmit($form, $form_state);
  }

  /**
   * Get available color options.
   */
  private function getColorOptions(): array {
    $colors = $this->homeboxSettings->get('portletColors');
    array_walk($colors, function (&$value, $key) {
      // phpcs:ignore
      $colors[htmlentities($key)] = $this->t(Html::escape($value));
    });

    // Add empty option:
    return ['' => $this->t('Default')] + $colors;
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    $access = $this->portletAccess($account);
    return $return_as_object ? $access : $access->isAllowed();
  }

  /**
   * Indicates whether the Homebox Portlet should be shown.
   *
   * Portlets with specific access checking should override this method rather
   * than access(), in order to avoid repeating the handling of the
   * $return_as_object argument.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user session for which to check access.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   *
   * @see self::access()
   */
  protected function portletAccess(AccountInterface $account) {
    // By default, the block is visible.
    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinitionAdminLabel(): string {
    return $this->pluginDefinition['admin_label'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinitionAdminDescription(): string {
    return $this->pluginDefinition['admin_description'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinitionLabel(): string {
    return $this->pluginDefinition['label'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinitionDescription(): string {
    return $this->pluginDefinition['description'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinitionGroup(): string {
    return $this->pluginDefinition['group'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getHomebox(): HomeboxInterface {
    return $this->homebox;
  }

  /**
   * {@inheritdoc}
   */
  public function setHomebox(HomeboxInterface $homebox) {
    $this->homebox = $homebox;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): self {
    $this->configuration = NestedArray::mergeDeep(
          $this->defaultConfiguration(),
          $this->defaultThirdPartyConfiguration(),
          $configuration
      );

    return $this;
  }

  /**
   * Returns the portlet label. Max be overwritten to implement custom logic.
   *
   * OVerwrite to get the label from the portlet content, for example.
   *
   * @return string
   *   The label.
   */
  public function getLabel(): string {
    $label = $this->getConfigurationPortletLabel() ?: $this->getHomeboxTypePortletTypeSettingLabel() ?: $this->getPluginDefinitionLabel();
    return trim($label);
  }

  /**
   * {@inheritDoc}
   */
  public function getDescription(): string {
    $description = $this->getConfigurationPortletDescription() ?: $this->getHomeboxTypePortletTypeSettingDescription() ?: $this->getPluginDefinitionDescription();
    return trim($description);
  }

  /**
   * {@inheritDoc}
   */
  final public function getConfigurationPortletLabel(): string {
    return $this->configuration['portletLabel'] ?? '';
  }

  /**
   * {@inheritDoc}
   */
  final public function getConfigurationPortletDescription(): string {
    return $this->configuration['portletDescription'] ?? '';
  }

  /**
   * {@inheritDoc}
   */
  final public function setConfigurationPortletLabel(string $label): self {
    $this->configuration['portletLabel'] = $label;

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  final public function setConfigurationPortletDescription(string $label): self {
    $this->configuration['portletDescription'] = $label;

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  final public function getConfigurationLayoutRegion(): string {
    return $this->configuration['layoutRegion'];
  }

  /**
   * {@inheritDoc}
   */
  final public function setConfigurationLayoutRegion(string $layoutRegion): self {
    $this->configuration['layoutRegion'] = $layoutRegion;

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  final public function getConfigurationEnabledStatus(): bool {
    return !empty($this->configuration['enabled']);
  }

  /**
   * {@inheritDoc}
   */
  final public function setConfigurationEnabledStatus(bool $enabled): self {
    $this->configuration['enabled'] = $enabled;

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  final public function getConfigurationLayoutRegionWeight(): int {
    return $this->configuration['layoutRegionWeight'];
  }

  /**
   * {@inheritDoc}
   */
  final public function setConfigurationLayoutRegionWeight(int $layoutRegionWeight): self {
    $this->configuration['layoutRegionWeight'] = $layoutRegionWeight;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  final public function getThirdPartyConfiguration(): array {
    return $this->configuration['thirdPartySettings'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getThirdPartyConfigurationAsJson(): string {
    return json_encode($this->getThirdPartyConfiguration());
  }

  /**
   * {@inheritdoc}
   */
  final public function setThirdPartyConfiguration(array $settings): self {
    $this->configuration['thirdPartySettings'] = $settings;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  final public function getThirdPartyConfigurationValue(string $key): mixed {
    return $this->configuration['thirdPartySettings'][$key] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  final public function setThirdPartyConfigurationValue($key, $value): self {
    $this->configuration['thirdPartySettings'][$key] = $value;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  final public function getDelta(): int {
    return $this->delta;
  }

  /**
   * {@inheritdoc}
   */
  final public function setDelta($delta): self {
    $this->delta = $delta;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHomeboxTypePortletTypeSettings(): array {
    $homeboxType = $this->getHomebox()->getHomeboxType();
    return $homeboxType->getHomeboxTypePortletTypeSettings($this->getPluginId());
  }

  /**
   * {@inheritdoc}
   */
  public function getHomeboxTypePortletTypeSetting(string $setting): mixed {
    $homeboxType = $this->getHomebox()->getHomeboxType();
    return $homeboxType->getHomeboxTypePortletTypeSetting($this->getPluginId(), $setting);
  }

  /**
   * {@inheritdoc}
   */
  public function getHomeboxTypePortletTypeSettingLabel(): string {
    return $this->getHomeboxTypePortletTypeSetting('label');
  }

  /**
   * {@inheritdoc}
   */
  public function getHomeboxTypePortletTypeSettingDescription(): string {
    return $this->getHomeboxTypePortletTypeSetting('description');
  }

  /**
   * {@inheritdoc}
   */
  public function getHomeboxTypePortletTypeSettingStatus(): bool {
    return $this->getHomeboxTypePortletTypeSetting('status');
  }

  /**
   * {@inheritdoc}
   */
  public function getHomeboxTypePortletTypeSettingSingleton(): bool {
    return $this->getHomeboxTypePortletTypeSetting('singleton');
  }

  /**
   * {@inheritdoc}
   */
  public function getHomeboxTypePortletTypeSettingDraggable(): bool {
    return $this->getHomeboxTypePortletTypeSetting('draggable');
  }

  /**
   * {@inheritdoc}
   */
  public function getHomeboxTypePortletTypeSettingHideable(): bool {
    return $this->getHomeboxTypePortletTypeSetting('hideable');
  }

  /**
   * {@inheritdoc}
   */
  public function getHomeboxTypePortletTypeSettingRemovable(): bool {
    return $this->getHomeboxTypePortletTypeSetting('removable');
  }

  /**
   * {@inheritdoc}
   */
  public function getHomeboxTypePortletTypeSettingWeight(): int {
    return $this->getHomeboxTypePortletTypeSetting('weight');
  }

}

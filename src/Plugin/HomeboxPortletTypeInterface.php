<?php

namespace Drupal\homebox\Plugin;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\homebox\Entity\HomeboxInterface;

/**
 * Defines the interface for homebox portlet type plugins.
 *
 * @improve: We may try PluginWithFormsInterface here one day? See
 * BlockFrom::getPluginForm for example.
 */
interface HomeboxPortletTypeInterface extends PluginInspectionInterface, ConfigurableInterface, ThirdPartyConfigurationInterface, ContainerFactoryPluginInterface, FormInterface {

  /**
   * Returns the icon URL for the Portlet add form.
   *
   * @return string|null
   *   The add icon URL.
   */
  public function getPortletAddIconUrl(): ?string;

  /**
   * Returns the administrative label for this homebox portlet type plugin.
   *
   * @return string
   *   The label.
   */
  public function getPluginDefinitionAdminLabel(): string;

  /**
   * Returns the admin description for this homebox portlet type plugin.
   *
   * @return string
   *   The description.
   */
  public function getPluginDefinitionAdminDescription(): string;

  /**
   * Returns the portlet label. Max be overwritten to implement custom logic.
   *
   * OVerwrite to get the label from the portlet content, for example.
   *
   * @return string
   *   The label.
   */
  public function getLabel(): string;

  /**
   * Returns the user label for this homebox portlet type plugin.
   *
   * @return string
   *   The label.
   */
  public function getPluginDefinitionLabel(): string;

  /**
   * Returns the user description for this homebox portlet type plugin.
   *
   * @return string
   *   The description.
   */
  public function getPluginDefinitionDescription(): string;

  /**
   * {@inheritDoc}
   */
  public function getDescription(): string;

  /**
   * Returns the group for this homebox portlet type plugin.
   *
   * @return string
   *   The portlet type group.
   */
  public function getPluginDefinitionGroup(): string;

  /**
   * Builds the homebox portlet render array.
   *
   * @return array
   *   The render array.
   */
  public function buildDisplay(): array;

  /**
   * Indicates whether the homebox portlet should be shown.
   *
   * This method allows base implementations to add general access restrictions
   * that should apply to all extending homebox portlet plugins.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user session for which to check access.
   * @param bool $return_as_object
   *   (optional) Defaults to FALSE.
   *
   * @return bool|\Drupal\Core\Access\AccessResultInterface
   *   The access result. Returns a boolean if $return_as_object is FALSE (this
   *   is the default) and otherwise an AccessResultInterface object.
   *   When a boolean is returned, the result of AccessInterface::isAllowed() is
   *   returned, i.e. TRUE means access is explicitly allowed, FALSE means
   *   access is either explicitly forbidden or "no opinion".
   *
   * @see \Drupal\homebox\HomeboxAccessControlHandler
   */
  public function access(AccountInterface $account, $return_as_object = FALSE);

  /**
   * Returns the parent homebox of this portlet.
   *
   * @return \Drupal\homebox\Entity\HomeboxInterface
   *   The homebox.
   */
  public function getHomebox(): HomeboxInterface;

  /**
   * Set the portlet's parent homebox.
   *
   * @param \Drupal\homebox\Entity\HomeboxInterface $homebox
   *   The homebox object.
   *
   * @return self
   *   The homebox portlet instance.
   */
  public function setHomebox(HomeboxInterface $homebox);

  /**
   * Get the the portlet field value delta within the homebox.
   *
   * @return int
   *   The delta value.
   */
  public function getDelta(): int;

  /**
   * Set the portlet field value delta within the homebox.
   *
   * @param int $delta
   *   The delta.
   *
   * @return self
   *   The portlet instance.
   */
  public function setDelta(int $delta): self;

  /**
   * Returns the portlet configuration form.
   *
   * @return array
   *   The configuration form array.
   */
  public function getConfigurationForm(): array;

  /**
   * Gets the layout region from the portlets configuration array.
   *
   * @return string
   *   The layout region.
   */
  public function getConfigurationLayoutRegion(): string;

  /**
   * Sets the layout region in the portlets configuration array.
   *
   * @param string $layoutRegion
   *   The layout region key.
   *
   * @return self
   *   The portlet instance.
   */
  public function setConfigurationLayoutRegion(string $layoutRegion): self;

  /**
   * Gets the layout region from the portlets configuration array.
   *
   * @return int
   *   The layout region weight.
   */
  public function getConfigurationLayoutRegionWeight(): int;

  /**
   * Sets the layout region weight in the portlets configuration array.
   *
   * @param int $layoutRegionWeight
   *   The layout region weight.
   *
   * @return self
   *   The portlet instance.
   */
  public function setConfigurationLayoutRegionWeight(int $layoutRegionWeight): self;

  /**
   * Gets the label from the portlets configuration array.
   *
   * @return string
   *   The layout label.
   */
  public function getConfigurationPortletLabel(): string;

  /**
   * Sets the label in the portlets configuration array.
   *
   * @param string $label
   *   The label to set.
   *
   * @return self
   *   The portlet instance.
   */
  public function setConfigurationPortletLabel(string $label): self;

  /**
   * The enabled status of the portlet.
   *
   * @return bool
   *   The enabled status of the portlet.
   */
  public function getConfigurationEnabledStatus(): bool;

  /**
   * Set the enabled status of the portlet.
   *
   * @param bool $enabled
   *   Enabled status to set.
   *
   * @return self
   *   The portlet instance.
   */
  public function setConfigurationEnabledStatus(bool $enabled): self;

  /**
   * Retreive a specific configuration settings value.
   *
   * @param string $key
   *   The configuration name to retreive.
   *
   * @return mixed
   *   The configuration value, or NULL if the configuration does not exist.
   */
  public function getThirdPartyConfigurationValue(string $key): mixed;

  /**
   * Set a specific configuration settings value.
   *
   * @param string $key
   *   The configuration settings key to retreive.
   * @param mixed $value
   *   The configuration settings value to set.
   *
   * @return self
   *   The portlet instance.
   */
  public function setThirdPartyConfigurationValue($key, $value): self;

  /**
   * Returns all Homebox Type Portlet Type settings of this portlet.
   *
   * @return array
   *   The sttings value, NULL if not existing.
   */
  public function getHomeboxTypePortletTypeSettings(): array;

  /**
   * Returns the given setting from the Homebox Type Portlet settings.
   *
   * @param string $setting
   *   The settings id to return the value for, e.g. "status".
   *
   * @return mixed
   *   The sttings value, NULL if not existing.
   */
  public function getHomeboxTypePortletTypeSetting(string $setting): mixed;

  /**
   * Returns label configured for this portlet type in the Homebox Type.
   *
   * @return string
   *   The configured portlet label.
   */
  public function getHomeboxTypePortletTypeSettingLabel(): string;

  /**
   * Returns description configured for the portlet type in the Homebox Type.
   *
   * @return string
   *   The configured portlet label.
   */
  public function getHomeboxTypePortletTypeSettingDescription(): string;

  /**
   * Returns the status configured for this portlet type in the Homebox Type.
   *
   * @return bool
   *   Enabled?
   */
  public function getHomeboxTypePortletTypeSettingStatus(): bool;

  /**
   * Returns the singleton configured for this portlet type in the Homebox Type.
   *
   * @return bool
   *   Singleton?
   */
  public function getHomeboxTypePortletTypeSettingSingleton(): bool;

  /**
   * Returns the draggable configured for this portlet type in the Homebox Type.
   *
   * @return bool
   *   Draggable?
   */
  public function getHomeboxTypePortletTypeSettingDraggable(): bool;

  /**
   * Returns the hideable configured for this portlet type in the Homebox Type.
   *
   * @return bool
   *   hideable?
   */
  public function getHomeboxTypePortletTypeSettingHideable(): bool;

  /**
   * Returns the removable configured for this portlet type in the Homebox Type.
   *
   * @return bool
   *   Removable?
   */
  public function getHomeboxTypePortletTypeSettingRemovable(): bool;

  /**
   * Returns the weight configured for this portlet type in the Homebox Type.
   *
   * @return int
   *   The portlet type weight.
   */
  public function getHomeboxTypePortletTypeSettingWeight(): int;

}

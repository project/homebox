<?php

namespace Drupal\homebox\Plugin;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an interface for configurable third party settings.
 *
 * This derives from Drupal\Component\Plugin\ConfigurableInterface
 * + Drupal\Component\Plugin\PluginFormInterface
 * but renames the methods to include ThirdPartySettings configuration.
 * Maybe one day this could be combined with methods similar to
 * Drupal\Core\Config\Entity\ThirdPartySettingsInterface if it helps.
 */
interface ThirdPartyConfigurationInterface {

  /**
   * Gets this plugin's third party configuration.
   *
   * @return array
   *   An array of this plugin's third party configuration.
   */
  public function getThirdPartyConfiguration(): array;

  /**
   * Gets this plugin's third party configuration as JSON string.
   *
   * @return string
   *   The third party configuration as JSON string.
   */
  public function getThirdPartyConfigurationAsJson(): string;

  /**
   * Sets the third party configuration for this plugin instance.
   *
   * @param array $configuration
   *   An associative array containing the plugin's third party configuration.
   */
  public function setThirdPartyConfiguration(array $configuration);

  /**
   * Gets default third party configuration.
   *
   * @return array
   *   An associative array with the default configuration.
   */
  public function defaultThirdPartyConfiguration(): array;

  /**
   * Builds the third party configuration form, for the plugin specific form.
   *
   * @param array $subform
   *   An associative array containing the initial structure of the plugin form.
   * @param \Drupal\Core\Form\FormStateInterface $subform_state
   *   The current state of the form. Calling code should pass on a subform
   *   state created through
   *   \Drupal\Core\Form\SubformState::createForSubform().
   *
   * @see Drupal\Core\Plugin\PluginformInterface::buildConfigurationForm()
   *
   * @return array
   *   The form structure.
   */
  public function buildThirdPartyConfigurationForm(array &$subform, FormStateInterface $subform_state): array;

  /**
   * Validates the third party configuration form, for the plugin specific form.
   *
   * @param array $subform
   *   An associative array containing the structure of the plugin form as built
   *   by static::buildThirdPartyConfigurationForm().
   * @param \Drupal\Core\Form\FormStateInterface $subform_state
   *   The current state of the form. Calling code should pass on a subform
   *   state created through
   *   \Drupal\Core\Form\SubformState::createForSubform().
   *
   * @see Drupal\Core\Plugin\PluginformInterface::validateConfigurationForm()
   */
  public function validateThirdPartyConfigurationForm(array &$subform, FormStateInterface $subform_state): void;

  /**
   * Submits the third party configuration form, for the plugin specific form.
   *
   * @param array $subform
   *   An associative array containing the structure of the plugin form as built
   *   by static::buildThirdPartyConfigurationForm().
   * @param \Drupal\Core\Form\FormStateInterface $subform_state
   *   The current state of the form. Calling code should pass on a subform
   *   state created through
   *   \Drupal\Core\Form\SubformState::createForSubform().
   *
   * @see Drupal\Core\Plugin\PluginformInterface::submitConfigurationForm()
   */
  public function submitThirdPartyConfigurationForm(array &$subform, FormStateInterface $subform_state): void;

}

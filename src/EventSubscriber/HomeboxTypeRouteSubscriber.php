<?php

namespace Drupal\homebox\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Homebox type route subscriber.
 *
 * Homebox type route subscriber, for creating custom homebox pages per homebox
 * type.
 */
class HomeboxTypeRouteSubscriber extends RouteSubscriberBase {

  /**
   * An entity type manager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritDoc}
   */
  public function __construct($entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Alters route.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   *   A collection of all the routes.
   */
  protected function alterRoutes(RouteCollection $collection) {
    $homeboxTypeStorage = $this->entityTypeManager->getStorage('homebox_type');
    /**
     * @var \Drupal\homebox\Entity\HomeboxTypeInterface[] $homeboxTypes
     * */
    $homeboxTypes = $homeboxTypeStorage->loadMultiple();
    foreach ($homeboxTypes as $homeboxType) {
      if ($homeboxType->getPageEnabled() && !empty($homeboxType->getPagePath())) {
        $route = new Route(
              $homeboxType->getPagePath() . '/{owner}',
              [
                '_controller' => '\Drupal\homebox\Controller\HomeboxController::buildPage',
                '_title_callback' => '\Drupal\homebox\Controller\HomeboxController::buildPageTitle',
                'homebox_type' => $homeboxType->id(),
                'owner' => NULL,
              ],
              [
                '_custom_access' => '\Drupal\homebox\Controller\HomeboxController::accessPage',
              ],
              [
                'parameters' => [
                  'homebox_type' => [
                    'type' => 'entity:homebox_type',
                  ],
                  'owner' => [
                    'type' => 'entity:user',
                  ],
                ],
              ]
        );

        $routePersonalize = new Route(
              $homeboxType->getPagePath() . '/{owner}/personalize',
              [
                '_controller' => '\Drupal\homebox\Controller\HomeboxController::buildPagePersonalize',
                '_title' => 'Personalize',
                'homebox_type' => $homeboxType->id(),
              ],
              [
                '_custom_access' => '\Drupal\homebox\Controller\HomeboxController::accessPagePersonalize',
              ],
              [
                'parameters' => [
                  'homebox_type' => [
                    'type' => 'entity:homebox_type',
                  ],
                  'owner' => [
                    'type' => 'entity:user',
                  ],
                ],
              ]
        );
        if (!empty($collection->get('homebox.page_route.' . $homeboxType->id()))) {
          // Remove the existing route:
          $collection->remove('homebox.page_route.' . $homeboxType->id());
        }
        $collection->add('homebox.page_route.' . $homeboxType->id(), $route);
        $collection->add('homebox.page_route.' . $homeboxType->id() . '.personalize', $routePersonalize);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // Come after field_ui.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -110];
    return $events;
  }

}

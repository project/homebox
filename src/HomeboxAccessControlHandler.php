<?php

namespace Drupal\homebox;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Homebox entity.
 *
 * @see \Drupal\homebox\Entity\Homebox.
 */
class HomeboxAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    /**
     * @var \Drupal\homebox\Entity\Homebox $entity
     */

    // We need to check these before "bypass homebox access" as this should not
    // get bypassed:
    if ($operation == 'set_default_preset' && $entity->isPresetDefault()) {
      return AccessResult::forbidden('This homebox is already the default preset for the given homebox type')->addCacheableDependency($entity);
    }
    if ($operation == 'delete' && $entity->isPresetDefault()) {
      return AccessResult::forbidden('Homebox default presets may not be deleted')->addCacheableDependency($entity);
    }
    if ($operation == 'reset' && $entity->isPreset()) {
      return AccessResult::forbidden('Homebox presets can not be reset')->addCacheableDependency($entity);
    }

    // Allow admin permission to override all operations:
    if ($account->hasPermission('bypass homebox access')) {
      return AccessResult::allowedIfHasPermission($account, 'bypass homebox access')->addCacheableDependency($entity);
    }

    $homeboxType = $entity->bundle();
    $isOwner = ($account->id() === $entity->getOwnerId());

    switch ($operation) {
      case 'view':
        if ($entity->isPreset()) {
          return AccessResult::allowedIfHasPermission($account, 'administer ' . $homeboxType . ' presets')->addCacheableDependency($entity);
        }
        else {
          if ($isOwner) {
            return AccessResult::allowedIfHasPermissions($account, [
              'view own ' . $homeboxType . ' homebox',
              'view any ' . $homeboxType . ' homebox',
            ], 'OR')->addCacheableDependency($entity);
          }
          else {
            return AccessResult::allowedIfHasPermission($account, 'view any ' . $homeboxType . ' homebox')->addCacheableDependency($entity);
          }
        }
        return AccessResult::neutral()->cachePerPermissions()->addCacheableDependency($entity);

      case 'update':
      case 'personalize':
      case 'reset':
      case 'delete':
        if ($account->isAnonymous()) {
          // These actions may never be performed by an anonymous user!
          return AccessResult::forbidden('Is anonymous')->cachePerUser()->addCacheableDependency($entity);
        }

        if ($entity->isPreset()) {
          return AccessResult::allowedIfHasPermission($account, 'administer ' . $homeboxType . ' presets')->addCacheableDependency($entity);
        }
        else {
          if ($isOwner) {
            return AccessResult::allowedIfHasPermissions($account, [
              'personalize own ' . $homeboxType . ' homebox',
              'personalize any ' . $homeboxType . ' homebox',
            ], 'OR'
            )->addCacheableDependency($entity);
          }
          else {
            return AccessResult::allowedIfHasPermission($account, 'personalize any ' . $homeboxType . ' homebox')->addCacheableDependency($entity);
          }
        }
        return AccessResult::neutral()->cachePerPermissions()->addCacheableDependency($entity);

      case 'set_default_preset':
        return AccessResult::allowedIfHasPermission($account, 'administer ' . $homeboxType . ' presets')->addCacheableDependency($entity);
    }

    return parent::checkAccess($entity, $operation, $account);
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'personalize any ' . $entity_bundle . ' homebox');
  }

}

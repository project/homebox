<?php

namespace Drupal\homebox;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the HomeboxType entity.
 *
 * @see \Drupal\homebox\Entity\Homebox.
 */
class HomeboxTypeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /**
     * @var \Drupal\homebox\Entity\HomeboxType $entity
     */
    // Allow admin permission to override all operations:
    if ($account->hasPermission('bypass homebox access')) {
      return AccessResult::allowedIfHasPermission($account, 'bypass homebox access');
    }

    switch ($operation) {
      case 'presets':
        return AccessResult::allowedIfHasPermission($account, 'administer ' . $entity->id() . ' presets')->addCacheableDependency($entity);
    }
    return parent::checkAccess($entity, $operation, $account);
  }

}

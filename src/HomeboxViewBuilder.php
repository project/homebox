<?php

namespace Drupal\homebox;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Theme\Registry;
use Drupal\Core\Url;
use Drupal\homebox\Entity\Homebox;
use Drupal\homebox\Entity\HomeboxInterface;
use Drupal\homebox\Form\HomeboxPortletAddForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the view builder for homebox entities.
 */
class HomeboxViewBuilder extends EntityViewBuilder {

  /**
   * The homebox portlet type plugin manager.
   *
   * @var \Drupal\homebox\HomeboxPortletTypePluginManager
   */
  protected $portletTypePluginManager;

  /**
   * The layout plugin manager.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutPluginManager;

  /**
   * The route match interface.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   *   The form builder service
   */
  protected $formBuilder;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new HomeboxViewBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Theme\Registry $theme_registry
   *   The theme registry.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The homebox portlet type plugin manager.
   * @param \Drupal\homebox\HomeboxPortletTypePluginManager $portlet_type_plugin_manager
   *   The homebox portlet type plugin manager.
   * @param \Drupal\Core\Layout\LayoutPluginManagerInterface $layout_plugin_manager
   *   The layout plugin manage servicer.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match service.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityRepositoryInterface $entity_repository, LanguageManagerInterface $language_manager, Registry $theme_registry, EntityDisplayRepositoryInterface $entity_display_repository, HomeboxPortletTypePluginManager $portlet_type_plugin_manager, LayoutPluginManagerInterface $layout_plugin_manager, RouteMatchInterface $route_match, FormBuilderInterface $form_builder, AccountInterface $current_user, RendererInterface $renderer, MessengerInterface $messenger) {
    parent::__construct($entity_type, $entity_repository, $language_manager, $theme_registry, $entity_display_repository);
    $this->portletTypePluginManager = $portlet_type_plugin_manager;
    $this->layoutPluginManager = $layout_plugin_manager;
    $this->routeMatch = $route_match;
    $this->formBuilder = $form_builder;
    $this->currentUser = $current_user;
    $this->renderer = $renderer;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
          $entity_type,
          $container->get('entity.repository'),
          $container->get('language_manager'),
          $container->get('theme.registry'),
          $container->get('entity_display.repository'),
          $container->get('plugin.manager.homebox_portlet_type'),
          $container->get('plugin.manager.core.layout'),
          $container->get('current_route_match'),
          $container->get('form_builder'),
          $container->get('current_user'),
          $container->get('renderer'),
          $container->get('messenger'),
      );
  }

  /**
   * {@inheritDoc}
   */
  protected function alterBuild(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
    // Hide all unwanted children:
    foreach (Element::getVisibleChildren($build) as $key) {
      $child = &$build[$key];
      $child['#access'] = FALSE;
    }

    if ($view_mode === 'personalize') {
      $this->alterBuildPersonalize($build, $entity, $display, $view_mode);
    }
    else {
      $this->alterBuildDefault($build, $entity, $display, $view_mode);
    }

    // Hide our fields we don't want to show.
  }

  /**
   * Alter the build for default view modes ('default', 'full', ...)
   *
   * @param array $build
   *   The render array that is being created.
   * @param \Drupal\homebox\Entity\HomeboxInterface $homebox
   *   The homebox to be prepared.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The entity view display holding the display options configured for the
   *   entity components.
   * @param string $view_mode
   *   The view mode that should be used to prepare the entity.
   */
  protected function alterBuildDefault(array &$build, HomeboxInterface $homebox, EntityViewDisplayInterface $display, $view_mode) {
    $build['homebox_actions_general'] = [
      '#type' => 'actions',
      '#weight' => -100,
      'homebox_personalize' => $this->buildPersonalizeButton($homebox, $this->routeMatch->getRouteName()),
      '#attributes' => [
        'class' => [
          'container-inline',
          'homebox-actions',
          'homebox-actions--general',
        ],
      ],
    ];

    $build['#title'] = $homebox->viewLabel();

    /** @var \Drupal\homebox\Entity\Homebox $entity */
    $build['layout'] = $this->prepareLayoutBuild($homebox, $view_mode);

    // Set additional layout class:
    $build['#attributes']['class'][] = 'layout--homebox';
    // Attach homebox libraries:
    $build['#attached']['library'][] = 'homebox/homebox';

    if ($homebox->getOwner() !== $this->currentUser) {
      $this->renderer->addCacheableDependency($build, $homebox->getOwner());
    }
    $this->renderer->addCacheableDependency($build, $this->currentUser);
    $this->renderer->addCacheableDependency($build, $homebox);
    return $build;
  }

  /**
   * Special altering for the 'personalize' view-mode.
   *
   * @param array $build
   *   The render array that is being created.
   * @param \Drupal\homebox\Entity\HomeboxInterface $homebox
   *   The homebox to be prepared.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The entity view display holding the display options configured for the
   *   entity components.
   * @param string $view_mode
   *   The view mode that should be used to prepare the entity.
   */
  protected function alterBuildPersonalize(array &$build, HomeboxInterface $homebox, EntityViewDisplayInterface $display, $view_mode) {
    $build['homebox_actions_general'] = [
      '#type' => 'actions',
      '#weight' => -100,
      'back_button' => $this->buildBackButton($homebox),
      '#attributes' => [
        'class' => [
          'container-inline',
          'homebox-actions',
          'homebox-actions--general',
          'homebox-actions--general-personalize',
        ],
      ],
    ];

    // Render the personalize label:
    $build['#title'] = $homebox->personalizeLabel();

    $build['form_add'] = $this->formBuilder->getForm(HomeboxPortletAddForm::class, $homebox);

    $build['layout'] = $this->prepareLayoutBuild($homebox, $view_mode);
    $build['layout']['#attributes']['class'][] = 'layout--homebox-personalize';

    $build['#attached']['library'][] = 'homebox/builder';

    if ($homebox->getOwner() !== $this->currentUser) {
      $this->renderer->addCacheableDependency($build, $homebox->getOwner());
    }
    $this->renderer->addCacheableDependency($build, $this->currentUser);
    $this->renderer->addCacheableDependency($build, $homebox);

    if ($homebox->isPreset()) {
      $this->messenger->addWarning($this->t('You are personalizing a %preset Homebox',
      [
        '%preset' => $homebox->isPresetDefault() ? $this->t('default preset') : $this->t('preset'),
      ]));
    }
    else {
      if ($homebox->getOwnerId() !== $this->currentUser->id()) {
        $this->messenger->addWarning($this->t("You are personalizing %username's Homebox",
        [
          '%username' => $homebox->getOwner()->getDisplayName(),
        ]));
      }
    }
  }

  /**
   * Returns the 'back' button render array.
   *
   * @return array
   *   The back button render array.
   */
  protected function buildBackButton(EntityInterface $entity): array {
    return [
      '#type' => 'link',
      '#title' => $this->t('Back'),
      '#attributes' => [
        'class' => ['button', 'button--back'],
        'title' => $this->t('Back to display'),
      ],
      '#url' => Homebox::getHomeboxUrlFromBreadcrumbs(),
      '#attached' => [
        'library' => ['core/drupal.dialog.ajax'],
      ],
    ];
  }

  /**
   * Returns the 'personalize' button render array.
   *
   * @param \Drupal\homebox\Entity\HomeboxInterface $homebox
   *   The homebox instance to create the personalize button for.
   * @param string $current_route
   *   The current route.
   *
   * @return array
   *   The back button render array.
   */
  protected function buildPersonalizeButton(HomeboxInterface $homebox, string $current_route): array {
    $url = NULL;
    if ($current_route === 'homebox.user_tab') {
      $url = Url::fromRoute('homebox.user_tab.personalize', [
        'homebox_type' => $homebox->bundle(),
        'user' => $homebox->getOwnerId(),
      ]);
    }
    elseif (str_starts_with($current_route, 'homebox.page_route.')) {
      $url = Url::fromRoute('homebox.page_route.' . $homebox->bundle() . '.personalize', [
        'homebox_type' => $homebox->bundle(),
        'owner' => $homebox->getOwnerId(),
      ]);
    }
    if (!empty($url) && $url->access()) {
      // The user has personalize access.
      return [
        '#type' => 'link',
        '#title' => $this->t('Personalize'),
        '#description' => $this->t('Personalize this homebox.'),
        '#attributes' => [
          'class' => [
            'button',
            'button--personalize',
          ],
        ],
        '#url' => $url,
      ];
    }
    else {
      // No access. Do not show the button.
      return [];
    }
  }

  /**
   * Prepares the homebox layout.
   */
  protected function prepareLayoutBuild(EntityInterface $entity, $view_mode) {
    $editMode = $view_mode === 'personalize';

    /** @var \Drupal\homebox\Entity\Homebox $homebox */
    $homebox = $entity;

    $layoutRegions = $this->layoutPluginManager->getDefinition($homebox->getLayoutId())->getRegions();
    $layoutInstance = $this->layoutPluginManager->createInstance($homebox->getLayoutId());

    // Helper array to put the portlets into their layout regions:
    $layoutPortletsByRegion = [];

    $portlets = $homebox->getPortletsArrays(!$editMode);
    foreach ($portlets as $delta => $portletArray) {
      /** @var \Drupal\homebox\Plugin\HomeboxPortletTypeInterface $portletInstance */
      $portletInstance = $this->portletTypePluginManager->createInstance($portletArray['portletTypeId'], $portletArray, $homebox, $delta);
      $access = $portletInstance->access($this->currentUser, TRUE);
      if ($editMode) {
        $portletInstanceConfigurationForm = $portletInstance->getConfigurationForm();
        if ($access->isAllowed()) {
          $layoutPortletsByRegion[$portletArray['layoutRegion']][$delta] = $portletInstanceConfigurationForm;
        }
      }
      else {
        if ($access->isAllowed()) {
          $layoutPortletsByRegion[$portletArray['layoutRegion']][$delta] = $portletInstance->buildDisplay();
        }
      }
    }
    // Sort portlets in their specific regions by weight:
    foreach ($layoutPortletsByRegion as &$regionPortlets) {
      usort(
          $regionPortlets, function ($a, $b) {
            return ($a['#layoutRegionWeight'] ?? 0) <=> ($b['#layoutRegionWeight'] ?? 0);
          }
        );
    }
    // Build the regions, that exist in the layout:
    $regions = [];
    foreach ($layoutRegions as $key => $layoutRegionData) {
      if (isset($layoutPortletsByRegion[$key])) {
        $regions[$key] = $layoutPortletsByRegion[$key];
      }
      if ($editMode) {
        $regions[$key]['#attributes']['class'][] = 'homebox-builder-region';
        $regions[$key]['#attributes']['data-homebox-label'] = $layoutRegionData['label'];
        $regions[$key]['#attributes']['data-layout-region'] = $key;
      }
    }
    $layoutBuild = $layoutInstance->build($regions);

    // Add ID via prefix / suffix for AJAX as suggested here:
    // https://www.drupal.org/docs/drupal-apis/ajax-api/basic-concepts
    $layoutBuild['#prefix'] = '<div id="layout-wrapper">';
    $layoutBuild['#suffix'] = '</div>';

    $layoutBuild['#attributes']['class'][] = 'layout--homebox';
    $layoutBuild['#attributes']['class'][] = $homebox->isPreset() || $homebox->isNew() ? 'layout--homebox-is-preset' : 'layout--homebox-is-personalized';

    $this->renderer->addCacheableDependency($layoutBuild, $homebox);
    return $layoutBuild;
  }

}

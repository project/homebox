const { watch, series, parallel, src, dest, lastRun } = require('gulp');

// =================================
// Configuration
// =================================

// List the module ('./') and submodule directories here, its important to have the
// "js", "css" & "scss" folders as direct children of this folders.
const folders = ['.'];

// Plugins
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');
const sass = require('gulp-sass')(require('sass'));
const postcss = require('gulp-postcss');
const cleanCss = require('gulp-clean-css');
const autoprefixer = require('autoprefixer');
const path = require('path');

function buildSass(done) {
  if (folders.length === 0) return done(); // nothing to do!
  const tasks = folders.map(function (folder) {
    return (
      src([path.join(folder, '/scss/**/*.scss')], {
        since: lastRun(buildSass),
      })
        .pipe(plumber())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(
          postcss([autoprefixer(), require('postcss-nested')], {
            syntax: require('postcss-scss'),
          }),
        )
        // Minitfy / Uglify
        .pipe(
          rename({
            suffix: '.min',
          }),
        )
        .pipe(cleanCss())
        .pipe(dest(`${folder}/dist/css`))
    );
  });
  return [tasks, done()];
}

exports.watch = function (done) {
  watch('./scss/', series(buildSass));
  // const doWatch = gulp.watch(globs.scss, gulp.series('sass', sync));
  // doWatch();
  done();
};

exports.build = function (done) {
  const doBuild = series(buildSass);
  doBuild();
  done();
};
